# CESI Project

[![pipeline status](https://framagit.org/cube-cesi/btp-maintenance/web-app/badges/master/pipeline.svg)](https://framagit.org/cube-cesi/btp-maintenance/web-app/-/commits/master)

[![coverage report](https://framagit.org/cube-cesi/btp-maintenance/web-app/badges/master/coverage.svg)](https://framagit.org/cube-cesi/btp-maintenance/web-app/-/commits/master)

## Installation du projet

Système d'exploitation nécessaire au projet :
- Ubuntu 20.04

Utilisateur **root** requis lors de la mise en place de l'application.

### Étape requise pour sécurisé l'installation

Afin que l'installation de l'application se fasse de manière sécurisé, nous allons créer un nouvel utilisateur sur le serveur distant.

```shell
$ adduser deploy
$ adduser deploy sudo
$ exit
```

Une fois cela effectué, connectez vous à l'utilisateur **deploy**.

### Installation de Ruby

Version de Ruby pour l'application:
- Ruby 2.7.2

Installation des paquets nécessaires pour l'installation de ruby, et sa compilation pour la gestion du **back-end** et du **front-end**.

⚠️ Vérifier à être connecter au bon utilisateur : **deploy**

```shell
deploy$ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
deploy$ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
deploy$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
deploy$ sudo add-apt-repository ppa:chris-lea/redis-server
deploy$ sudo apt-get update
deploy$ sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev dirmngr gnupg apt-transport-https ca-certificates redis-server redis-tools nodejs yarn
```

Nous allons pouvoir désormais installer la version **2.7.2** de Ruby via les commandes suivantes :

```shell
deploy$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
deploy$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
deploy$ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
deploy$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
deploy$ echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
deploy$ git clone https://github.com/rbenv/rbenv-vars.git ~/.rbenv/plugins/rbenv-vars
deploy$ exec $SHELL
deploy$ rbenv install 2.7.2
deploy$ rbenv global 2.7.2
```

Une fois cette installation finie, vous pouvez vérifier si Ruby est désormais installer sur votre serveur via la commande :

```shell
deploy$ ruby -v
deploy$ #Ruby 2.7.2
```

Ruby à besoin d'un paquet nommé **bundler** lui permettant de compiler ses composants.

```shell
deploy$ gem install bundler
```

Vous pouvez aussi bien vérifier son installation via la commande :

```shell
deploy$ bundle -v
deploy$ #Bundler version X.X
```

Afin que **Ruby** prennent en compte les nouveaux changements lié au bundler effectuez la commande :

```shell
deploy$ rbenv rehash
```

### Configuration du serveur

Afin d'avoir un serveur web permettant l'exploitation de l'application, vous devrez installer deux paquets :
- Nginx
- Passenger

```shell
deploy$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
deploy$ sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger focal main > /etc/apt/sources.list.d/passenger.list'
deploy$ sudo apt-get update
deploy$ sudo apt-get install -y nginx-extras libnginx-mod-http-passenger
deploy$ if [ ! -f /etc/nginx/modules-enabled/50-mod-http-passenger.conf ]; then sudo ln -s /usr/share/nginx/modules-available/mod-http-passenger.load /etc/nginx/modules-enabled/50-mod-http-passenger.conf ; fi
deploy$ sudo ls /etc/nginx/conf.d/mod-http-passenger.conf
```

Afin que **Passenger** pointe sur la bonne configuration ruby, vous devrez modifier les lignes suivantes :

Chemin du fichier: `/etc/nginx/conf.d/mod-http-passenger.conf`

```conf
passenger_ruby /home/deploy/.rbenv/shims/ruby;
```

Puis redémarrer le serveur **Nginx** avec sa nouvelle configuration.

```shell
deploy$ sudo service nginx start
```

Si aucune erreur et survenue, et que chaques étapes sont faites, vous devrez accéder à la page d'accueil de **Nginx** sur l'adresse ip publique de votre serveur distant.

Une fois cela vérifier, nous allons promouvoir la nouvelle configuration de Nginx pour l'adapter à notre serveur Ruby.

```shell
deploy$ sudo rm /etc/nginx/sites-enabled/default
```

Une fois la configuration supprimée, créer en une nouvelle avec ce chemin exact : `/etc/nginx/sites-enabled/web_app`.
Puis insérer le contenu suivant :

```
server {
  listen 80;
  listen [::]:80;

  server_name _;
  root /home/deploy/web_app/current/public;

  passenger_enabled on;
  passenger_app_env production;

  location /cable {
    passenger_app_group_name web_app_websocket;
    passenger_force_max_concurrent_requests_per_process 0;
  }

  # Allow uploads up to 100MB in size
  client_max_body_size 100m;

  location ~ ^/(assets|packs) {
    expires max;
    gzip_static on;
  }
}
```

Enregistrer le fichier, puis redémarrer le serveur **Nginx** via la commande suivante :

```shell
deploy$ sudo service nginx reload
```

### Création de la base de données

La base de données est MySQL, nous allons donc installer les paquets en question puis faire la configuration nécessaire pour l'application.

```shell
deploy$ sudo apt-get install mysql-server mysql-client libmysqlclient-dev
deploy$ sudo mysql
```

```sql
mysql> CREATE DATABASE IF NOT EXISTS web_app;
mysql> CREATE USER IF NOT EXISTS 'deploy'@'localhost' IDENTIFIED BY 'mot de passe';
mysql> CREATE USER IF NOT EXISTS 'deploy'@'%' IDENTIFIED BY 'mot de passe précédent';
mysql> GRANT ALL PRIVILEGES ON web_app.* TO 'deploy'@'localhost';
mysql> GRANT ALL PRIVILEGES ON web_app.* TO 'deploy'@'%';
mysql> FLUSH PRIVILEGES;
mysql> quit;
```

La base de données est désormais prête à l'implémentation de l'application.

### Déploiement du projet

Utilitaires nécessaires pour le déploiement:
- environnement local
  - Si Windows 10 : installer WSL ubuntu
  - Linux
  - MacOS
- accès au dépôt Git avec les permissions

#### Environnement Windows 10 local

Windows n'autorise pas actuellement les commandes exemplaires d'un serveur comme le font linux et macos. Pour se faire, vous devrez installer WSL :

Sur votre terminal Windows 10 : 
```shell
local$ dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
local$ dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

Ensuite, installer **ubuntu** depuis le Microsoft Store, puis suivre les étapes de **Environnement linux et macos**.

#### Environnement linux et macos

Faites l'étape **Installation de Ruby** sur votre environnement local.

#### Mise en place de la clé SSH

Pour permettre l'obtention et le déploiement de l'application, vous devrez utiliser une clé SSH.

Sur votre environnement local, exécutez les commandes suivantes en remplaçant par vos données de votre compte Git :

```shell
local$ ssh-keygen -t rsa -b 2048 -C "quentin.pipelier@viacesi.fr"
```

__Conditions à remplir:__
- ne pas changer l'emplacement du fichier
- ne pas mettre de **passphrase**

Copier désormais la clé via les commandes suivantes :

```shell
MacOS$ pbcopy < ~/.ssh/id_ed25519.pub
Linux$ xclip -sel clip < ~/.ssh/id_ed25519.pub
Windows$ cat ~/.ssh/id_ed25519.pub | clip
```

Puis insérer cette clé, dans vos paramètres de votre compte Git, dans la catégorie **SSH Keys**.
#### Configuration des fichiers déploiement

Désormais vous avez accès à la récupération du dépôt git via la commande :

```shell
local$ git clone git@framagit.org:cube-cesi/btp-maintenance/web-app.git 
```

Une fois le projet cloner, installer les composants nécessaires au déploiement :

```shell
local$ cd web-app/
local/web-app$ bundle
```

Puis modifier le fichier `config/deploy/production.rb` via un éditeur :
Vous aurez juste à modifier les lignes 62 à 71 avec vos identifiants de serveur,
- adresse ip du serveur
- mot de passe de l'utilisateur **deploy**

#### Configuration du serveur distant pour le déploiement

Revenez sur votre environnement distant, puis créer un nouveau fichier comportant ce chemin exact : `/home/deploy/myapp/.rbenv-vars`.
Et insérer le contenu suivant en modifiant le mot de passe pour l'utilisateur **deploy du serveur MySQL** :

```conf
# For MySQL
DATABASE_URL=mysql2://deploy:mot de passe de base de données@localhost/web_app

RAILS_MASTER_KEY=ohai
SECRET_KEY_BASE=1234567890

STRIPE_PUBLIC_KEY=x
STRIPE_PRIVATE_KEY=y
```

Vous êtes désormais prêt à faire le lancement du déploiement depuis votre environnement local.

```shell
local$ ssh-add ~/.ssh/id_rsa
local$ cap production deploy
```

Une fois la production faite, vous pouvez accéder à l'adresse ip publique de votre serveur distant sur un navigateur avec l'application en marche.
