FactoryBot.define do
  factory :issue, aliases: [:ticket] do
    description {"Test"}
    title {'Ticket'}
    material
    member 
    state
    priority
    technician { nil }
  end
end
