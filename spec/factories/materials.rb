FactoryBot.define do
  factory :material, aliases: [:item] do
    name { "Chaise" }
    quantity { 28 }
    room
  end
end
