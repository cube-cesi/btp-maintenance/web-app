FactoryBot.define do
  factory :priority do
    sequence(:name) { |i| "Priorité n° #{i}" } 
  end
end
