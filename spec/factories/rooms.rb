FactoryBot.define do
  factory :room do
    name { 'Salle 24' }
    position_x { '17.22' }
    position_y { '8.36' }
    localisation_gps { '128.36,39.280,52.123' }
    name_url { name.parameterize }
    floor_id { FactoryBot.create(:floor).id }
  end
end
