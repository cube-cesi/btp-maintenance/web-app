FactoryBot.define do
  factory :state, aliases: [:etat] do
    name { 'État de test' } 
    order { 2 }
  end
end
