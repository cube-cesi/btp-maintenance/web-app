FactoryBot.define do
  factory :setting do
    mailSmtpAddress { 'smtp.gmail.com' } 
    mailSmtpPort { 587 }
    mailAddress { 'azerty@uiop.fr' }
    encrypted_pswd { SecureRandom.alphanumeric(255) }
    encrypted_pswd_iv { SecureRandom.alphanumeric(18) }
  end
end