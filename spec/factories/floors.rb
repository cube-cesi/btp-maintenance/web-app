FactoryBot.define do
  factory :floor do
    name { 'Étage 8' }
    plan_source { 'truc.png' }
  end
end
