require 'rails_helper'

RSpec.describe Comment, type: :model do
    context 'CRUD' do
        before do
          i = Issue.create(description: "Test", title: 'test issue', material: Material.first, member: Member.first,
             state: State.first, technician: nil ) 
          @comment = Comment.create(description: 'Test description', member: FactoryBot.create(:user), 
            issue: FactoryBot.create(:issue))
        end
    
        it 'should persist a comment' do
          expect{ Comment.create(description: 'Test description', member: Member.first, issue: Issue.first) }
          .to change{ Comment.count }.by(1)
        end 
    
        it 'should update a comment' do
          @comment.update(description: 'Lorem ipsum')
      
          expect(@comment.description).to eq('Lorem ipsum')
        end
    
        it 'should delete a comment' do
          comment = Comment.last
    
          expect{ comment.destroy }.to change{ Comment.count }.by(-1)
        end
    
        it 'should read comments' do
          comments = Comment.all
    
          expect(Comment.count).to eq(comments.count)
        end
      end
end
