require 'rails_helper'

RSpec.describe Room, type: :model do
  context 'CRUD' do
    before do 
      @room = FactoryBot.create(:room)
    end

    it 'should persist a room' do
      expect{ Room.create(name: 'Salle 12', floor_id: Floor.first.id) }.to change{ Room.count }.by(1)
    end 

    it 'should update a room' do
      @room.update(position_y: '1478.23')
  
      expect(@room.position_y).to eq('1478.23')
    end

    it 'should delete a room' do
      room = Room.last

      expect{ room.destroy }.to change{ Room.count }.by(-1)
    end

    it 'should read rooms' do
      rooms = Room.all

      expect(Room.count).to eq(rooms.count)
    end
  end
end
