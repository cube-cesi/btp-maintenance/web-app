require 'rails_helper'

RSpec.describe Priority, type: :model do
  context 'CRUD' do
    before do 
      @prio = Priority.create(name: 'test')
    end

    it 'should persist a priority' do
      expect{ Priority.create(name: 'Test') }.to change{ Priority.count }.by(1)
    end 

    it 'should update a priority' do
      @prio.update(name: 'New name')
  
      expect(@prio.name).to eq('New name')
    end

    it 'should delete a priority' do
      priority = Priority.last

      expect{ priority.destroy }.to change{ Priority.count }.by(-1)
    end

    it 'should read priorities' do
      priorities = Priority.all

      expect(Priority.count).to eq(priorities.count)
    end
  end
end
