require 'rails_helper'

RSpec.describe Floor, type: :model do
  context 'CRUD' do
    before do 
      @floor = Floor.create(name: 'test')
    end

    it 'should persist a floor' do
      expect{ Floor.create(name: 'Floor') }.to change{ Floor.count }.by(1)
    end 

    it 'should update a floor' do
      @floor.update(plan_source: 'test.png')
  
      expect(@floor.plan_source).to eq('test.png')
    end

    it 'should delete a floor' do
      floor = Floor.last

      expect{ floor.destroy }.to change{ Floor.count }.by(-1)
    end

    it 'should read floors' do
      floors = Floor.all

      expect(Floor.count).to eq(floors.count)
    end
  end
end
