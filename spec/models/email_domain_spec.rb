require 'rails_helper'

RSpec.describe EmailDomain, type: :model do
  context 'CRUD' do
    before do 
      @domain = EmailDomain.create(name: 'test.org')
    end

    it 'should persist a domain' do
      expect{ EmailDomain.create(name: 'other-test.fr') }.to change{ EmailDomain.count }.by(1)
    end 

    it 'should update a domain' do
      @domain.update(name: 'test.fr')
  
      expect(@domain.name).to eq('test.fr')
    end

    it 'should delete a domain' do
      domain = EmailDomain.last

      expect{ domain.destroy }.to change{ EmailDomain.count }.by(-1)
    end

    it 'should read domain' do
      domains = EmailDomain.all

      expect(EmailDomain.count).to eq(domains.count)
    end
  end
end
