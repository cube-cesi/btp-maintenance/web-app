require 'rails_helper'

RSpec.describe Notification, type: :model do
  context 'CRUD' do
    before do 
      @notification = Notification.create(description: 'Test', member: FactoryBot.create(:user))
    end

    it 'should persist a notification' do
      expect{ Notification.create(description: 'Notification', member: FactoryBot.create(:member_2)) }.
        to change{ Notification.count }.by(1)
    end 

    it 'should update a notification' do
      @notification.update(description: 'A description')
  
      expect(@notification.description).to eq('A description')
    end

    it 'should delete a notification' do
      notification = Notification.last

      expect{ notification.destroy }.to change{ Notification.count }.by(-1)
    end

    it 'should read notifications' do
      notifications = Notification.all

      expect(Notification.count).to eq(notifications.count)
    end
  end
end
