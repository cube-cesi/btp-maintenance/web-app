require 'rails_helper'

RSpec.describe Material, type: :model do
  context 'CRUD' do
    before do 
      @material = FactoryBot.create(:item)
    end

    it 'should persist a material' do
      expect{ Material.create(room: Room.first, name: 'name', quantity: 1) }.to change{ Material.count }.by(1)
    end 

    it 'should update a material' do
      @material.update(quantity: 123)
  
      expect(@material.quantity).to eq(123)
    end

    it 'should delete a material' do
      material = Material.last

      expect{ material.destroy }.to change{ Material.count }.by(-1)
    end

    it 'should read materials' do
      materials = Material.all

      expect(Material.count).to eq(materials.count)
    end
  end
end
