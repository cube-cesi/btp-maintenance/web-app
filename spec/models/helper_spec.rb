require 'rails_helper'

RSpec.describe Helper, type: :model do
  context 'CRUD' do
    before do 
      @helper = Helper.create(heading: 'Title test', description: 'test')
    end

    it 'should persist a helper' do
      expect{ Helper.create(heading: 'Helper') }.to change{ Helper.count }.by(1)
    end 

    it 'should update a helper' do
      @helper.update(description: 'A description')
  
      expect(@helper.description).to eq('A description')
    end

    it 'should delete a helper' do
      helper = Helper.last

      expect{ helper.destroy }.to change{ Helper.count }.by(-1)
    end

    it 'should read helpers' do
      helpers = Helper.all

      expect(Helper.count).to eq(helpers.count)
    end
  end
end
