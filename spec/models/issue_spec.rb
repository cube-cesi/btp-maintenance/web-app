require 'rails_helper'

RSpec.describe Issue, type: :model do
  context 'CRUD' do
    before do
      @issue = FactoryBot.create(:issue)
    end

    it 'should persist an issue' do
      expect{ Issue.create(description: "Azerty", title: 'Test2', member: Member.first, 
                material: Material.first, state: State.first, priority: Priority.first) }
        .to change{ Issue.count }.by(1)
    end 

    it 'should update an issue' do
      @issue.update(technician: 'Alberto Gonzales')
  
      expect(@issue.technician).to eq('Alberto Gonzales')
    end

    it 'should delete an issue' do
      issue = Issue.last

      expect{ issue.destroy }.to change{ Issue.count }.by(-1)
    end

    it 'should read issues' do
      issues = Issue.all

      expect(Issue.count).to eq(issues.count)
    end
  end
end
