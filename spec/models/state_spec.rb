require 'rails_helper'

RSpec.describe State, type: :model do
  context 'CRUD' do
    before do 
      @state = State.create(name: 'test')
    end

    it 'should persist a state' do
      expect{ State.create(name: 'Test') }.to change{ State.count }.by(1)
    end 

    it 'should update a state' do
      @state.update(order: 4)
  
      expect(@state.order).to eq(4)
    end

    it 'should delete a state' do
      state = State.last

      expect{ state.destroy }.to change{ State.count }.by(-1)
    end

    it 'should read states' do
      states = State.all

      expect(State.count).to eq(states.count)
    end
  end
end
