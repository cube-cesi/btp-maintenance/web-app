Rails.application.routes.draw do
root 'pages#index'

  namespace :api do
    resources :members, only: [:update, :create, :show, :index, :destroy]
    resources :rooms, param: :name_url
    resources :issues, only: [:update, :create, :show, :index, :destroy]
    resources :materials, only: [:update, :create, :show, :index, :destroy]
    resources :ranks, only: [:update, :create, :show, :index, :destroy]
    resources :floors, only: [:update, :create, :show, :index, :destroy]
    resources :states, only: [:update, :show, :create, :index, :destroy]
    resources :priorities, only: [:update, :create, :show, :index, :destroy]
    resources :notifications, only: [:update, :create, :show, :index, :destroy]
    resources :comments, only: [:update, :create, :show, :index, :destroy]
    resources :helpers, only: [:index, :create, :update, :destroy]
    resources :email_domains, only: [:index, :show, :create, :update, :destroy]
    resources :room_members, only: [:create]
    resources :settings, only: [:index]

    post '/comments/mobile', to: 'comments#create_mobile'

    get '/materials/room/:name_url', to: 'materials#index_by_room'

    patch '/floors/image/:id', to: 'floors#update_img'

    get '/states/manage/get', to: 'states#manageState'
    get '/states/manage/getIdClose', to: 'states#getIdCloseState'

    post '/room_members/delete', to: 'room_members#delete'

    post '/mailer/sendMaterial', to: 'mailers#sendEmailMaterial'

    get '/issues/room/:name_url', to: 'issues#index_by_room'
    get '/issues/get/all', to: 'issues#all'
    get '/issues/get/all/:id', to: 'issues#allByMember'
    post '/issues/mobile', to: 'issues#create_mobile'

    post '/rooms/create', to: 'rooms#create'
    get '/rooms/where/:floor_id', to: 'rooms#index_floor'
    get '/rooms/mobile/where/:floor_id', to: 'rooms#index_floor_mobile'
    get '/rooms/localisation/:localisation', to: 'rooms#index_coords'
    get '/rooms/informations/:name_url', to: 'rooms#informations'
    get '/rooms/notified/:room_id', to: 'rooms#notified'

    post '/2fa/send', to: 'twofa#sender'
    post '/2fa/handle', to: 'twofa#handle2FA'
    patch '/2fa/generate-code/:id', to: 'twofa#generate2FACode'

    patch '/settings/update/mail', to: 'settings#updateMailSettings'

    get '/members/token/:id', to: 'tokens#create'
    get '/members/validate_token/:token', to: 'tokens#validate_token'

    post '/auth/login',    to: 'sessions#create'
    get '/auth/anonymous', to: 'sessions#create_anonymous'
    get '/auth/remove-anonymous', to: 'sessions#remove_anonymous'
    get '/auth/is-anonymous', to: 'sessions#anonymous?'
    post '/auth/checkLogin', to: 'sessions#checkLogin'
    get '/auth/logout',   to: 'sessions#destroy'
    get '/auth/logged_in', to: 'sessions#is_logged_in?'
    
    get '/members/get/tech', to: 'members#all_technicians'
    patch '/members/last-connection/:id', to: 'members#updateLastConnection'
  end
# :nocov:
  get '*path', to: 'pages#index', constraints: lambda { |req|
    req.path.exclude? 'rails/active_storage'
  }
# :nocov:
end
