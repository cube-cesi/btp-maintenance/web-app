if Rails.env === 'production' 
# :nocov:
    Rails.application.config.session_store :cookie_store, key: '_web-app', domain: 'web-app-json-api'
# :nocov:  
  else
    Rails.application.config.session_store :cookie_store, key: '_web-app'
end