SecureHeaders::Configuration.default do |config|
    config.cookies = {
      secure: true, # mark all cookies as "Secure"
      httponly: true, # mark all cookies as "HttpOnly"
    }
    config.x_content_type_options = "nosniff"
    config.x_xss_protection = "1; mode=block"
    config.csp = {
      default_src: Rails.env.production? ? %w(https: 'self' 'unsafe-inline') : %w(http: 'self' 'unsafe-inline'),
      base_uri: %w(
        'self'
      ),
      form_action: %w(
        'self'
      ),
      connect_src: %w(
        'self'
      ),
      font_src: %w(
        'self'
        https://fonts.gstatic.com
      ),
      img_src: %w(
        'self'
        https://yt3.ggpht.com
      ),
      script_src: %w(
        'self'
        'unsafe-inline'
        https://*.cloudfront.net)
    }
    # Use the following if you have CSP issues locally with 
    # tools like webpack-dev-server
    if !Rails.env.production?
      config.csp[:connect_src] << "*"
    end
  end