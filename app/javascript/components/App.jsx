import React, { Component, Fragment } from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Login from './src/auth/Login'
import Register from './src/auth/Register'
import IssueView from './src/issues/IssueView'
import Home from './src/Home'
import Dashboard from './src/admin/Dashboard'
import MapView from './src/map/MapView'
import RoomView from './src/rooms/RoomView'
import RoomsView from './src/rooms/RoomsView'
import axios from 'axios'
import AxiosHelper from './utils/AxiosHelper'
import AlertTemplate from 'react-alert-template-basic'
import { positions, Provider } from 'react-alert';

const options = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: "50px"
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: null,
      member: {},
      rank: undefined,
      anonymous: false,
      sendAlertDisconnect: false,
      loaded: false
    };
  };

  componentDidMount = () => {
    this.loginStatus()
  }

  loginStatus = () => {
    AxiosHelper();

    axios.get('/api/auth/is-anonymous')
      .then(response => {
        if (!response.data.anonymous) {
          AxiosHelper()
          axios.get('/api/auth/logged_in',
            { withCredentials: true })
            .then(response => {
              if (response.data.logged_in) {
                this.handleLogin(response.data)
              } else {
                this.handleLogout()
              }
            })
            .catch(error => console.log('api errors:', error))
        } else {
          this.setState({ anonymous: response.data.anonymous, isLoggedIn: response.data.anonymous, loaded: true })
        }
      })
      .catch(error => console.log('api errors:', error))
  }

  handleAnonymous = (response) => {
    this.setState({
      anonymous: response, isLoggedIn: response
    })
  }

  handleLogin = (data) => {
    this.setState({
      isLoggedIn: true,
      member: data.member,
      rank: data.rank,
      loaded: true
    })
  }

  handleLogout = () => {
    this.setState({
      isLoggedIn: false,
      member: {},
      loaded: true
    })
  }

  handleDisconnect = () => {
    AxiosHelper()
    axios.get('/api/auth/logout')
      .then(response => {
        if (response.data.logged_out) {
          this.setState({ sendAlertDisconnect: true })
          this.handleLogout()
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  handleDesactivateAnonymous = () => {
    this.setState({ anonymous: false, isLoggedIn: false })
  }

  render() {
    const { loaded } = this.state
    return (
      <Provider template={AlertTemplate} {...options}>
        {loaded &&
          <BrowserRouter>
            <Switch>
              {
                !this.state.isLoggedIn &&
                
                <Fragment>
                  <Route
                    exact path='/register'
                    render={props => (
                      <Register {...props} handleLogin={this.handleLogin} />
                    )}
                  />
                  <Route
                    exact path='/login'
                    render={props => (
                      <Login handleLogin={this.handleLogin} handleAnonymous={this.handleAnonymous} isAnonymous={this.state.anonymous} alertDisconnect={this.state.sendAlertDisconnect} />
                    )}
                  />
                  {
                    window.location.pathname !== "/register" &&
                    <Redirect to="/login"/>
                  }
                </Fragment>
              }
              <Route
                exact path='/home'
                render={props => (
                  <Home {...props} isAnonymous={this.state.anonymous} handleDesactivateAnonymous={this.handleDesactivateAnonymous} handleDisconnect={this.handleDisconnect} rank={this.state.rank} member={this.state.member} />
                )}
              />
              <Route
                exact path='/plan'
                render={props => (
                  <MapView {...props} handleDesactivateAnonymous={this.handleDesactivateAnonymous} handleDisconnect={this.handleDisconnect} rank={this.state.rank} member={this.state.member} />
                )}
              />
              {!this.state.isAnonymous &&
                <Route
                  exact path='/my-issues'
                  render={props => (
                    <IssueView {...props} handleDesactivateAnonymous={this.handleDesactivateAnonymous} handleDisconnect={this.handleDisconnect} member={this.state.member} rank={this.state.rank}/>
                  )}
                />
              }
              <Route
                exact path='/rooms/:name_url'
                render={props => (
                  <RoomView {...props} isAnonymous={this.state.anonymous} handleDesactivateAnonymous={this.handleDesactivateAnonymous} handleDisconnect={this.handleDisconnect} member={this.state.member} rank={this.state.rank}/>
                )}
              />
              <Route
                exact path='/rooms'
                render={props => (
                  <RoomsView {...props} isAnonymous={this.state.anonymous} handleDesactivateAnonymous={this.handleDesactivateAnonymous} handleDisconnect={this.handleDisconnect} member={this.state.member} rank={this.state.rank}/>
                )}
              />
              {
                this.state.member.id && this.state.rank !== "Utilisateur" ? (
                  <Route
                    exact path='/dashboard'
                    render={props => (
                      <Dashboard {...props} rank={this.state.rank} member={this.state.member} />
                    )}
                  />) : (
                    <Redirect to="/home" />
                  )
              }
              <Redirect to="/home" />
            </Switch>
          </BrowserRouter>
        }
      </Provider>
    );
  }
};

export default App;