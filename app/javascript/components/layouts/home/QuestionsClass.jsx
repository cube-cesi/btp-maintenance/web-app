import React, { Component, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import { SectionHeading, Subheading as SubheadingBase } from "../misc/Headings";
import { Container, ContentWithPaddingXl } from "../misc/Container";
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import FeatherIcon from 'feather-icons-react';

const Heading = tw(SectionHeading)`w-full`;
const Description = tw.p`mt-4 text-sm md:text-base lg:text-lg font-medium leading-relaxed text-secondary-100 max-w-xl w-full text-center`;
const Column = tw.div`flex flex-col items-center`;
const HeaderContent = tw.div``;

const FAQSContainer = tw.dl`mt-12 max-w-4xl relative`;
const FAQ = tw.div`cursor-pointer select-none mt-5 px-8 sm:px-10 py-5 sm:py-4 rounded-lg text-gray-800 hover:text-gray-900 bg-gray-200 hover:bg-gray-300 transition duration-300`;
const Question = tw.dt`flex justify-between items-center`;
const QuestionText = tw.span`text-lg lg:text-xl font-semibold`;
const QuestionToggleIcon = motion.custom(styled.span`
  ${tw`ml-2 transition duration-300`}
  svg {
    ${tw`w-6 h-6`}
  }
`);
const Answer = motion.custom(tw.dd`pointer-events-none text-sm sm:text-base leading-relaxed`);

class QuestionsClass extends Component{
    constructor(props){
        super(props)

        this.state = {
            helpers: [],
            activateQuestionIndex: null
        }
    }

    componentDidMount(){
        AxiosHelper();
        axios.get(`/api/helpers`)
       .then(res => {
             this.setState({helpers: res.data.data})
       })
       .catch(error => this.props.alert.error("erreur : ", error))
    }

    toggleQuestion = (questionIndex) => {
        const {activateQuestionIndex} = this.state;

        if(questionIndex === activateQuestionIndex){
            this.setState({activateQuestionIndex: null})
        }else{
            this.setState({activateQuestionIndex: questionIndex})
        }
    };

    render(){

        const {activateQuestionIndex, helpers} = this.state;

        return(
            <Container id="helpers">
                <ContentWithPaddingXl>
                    <Column>
                        <HeaderContent>
                            <Heading>Comment ça marche ?</Heading>
                            <Description>Besoin d'aide ? Toutes les étapes sont présentes ici !</Description>
                        </HeaderContent>
                        <FAQSContainer>
                            {
                                helpers.map(helper =>
                                    <FAQ key={helper.id} onClick={() => this.toggleQuestion(helper.id)} className="group">
                                        <Question>
                                            <QuestionText>{helper.attributes.heading}</QuestionText>
                                            <QuestionToggleIcon
                                                variants={{
                                                collapsed: { rotate: 0 },
                                                open: { rotate: -180 }
                                                }}
                                                initial="collapsed"
                                                animate={activateQuestionIndex === helper.id ? "open" : "collapsed"}
                                                transition={{ duration: 0.02, ease: [0.04, 0.62, 0.23, 0.98] }}
                                            >
                                                
                                                <FeatherIcon icon="chevron-down"/>
                                            </QuestionToggleIcon>
                                        </Question>
                                        <Answer
                                            variants={{
                                                open: { opacity: 1, height: "auto", marginTop: "16px" },
                                                collapsed: { opacity: 0, height: 0, marginTop: "0px" }
                                            }}
                                            initial="collapsed"
                                            animate={activateQuestionIndex === helper.id ? "open" : "collapsed"}
                                            transition={{ duration: 0.3, ease: [0.04, 0.62, 0.23, 0.98] }}
                                        >
                                            {helper.attributes.description}
                                        </Answer>
                                    </FAQ>
                                )
                            }
                                
                        </FAQSContainer>
                    </Column>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default QuestionsClass