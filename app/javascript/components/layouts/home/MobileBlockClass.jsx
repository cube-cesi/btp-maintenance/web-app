import React, { Component } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro"; //eslint-disable-line
import { ContentWithPaddingXl, Container as ContainerBase } from "../misc/Container";
import { SectionHeading, Subheading as SubheadingBase } from "../misc/Headings";
import AppleIcon from '../../../../assets/images/icons/apple-icon.png'
import GooglePlayIcon from '../../../../assets/images/icons/google-play-icon.png'
import PhoneCesi from '../../../../assets/images/app-cesi.png'

const Container = tw(ContainerBase)`bg-gray-900 -mx-8`
const Content = tw(ContentWithPaddingXl)``
const Row = tw.div`px-8 flex items-center relative z-10 flex-col lg:flex-row text-center lg:text-left justify-center`;

const ColumnContainer = tw.div`max-w-2xl`
const TextContainer = tw(ColumnContainer)``;
const Text = tw(SectionHeading)`text-gray-100 lg:text-left max-w-none text-3xl leading-snug`;
const Subheading = tw(SubheadingBase)`text-yellow-500 mb-4 tracking-wider`

const LinksContainer = tw.div`mt-8 lg:mt-16 flex flex-col items-center sm:block`
const Link = styled.a`
  ${tw`w-56 p-3 sm:p-4 text-sm sm:text-base font-bold uppercase tracking-wider rounded-full inline-flex justify-center items-center mt-6 first:mt-0 sm:mt-0 sm:ml-8 first:ml-0 bg-gray-100 hocus:bg-gray-300 text-gray-900 hocus:text-gray-900 shadow hover:shadow-lg focus:shadow-outline focus:outline-none transition duration-300`}
  img {
    ${tw`inline-block h-8 mr-3`}
  }
  span {
    ${tw`leading-none inline-block`}
  }
`;

const ImageContainer = tw(ColumnContainer)`mt-16 lg:mt-0 lg:ml-16 flex justify-end`;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class MobileBlockClass extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Container css={false && tw`mb-20 lg:mb-24`}>
                <Content>
                    <Row>
                        <TextContainer>
                            <Subheading>Notre application mobile</Subheading>
                            <Text>Encore plus de fonctionnalités sur notre application <HighlightedText>mobile</HighlightedText> !</Text>
                            <LinksContainer>
                                <Link>
                                    <img src={AppleIcon} alt="apple-icon"/>
                                    <span>Télécharger</span>
                                </Link>
                                <Link>
                                    <img src={GooglePlayIcon} alt="google-play-icon"/>
                                    <span>Télécharger</span>
                                </Link>
                            </LinksContainer>
                        </TextContainer>
                        <ImageContainer>
                            <img src={PhoneCesi} alt="app-cesi-phone" tw="w-64"/>
                        </ImageContainer>
                    </Row>
                </Content>
            </Container>
        )
    }
}

export default MobileBlockClass