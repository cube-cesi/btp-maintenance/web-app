import React, { Component } from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { ContentWithPaddingXl, Container } from "../misc/Container";
import {Redirect}  from 'react-router-dom'

const OrangeBackgroundContainer = tw.div`lg:py-12 bg-orange-400 rounded-lg relative`
const Row = tw.div`px-8 max-w-screen-lg mx-auto flex items-center relative z-10 flex-col lg:flex-row text-center lg:text-left`;

const ColumnContainer = tw.div`lg:w-1/2 max-w-lg`
const TextContainer = tw(ColumnContainer)``;
const Text = tw.h5`text-gray-100 text-2xl sm:text-3xl font-bold`;

const LinksContainer = tw(ColumnContainer)`flex justify-center lg:justify-end mt-6 lg:mt-0 flex-col sm:flex-row`;

const PrimaryButton = styled.button`
  ${tw`w-full sm:w-auto text-sm sm:text-base bg-red-500 text-gray-100 shadow-lg hocus:bg-red-600 hocus:text-gray-200 px-6 py-3 sm:px-8 sm:py-4 lg:px-10 lg:py-5 mt-4 first:mt-0 sm:mt-0 sm:mr-8 sm:last:mr-0 rounded-full font-bold border border-transparent tracking-wide transition duration-300 focus:outline-none focus:shadow-outline`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
`;

const SecondaryButton = styled.button`
  ${tw`w-full sm:w-auto text-sm sm:text-base text-gray-100 border-white hover:bg-gray-100 hover:text-orange-500 hover:border-orange-500 px-6 py-3 sm:px-8 sm:py-4 lg:px-10 lg:py-5 mt-4 first:mt-0 sm:mt-0 sm:mr-8 sm:last:mr-0 rounded-full font-bold border tracking-wide transition duration-300 focus:outline-none focus:shadow-outline`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
`;

const HighlightedText = tw.span`bg-white text-orange-400 px-4 transform -skew-x-12 inline-block`;

class StartedBlockClass extends Component{
    constructor(props){
        super(props)

        this.state = {
            redirectPlan: false
        }
    }

    redirectToPlan = () => {
        this.setState({redirectPlan: true})
    }

    render(){
        const {redirectPlan} = this.state

        if(redirectPlan){
            return(<Redirect to='/plan'/>)
        }else{
            return(
                <Container css={true && tw`mb-20`}>
                    <ContentWithPaddingXl>
                        <OrangeBackgroundContainer>
                            <Row>
                                <TextContainer>
                                    <Text>Il est <HighlightedText>temps</HighlightedText> de commencer ...</Text>
                                </TextContainer>
                                <LinksContainer>
                                    <PrimaryButton onClick={this.redirectToPlan} type="button">
                                        <span className="text">Go !</span>
                                    </PrimaryButton>
                                    <a href="#helpers"><SecondaryButton type="button">
                                        <span className="text">Comment ça marche ?</span>
                                    </SecondaryButton></a>
                                </LinksContainer>
                            </Row>
                        </OrangeBackgroundContainer>
                    </ContentWithPaddingXl>
                </Container>
        )
        }
    }
}

export default StartedBlockClass