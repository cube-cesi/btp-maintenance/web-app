import React, {Component} from 'react'
import styled from "styled-components";
import tw from "twin.macro";
import { SectionHeading } from "../misc/Headings";

const Container = tw.div`relative`;

const ThreeColumnContainer = styled.div`
  ${tw`flex flex-col items-center md:items-stretch md:flex-row flex-wrap md:justify-center max-w-screen-xl mx-auto py-20 md:py-24`}
`;
const Heading = tw(SectionHeading)`w-full`;

const Column = styled.div`
  ${tw`md:w-1/2 lg:w-1/3 px-6 flex`}
`;

const Card = styled.div`
  ${tw`flex flex-col mx-auto max-w-xs items-center px-6 py-10 border-2 border-dashed border-orange-400 rounded-lg mt-12`}
  .imageContainer {
    ${tw`border-2 border-orange-400 text-center rounded-full p-6 flex-shrink-0 relative`}
    img {
      ${tw`w-8 h-8`}
    }
  }

  .textContainer {
    ${tw`mt-6 text-center`}
  }

  .title {
    ${tw`mt-2 font-bold text-xl leading-none text-orange-400`}
  }

  .description {
    ${tw`mt-3 font-semibold text-secondary-100 text-sm leading-loose`}
  }
`;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class StartedStepClass extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Container>
                <ThreeColumnContainer>
                    <Heading>Les <HighlightedText>services</HighlightedText> disponibles</Heading>
                    <Column>
                        <Card>
                            <span className="imageContainer">
                                <img src={require('../../../../assets/images/icons/support.svg')} alt="" />
                            </span>
                            <span className="textContainer">
                                <span className="title">Tickets</span>
                                <p className="description">
                                Grâce aux tickets vous pouvez poster vos incidents vu dans des salles du CESI à tout moment, grâce à l'application web ou mobile.
                                </p>
                            </span>
                        </Card>
                    </Column>
                    <Column>
                        <Card>
                            <span className="imageContainer">
                                <img src={require('../../../../assets/images/icons/info.svg')} alt="" />
                            </span>
                            <span className="textContainer">
                                <span className="title">Informations</span>
                                <p className="description">
                                Toutes les informations des salles sont disponibles au public, récapitulant les matériaux et autres.
                                </p>
                            </span>
                        </Card>
                    </Column>
                    <Column>
                        <Card>
                            <span className="imageContainer">
                                <img src={require('../../../../assets/images/icons/map.svg')} alt="" />
                            </span>
                            <span className="textContainer">
                                <span className="title">Plan intéractif</span>
                                <p className="description">
                                Un plan intéractif est disponible pour voir les salles à une toute autre grandeur.
                                </p>
                            </span>
                        </Card>
                    </Column>
                </ThreeColumnContainer>
            </Container>
        )
    }
}

export default StartedStepClass