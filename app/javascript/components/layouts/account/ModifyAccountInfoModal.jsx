import React, { Component, Fragment } from 'react'
import FeatherIcon from 'feather-icons-react';
import tw from "twin.macro";
import styled from "styled-components";
import AxiosHelper from '../../utils/AxiosHelper';
import Axios from 'axios';
import { withAlert } from 'react-alert'

const Card = tw.div`md:z-50 h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;

const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ModifyAccountInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: this.props.member.firstname,
            lastname: this.props.member.lastname,
            email: this.props.member.email,
            password: "",
            loaded: true
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const {firstname, lastname, email, password} = this.state;

        let member;
        if(password.length > 0){
            member = {
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            }
        }else{
            member = {
                firstname: firstname,
                lastname: lastname,
                email: email,
            }
        }

        AxiosHelper()
        Axios.patch(`/api/members/${this.props.member.id}`, {member})
        .then(response => {
            if(!response.error){
                this.props.alert.success("Modification réussie.")
            }
        })
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleClose = () => {
        this.props.handleCloseModal()
    }

    render() {
        const { loaded, firstname, lastname, email, password } = this.state
        return (
            <Card>
                {loaded &&
                    <FormContainer>
                        <Form onSubmit={this.handleSubmit}>
                            <TextInfo>
                                <TitleReviewContainer>
                                    <Title>Paramètre du compte</Title>
                                    <CloseInfo onClick={this.handleClose}>
                                        <FeatherIcon icon="x" />
                                    </CloseInfo>
                                </TitleReviewContainer>
                                <Description>
                                    <Input
                                        placeholder="Prénom"
                                        type="text"
                                        name="firstname"
                                        value={firstname}
                                        required
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Nom de famille"
                                        type="text"
                                        name="lastname"
                                        value={lastname}
                                        required
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Adresse e-mail de votre compte"
                                        type="text"
                                        name="email"
                                        value={email}
                                        required
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Nouveau mot de passe"
                                        type="password"
                                        name="password"
                                        value={password}
                                        onChange={this.handleChange}
                                    />
                                </Description>
                            </TextInfo>
                            <PrimaryButton type="submit">Valider</PrimaryButton>
                        </Form>
                    </FormContainer>
                }
            </Card>
        )
    }
}

export default withAlert()(ModifyAccountInfo)