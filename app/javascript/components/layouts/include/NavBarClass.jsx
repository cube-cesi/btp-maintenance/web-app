import React, { Component, Fragment } from "react";
import tw from "twin.macro";
import styled from "styled-components";
import axios from 'axios'
import { css } from "styled-components/macro";
import NavBarMobile from "../misc/NavBarMobile";
import AxiosHelper from "../../utils/AxiosHelper";
import FeatherIcon from 'feather-icons-react';
import withUnmounted from '@ishawnwang/withunmounted'
import Modal from 'react-modal';
import ModifyAccountInfo from '../account/ModifyAccountInfoModal'

const Header = tw.header`
  flex justify-between items-center
  max-w-screen-xl mx-auto
`;

const NavLinks = tw.div`inline-block`;

const NavLink = tw.a`
  text-lg my-2 lg:text-sm lg:mx-6 lg:my-0
  font-semibold tracking-wide transition duration-300
  pb-1 border-b-2 border-transparent hover:border-orange-400 hocus:text-orange-400
`;

const DisconnectLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-red-500 text-gray-100
  hocus:bg-red-700 hocus:text-gray-200 focus:shadow-outline
  border-b-0 cursor-pointer ml-2!
`;

const DashboardLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-gray-800 text-gray-100
  hocus:bg-gray-900 hocus:text-gray-200 focus:shadow-outline 
  border-b-0 cursor-pointer ml-2!
`;

const AccountLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-orange-400 text-white
  hocus:bg-orange-500 hocus:text-white focus:shadow-outline 
  border-b-0 cursor-pointer ml-2!
`;

const LogoLink = styled(NavLink)`
  ${tw`flex items-center font-black border-b-0 text-2xl! ml-0!`};

  img {
    ${tw`w-10 mr-3`}
  }
`;

const DesktopNavLinks = tw.nav`
  hidden lg:flex flex-1 justify-between items-center
`;

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: 'transparent',
    border: '0px'
  }
};
class NavBarClass extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isAnonymous: false,
      showAccountModal: false
    }
  }

  componentDidMount() {
    Modal.setAppElement('body')

    this.checkIsAnonymous();
  }

  checkIsAnonymous() {
    AxiosHelper();

    axios.get('/api/auth/is-anonymous')
      .then(response => {
        this.setState({ isAnonymous: response.data.anonymous })
      })
  }

  handleDisconnectNav() {
    this.props.handleDisconnect();
  }

  handleClickOpenModal = () => {
    this.setState({ showAccountModal: true })
  }

  handleCloseModal = () => {
    this.setState({ showAccountModal: false });
  }

  render() {
    const collapseBreakpointCss = collapseBreakPointCssMap["lg"];

    return (
      <Header className="header-light">
        <Modal
          isOpen={this.state.showAccountModal}
          style={customStyles}
        >
          <ModifyAccountInfo member={this.props.member} handleCloseModal={this.handleCloseModal} />
        </Modal>
        <DesktopNavLinks css={collapseBreakpointCss.desktopNavLinks}>
          <LogoLink href="/">
            <img src="https://yt3.ggpht.com/a/AATXAJwHM242C6zlkiNyKhjc0jRq9WOcz0LdBMU-UHhF9Q=s900-c-k-c0x00ffffff-no-rj" alt="logo" />
          </LogoLink>
          <NavLinks key={1}>
            <NavLink href="/#">Accueil</NavLink>
            <NavLink href="/rooms">Salles</NavLink>
            {!this.state.isAnonymous &&
              (
                <NavLink href="/my-issues">Vos tickets</NavLink>
              )
            }

            {
              this.props.rank && this.props.rank !== "Utilisateur" &&
              <DashboardLink href="/dashboard">Dashboard</DashboardLink>
            }
            {
              this.props.rank !== undefined &&
              <AccountLink onClick={() => this.handleClickOpenModal()}>Mon compte</AccountLink>
            }
            {
              this.state.isAnonymous ?
                (
                  <DisconnectLink onClick={this.props.handleDesactivateAnonymous} css={false && tw`rounded-full`}>Se connecter</DisconnectLink>
                ) :
                (
                  <DisconnectLink onClick={() => this.handleDisconnectNav()} css={false && tw`rounded-full`}>Se déconnecter</DisconnectLink>
                )
            }
          </NavLinks>
        </DesktopNavLinks>
        <NavBarMobile {...this.props} isAnonymous={this.state.isAnonymous} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect} />
      </Header>
    )
  }
}

export default withUnmounted(NavBarClass)

const collapseBreakPointCssMap = {
  sm: {
    mobileNavLinks: tw`sm:hidden`,
    desktopNavLinks: tw`sm:flex`,
    mobileNavLinksContainer: tw`sm:hidden`
  },
  md: {
    mobileNavLinks: tw`md:hidden`,
    desktopNavLinks: tw`md:flex`,
    mobileNavLinksContainer: tw`md:hidden`
  },
  lg: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  },
  xl: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  }
};
