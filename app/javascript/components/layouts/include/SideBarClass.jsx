import React, { Component, Fragment } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import FeatherIcon from 'feather-icons-react';

const Container = tw.div`w-64 absolute h-screen inset-y-0 left-0 bg-gray-100`
const Nav = tw.nav`mt-10`
const ButtonNav = tw.button`w-full flex justify-between items-center py-3 px-6 text-gray-600 cursor-pointer hover:bg-gray-200 hover:text-gray-700 focus:outline-none`
const Span = tw.span`flex items-center`
const SubNav = tw.div`bg-gray-200`
const ButtonSubNav = tw.button`py-2 px-16 block text-sm text-gray-600 hover:bg-orange-400 hover:text-white`

const ContainerBack = tw.div`absolute bottom-0 my-8`
const LinkBack = tw.a`flex items-center py-2 px-8 text-gray-700 hover:text-gray-600`

class SideBarClass extends Component{
    constructor(props){
        super(props)
        this.state = {
            openContent: "",
            isOpen: false
        }
    }

    handleClickTab = (element) => {
        this.props.modifyElementRender(element);
    }

    handleOpenTab = (content) => {
        const {isOpen, openContent} = this.state

        if(content === openContent){
            this.setState({openContent: content, isOpen: !isOpen})
        }else{
            this.setState({openContent: content, isOpen: true})
        }
    } 

    render(){
        const {openContent, isOpen} = this.state;
        const {rank} = this.props;

        return(
            <Container>
                <Nav>
                    {rank === "Administrateur" && 
                        <Fragment>
                            <div>
                                <ButtonNav>
                                    <Span onClick={() => this.handleOpenTab("home")}>  
                                        <FeatherIcon tw="h-5 w-5" icon="home"/>
                                        <span tw="mx-4 font-medium">Accueil</span>
                                    </Span>

                                    <span>
                                    {isOpen && openContent === "home" ?
                                        (
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-down"/>
                                        ):(
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-right"/>
                                        )
                                    }
                                    </span>
                                </ButtonNav>
                                {isOpen && openContent === "home" &&
                                    <SubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageHelpers")}>Gérer catégorie aide</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageSmtp")}>Gérer serveur SMTP</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageDomains")}>Gérer les domaines</ButtonSubNav>
                                    </SubNav>
                                }
                            </div>
                            <div>
                                <ButtonNav>
                                    <Span onClick={() => this.handleOpenTab("map")}>  
                                        <FeatherIcon tw="h-5 w-5" icon="map"/>
                                        <span tw="mx-4 font-medium">Plan</span>
                                    </Span>

                                    <span>
                                    {isOpen && openContent === "map" ?
                                        (
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-down"/>
                                        ):(
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-right"/>
                                        )
                                    }
                                    </span>
                                </ButtonNav>
                                {isOpen && openContent === "map" &&
                                    <SubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("mapRooms")}>Position salle</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageRooms")}>Gérer les salles</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageFloors")}>Gérer les étages</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageMaterials")}>Gérer les matériels</ButtonSubNav>
                                    </SubNav>
                                }
                            </div>
                            <div>
                                <ButtonNav>
                                    <Span onClick={() => this.handleOpenTab("user")}>  
                                        <FeatherIcon tw="h-5 w-5" icon="user"/>
                                        <span tw="mx-4 font-medium">Utilisateurs</span>
                                    </Span>

                                    <span>
                                    {isOpen && openContent === "user" ?
                                        (
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-down"/>
                                        ):(
                                            <FeatherIcon tw="h-4 w-4" icon="chevron-right"/>
                                        )
                                    }
                                    </span>
                                </ButtonNav>
                                {isOpen && openContent === "user" &&
                                    <SubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageUsers")}>Gérer les utilisateurs</ButtonSubNav>
                                    </SubNav>
                                }
                            </div>
                        </Fragment>
                    }
                    
                    <div>
                        <ButtonNav>
                            <Span onClick={() => this.handleOpenTab("issue")}>  
                                <FeatherIcon tw="h-5 w-5" icon="file-text"/>
                                <span tw="mx-4 font-medium">Tickets</span>
                            </Span>
                            <span>
                            {isOpen && openContent === "issue" ?
                                (
                                    <FeatherIcon tw="h-4 w-4" icon="chevron-down"/>
                                ):(
                                    <FeatherIcon tw="h-4 w-4" icon="chevron-right"/>
                                )
                            }
                            </span>
                        </ButtonNav>
                        {isOpen && openContent === "issue" &&
                            <SubNav>
                                <ButtonSubNav onClick={() => this.handleClickTab("showIssues")}>Gérer les tickets</ButtonSubNav>
                                {rank === "Administrateur" && 
                                    <Fragment>
                                        <ButtonSubNav onClick={() => this.handleClickTab("manageStates")}>Gérer statuts</ButtonSubNav>
                                        <ButtonSubNav onClick={() => this.handleClickTab("managePriorities")}>Gérer priorités</ButtonSubNav>
                                    </Fragment>
                                }
                            </SubNav>
                        }
                    </div>
                    
                </Nav>
                <ContainerBack>
                    <LinkBack href="/">
                        <span>Retour à l'accueil</span>
                    </LinkBack>
                </ContainerBack>
            </Container>
        )
    }
}

export default SideBarClass