import React, { Component } from 'react'
import tw from "twin.macro";
import { css } from "styled-components/macro";
import Logo from '../../../../assets/images/logo-CESI.png'

const Container = tw.div`relative bg-gray-900 text-gray-700 -mb-8 -mx-8 px-8 py-20 lg:py-24`;
const Content = tw.div`max-w-screen-xl mx-auto relative z-10`;

const ThreeColRow = tw.div`flex flex-col md:flex-row items-center justify-between`;

const LogoContainer = tw.div`flex items-center justify-center md:justify-start`;
const LogoImg = tw.img`w-96`;

const CopywrightNotice = tw.p`text-center text-sm sm:text-base mt-8 md:mt-0 font-medium text-gray-500`;

class FooterClass extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Container>
                <Content>
                    <ThreeColRow>
                        <LogoContainer>
                            <LogoImg src={Logo} />
                        </LogoContainer>
                        <CopywrightNotice>&copy; 2020 CESI RIL. Tout droits réservés.</CopywrightNotice>
                    </ThreeColRow>
                </Content>
            </Container>
        )
    }
}

export default FooterClass