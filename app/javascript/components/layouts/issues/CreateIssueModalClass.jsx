import React, { Component, Fragment } from 'react'
import FeatherIcon from 'feather-icons-react';
import tw from "twin.macro";
import styled from "styled-components";
import AxiosHelper from '../../utils/AxiosHelper';
import Axios from 'axios';
import { withAlert } from 'react-alert'

const Card = tw.div`h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;

const SecondaryInfoContainer = tw.div`flex flex-col sm:flex-row mt-2 sm:mt-4`;
const IconWithText = tw.div`flex items-center mr-6 my-2 sm:my-0`;
const IconContainer = styled.div`
  ${tw`inline-block rounded-full p-2 bg-gray-700 text-gray-100`}
  svg {
    ${tw`w-3 h-3`}
  }
`;
const Text = tw.div`ml-2 text-sm font-semibold text-gray-800`;

const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextArea = tw.textarea`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class CreateIssue extends Component{
    constructor(props){
        super(props)
        this.state = {
            material_id: 0,
            priority_id: 0,
            title: "",
            description: "",
            priorities: [],
            loaded: false
        }
    }

    componentDidMount(){
        this.loadPriorities()
    }

    loadPriorities = () => {
        AxiosHelper()
        Axios.get('/api/priorities')
        .then(response => {
            this.setState({priorities: response.data.data, loaded: true})
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const {description, priority_id, title, material_id} = this.state
        let issue = {
          description: description,
          priority_id: priority_id,
          title: title,
          material_id: material_id,
          member_id: this.props.member_id,
          anonyme: this.props.isAnonymous
        }

        AxiosHelper()
        Axios.post('/api/issues', {issue})
        .then(response => {
          if (response.data.error) {
            this.props.alert.error(response.data.error)
            this.handleClose()
          }else{
            this.props.alert.success("Nouveau ticket créer.")
            this.handleClose()
          }
          
        })
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleClose = () => {
        this.props.handleCloseModal()
    }

    render(){
        const {priorities, loaded, title, description} = this.state
        return(
            <Card>
                {loaded && 
                        <FormContainer>
                            <Form onSubmit={this.handleSubmit}>
                                <TextInfo>
                                    <TitleReviewContainer>
                                        <Title>Nouveau ticket</Title>
                                        <CloseInfo onClick={this.handleClose}>
                                            <FeatherIcon icon="x"/>
                                        </CloseInfo>
                                    </TitleReviewContainer>
                                    <SecondaryInfoContainer>
                                        <IconWithText>
                                            <IconContainer>
                                                <FeatherIcon icon="map"/>
                                            </IconContainer>
                                            <Text>{this.props.room_name}</Text>
                                        </IconWithText>
                                        <IconWithText>
                                            <IconContainer>
                                                <FeatherIcon icon="user"/>
                                                </IconContainer>
                                                <Text>{this.props.member_name}</Text>
                                        </IconWithText>
                                    </SecondaryInfoContainer>
                                    <Description>
                                       <Select name="material_id" id="material" onChange={this.handleChange}>
                                            <option value="0">Choisir le matériel</option>
                                            { this.props.materials.map(material => 
                                                <option key={material.id} value={material.id}>{material.attributes.name}</option>
                                            )}
                                        </Select>
                                        <Select name="priority_id" id="priority" onChange={this.handleChange}>
                                            <option value="0">Choisir la priorité</option>
                                            { priorities.map(priority => 
                                                <option key={priority.id} value={priority.id}>{priority.attributes.name}</option>
                                            )}
                                        </Select>
                                        <Input
                                            placeholder="Titre du ticket"
                                            type="text"
                                            name="title"
                                            value={title}
                                            onChange={this.handleChange}
                                        />
                                        <TextArea 
                                            name="description" 
                                            placeholder="Description du problème"
                                            value={description}
                                            onChange={this.handleChange}
                                        />
                                    </Description>
                                </TextInfo>
                                <PrimaryButton type="submit">Valider</PrimaryButton>
                            </Form>
                        </FormContainer>
                    }
            </Card>
        )
    }
}

export default withAlert()(CreateIssue)