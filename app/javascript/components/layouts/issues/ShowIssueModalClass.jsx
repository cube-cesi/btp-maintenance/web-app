import React, { Component, Fragment } from 'react'
import FeatherIcon from 'feather-icons-react';
import tw from "twin.macro";
import styled from "styled-components";
import AxiosHelper from '../../utils/AxiosHelper';
import { Subheading as SubheadingBase } from "../misc/Headings";
import Axios from 'axios';
import Moment from 'react-moment';
import { Scrollbars } from 'rc-scrollbars';

const Card = tw.div`lg:mx-4 bg-white xl:mx-8 max-w-sm flex flex-col h-full `;
const Details = tw.div`p-6 rounded border-2 border-gray-500 flex-1 flex flex-col items-center text-center lg:block lg:text-left`;
const MetaContainer = tw.div`flex items-center`;
const Meta = styled.div`
  ${tw`text-secondary-100 font-medium text-sm flex items-center leading-none mr-6 last:mr-0`}
  svg {
    ${tw`w-4 h-4 mr-1`}
  }
`;
const TitleReviewContainer = tw.div`flex mb-2 flex-col sm:flex-row sm:justify-between sm:items-center`;
const TitleReview = tw.h5`text-2xl font-bold`;

const Title = tw.h5`mt-4 leading-snug font-bold text-lg`;
const Description = tw.p`mt-2 text-sm text-secondary-700`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;
const Subpart = tw.span`text-orange-500`
const Divider = tw.div`my-4 border-b-2 border-gray-300 w-full`;
const Subheading = tw(SubheadingBase)`mb-4 text-orange-500 text-left`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const TextArea = tw.textarea`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const PrimaryButton = tw.button`px-2 rounded bg-red-400 text-gray-100 hocus:bg-red-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg w-16 py-3 `;

class ShowIssueModalClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            issue: [],
            comments: [],
            commentaryPost: "",
            loaded: false
        }
    }

    componentDidMount(){
        this.getInformationsIssue()
    }

    handleClose = () => {
        this.props.handleCloseModal()
    }

    getInformationsIssue = () => {
        AxiosHelper();

        Axios.get(`/api/issues/${this.props.issue_id}`)
        .then(response => {
            this.setState({issue: response.data.data, comments: response.data.comments, loaded: true})
        })
    }

    handleSubmitComment = (event) => {
        event.preventDefault()

        AxiosHelper();

        let comment = {
            member_id: this.props.member.id,
            issue_id: this.props.issue_id,
            description: this.state.commentaryPost
        }
        Axios.post('/api/comments', {comment})
        .then(response => {
            if(!response.error){
                this.getInformationsIssue()
                this.setState({commentaryPost: ""})
            }
        })
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    render(){
        const {issue, loaded, comments, commentaryPost} = this.state;

        return(
            <Card>
                {loaded &&
                    <Details>
                        <TitleReviewContainer>
                            <TitleReview>Détails ticket - {issue.materialName}</TitleReview>
                            <CloseInfo onClick={this.handleClose}>
                                <FeatherIcon icon="x"/>
                            </CloseInfo>
                        </TitleReviewContainer>
                        <MetaContainer>
                            <Meta>
                                <FeatherIcon icon="user"/>
                                <div>{issue.memberFirstname !== null ? (issue.memberFirstname + " " + issue.memberLastname):("Anonyme")}</div>
                            </Meta>
                            <Meta>
                                <FeatherIcon icon="alert-circle"/>
                                <div>{issue.priority.name}</div>
                            </Meta>
                            <Meta>
                                <FeatherIcon icon="map"/>
                                <div>{issue.roomName}</div>
                            </Meta>
                            <Meta>
                                <FeatherIcon icon="tag"/>
                                <div>{issue.state.name}</div>
                            </Meta>
                        </MetaContainer>
                        <Title>
                            <Subpart>Titre:</Subpart> {issue.title}
                        </Title>
                        <Description>
                            {issue.description}
                        </Description>
                        {issue.state.name !== "Fermé" &&
                            <Fragment>
                                <Divider/>
                                <Subheading>Commentaires: </Subheading>
                                {this.props.member.id ?
                                (
                                    <FormContainer>
                                        <Form onSubmit={this.handleSubmitComment}>
                                            <TextArea
                                                name="commentaryPost"
                                                onChange={this.handleChange}
                                                placeholder="Votre commentaire"
                                                value={commentaryPost}
                                            />
                                            <PrimaryButton type="submit">Poster</PrimaryButton>
                                        </Form>
                                    </FormContainer>
                                ):(
                                    null
                                )}
                            </Fragment>
                        }
                        <Scrollbars style={{ width: 340, height: 170 }}>
                        {comments.map(comment => (
                            <Fragment key={comment.id}>
                                <Divider/>
                                <MetaContainer>
                                    <Meta>
                                        <FeatherIcon icon="user"/>
                                        <div>{comment.memberFirstname + " " + comment.memberLastname}</div>
                                    </Meta>
                                    <Meta>
                                        <FeatherIcon icon="calendar"/>
                                        <Moment format="DD/MM/YYYY" date={comment.created_at}/>
                                    </Meta>
                                </MetaContainer>
                                <Description>
                                    {comment.description}
                                </Description>
                            </Fragment>
                        ))}
                        </Scrollbars>
                        
                    </Details>
                    
                }
            </Card>
        )
    }
}

export default ShowIssueModalClass