import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../misc/Container";
import { SectionHeading } from "../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import ShowIssuesClass from './ShowIssuesClass'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)``;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-orange-400! text-gray-100!`}
  }
`;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class TabIssueClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            states: [],
            activeTabName: "",
            activeTabId: 1,
            showContent: false
        }
    }

    componentDidMount(){
        AxiosHelper()
        axios.get('/api/states')
        .then(res => {            
            this.setState({ 
                states: res.data.data,
                activeTabName: res.data.data[0].attributes.name,
                activeTabId: res.data.data[0].id,
                showContent: true,
            });
          })
          .catch(error => this.props.alert.error("erreur : ", error))
    }

    handleClickTab = (tabName, tabId) => {
        this.setState(
            {
                activeTabName: tabName,
                activeTabId: tabId 
            })
            this.render()
    }

    render(){
        const { activeTabName, activeTabId, states, showContent } = this.state
        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Vos <HighlightedText>tickets</HighlightedText></Header>
                        <TabsControl>
                        { states.map(state =>
                            <TabControl key={state.id} active={activeTabName === state.attributes.name} onClick={() => this.handleClickTab(state.attributes.name, state.id)}>
                                {state.attributes.name}
                            </TabControl>
                        )}
                        </TabsControl>
                    </HeaderRow>
                    {showContent ? 
                        (
                            <ShowIssuesClass member={this.props.member} activePlanId={activeTabId}/>
                        ):
                        (
                            null
                        )
                    }
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default TabIssueClass