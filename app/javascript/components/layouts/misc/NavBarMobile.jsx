import React, { useState } from "react";
import { useAnimation, useCycle, motion } from "framer-motion";
import FeatherIcon from 'feather-icons-react';
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import AxiosHelper from "../../utils/AxiosHelper";
import axios from 'axios';

const collapseBreakPointCssMap = {
  sm: {
    mobileNavLinks: tw`sm:hidden`,
    desktopNavLinks: tw`sm:flex`,
    mobileNavLinksContainer: tw`sm:hidden`
  },
  md: {
    mobileNavLinks: tw`md:hidden`,
    desktopNavLinks: tw`md:flex`,
    mobileNavLinksContainer: tw`md:hidden`
  },
  lg: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  },
  xl: {
    mobileNavLinks: tw`lg:hidden`,
    desktopNavLinks: tw`lg:flex`,
    mobileNavLinksContainer: tw`lg:hidden`
  }
};

const NavLinks = tw.div`inline-block`;

const NavLink = tw.a`
  text-lg my-2 lg:text-sm lg:mx-6 lg:my-0
  font-semibold tracking-wide transition duration-300
  pb-1 border-b-2 border-transparent hover:border-orange-400 hocus:text-orange-400
`;

const DisconnectLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-red-500 text-gray-100
  hocus:bg-red-700 hocus:text-gray-200 focus:shadow-outline
  border-b-0
`;

const LogoLink = styled(NavLink)`
  ${tw`flex items-center font-black border-b-0 text-2xl! ml-0!`};

  img {
    ${tw`w-10 mr-3`}
  }
`;

const MobileNavLinksContainer = tw.nav`lg:hidden flex flex-1 items-center justify-between`;
const NavToggle = tw.button`
  lg:hidden z-20 focus:outline-none hocus:text-primary-500 transition duration-300
`;
const MobileNavLinks = motion.custom(styled.div`
  ${tw`lg:hidden z-10 fixed top-0 inset-x-0 mx-4 my-6 p-8 border text-center rounded-lg text-gray-900 bg-white`}
  ${NavLinks} {
    ${tw`flex flex-col items-center`}
  }
`);

const DashboardLink = tw(NavLink)`
  lg:mx-0
  px-8 py-3 rounded bg-gray-800 text-gray-100
  hocus:bg-gray-900 hocus:text-gray-200 focus:shadow-outline 
  border-b-0 cursor-pointer ml-2!
`;

const NavBarMobile = (props) => {
  const [showNavLinks, setShowNavLinks] = useState(false);
  const [isAnonymous, setAnonymous] = useState(false);
  const [x, cycleX] = useCycle("0%", "150%");
  const animation = useAnimation();

  const toggleNavbar = () => {
    setShowNavLinks(!showNavLinks);
    animation.start({ x: x, display: "block" });
    cycleX();
  };

  AxiosHelper();
  axios.get('/api/auth/is-anonymous')
    .then(response => {
      setAnonymous(response.data.anonymous);
    })

  const collapseBreakpointCss = collapseBreakPointCssMap["lg"];

  return (
    <MobileNavLinksContainer css={collapseBreakpointCss.mobileNavLinksContainer}>
      <LogoLink href="/">
        <img src="https://yt3.ggpht.com/a/AATXAJwHM242C6zlkiNyKhjc0jRq9WOcz0LdBMU-UHhF9Q=s900-c-k-c0x00ffffff-no-rj" alt="logo" />
      </LogoLink>
      <MobileNavLinks initial={{ x: "150%", display: "none" }} animate={animation} css={collapseBreakpointCss.mobileNavLinks} key={1}>
        <NavLink href="/#">Accueil</NavLink>
        <NavLink href="/rooms">Salles</NavLink>
        {!props.isAnonymous &&
          (
            <NavLink href="/my-issues">Vos tickets</NavLink>
          )
        }

        {props.isAnonymous ?
          (
            <DisconnectLink onClick={props.handleDesactivateAnonymous} css={false && tw`rounded-full`}>Se connecter</DisconnectLink>
          ) :
          (
            <DisconnectLink onClick={props.handleDisconnect} css={false && tw`rounded-full`}>Se déconnecter</DisconnectLink>
          )}
        <DashboardLink href="/dashboard">Dashboard</DashboardLink>
      </MobileNavLinks>
      <NavToggle onClick={toggleNavbar} className={showNavLinks ? "open" : "closed"}>
        {showNavLinks ? <FeatherIcon icon="close" tw="w-6 h-6" /> : <FeatherIcon icon="menu" tw="w-6 h-6" />}
      </NavToggle>
    </MobileNavLinksContainer>
  )
}

export default NavBarMobile