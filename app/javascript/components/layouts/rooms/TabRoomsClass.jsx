import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../misc/Container";
import { SectionHeading } from "../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import ShowRoomCardClass from "./ShowRoomCardClass";

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)``;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-orange-400! text-gray-100!`}
  }
`;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;
const ThreeColumn = tw.div`flex flex-col items-center lg:items-stretch lg:flex-row flex-wrap mt-4`;
const Column = tw.div` lg:w-1/3 mb-8`;

class TabRoomsClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            floors: [],
            activeTabName: "",
            activeTabId: 0,
            rooms: [],
            showContent: false
        }
    }

    componentDidMount(){
        this.getFloors();
    }

    getFloors(){
        AxiosHelper()
        axios.get('/api/floors.json')
        .then(response =>{
            this.setState(
                {
                    floors: response.data,
                    activeTabName: response.data[0].name,
                    activeTabId: response.data[0].id,
                    showContent: true, 
                })

            this.getRoomsByFloor(response.data[0].id)
        })
        .catch(error => this.props.alert.error("erreur : ", error))
    }

    getRoomsByFloor(idFloor){
        AxiosHelper()

        axios.get(`/api/rooms/where/${idFloor}`)
        .then(response => {
            this.setState({rooms: response.data.data})
        })
    }

    handleClickTab = (tabName, tabId) => {
        this.setState(
            {
                activeTabName: tabName, 
                activeTabId: tabId,
            })
        this.getRoomsByFloor(tabId)
    }

    render(){
        const { floors, activeTabName, activeTabId, showContent, rooms } = this.state
        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Choisir votre <HighlightedText>étage</HighlightedText></Header>
                        <TabsControl>
                        { floors.map(floor =>
                            <TabControl key={floor.id} active={activeTabName === floor.name} onClick={() => this.handleClickTab(floor.name, floor.id)}>
                                {floor.name}
                            </TabControl>
                        )}
                        </TabsControl>
                    </HeaderRow>
                    {showContent ? 
                        (
                            <ThreeColumn>
                                {rooms.map(room =>
                                    <Column key={room.id}>
                                        <ShowRoomCardClass name_url={room.attributes.name_url}/>
                                    </Column>
                                )}
                            </ThreeColumn>
                        ):
                        (
                            null
                        )
                    }
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default TabRoomsClass