import React, { Component, Fragment } from 'react'
import {Redirect}  from 'react-router-dom'
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import styled from 'styled-components';
import tw from 'twin.macro';
import { css } from "styled-components/macro";
import { SectionHeading as HeadingTitle, Subheading } from "../misc/Headings";
import FeatherIcon from 'feather-icons-react'
import Modal from 'react-modal';
import ShowIssueModalClass from "../issues/ShowIssueModalClass";
const Card = tw.div`lg:mx-4 xl:mx-8 max-w-sm flex flex-col h-full `;
const PrimaryButton = tw.button`px-8 mt-4 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;
const Details = tw.div`p-6 rounded border-2 border-gray-500 flex-1 flex flex-col items-center text-center lg:block lg:text-left`;
const MetaContainer = tw.div`flex items-center`;
const Meta = styled.div`
  ${tw`text-secondary-100 font-medium text-sm flex items-center leading-none mr-6 last:mr-0`}
  svg {
    ${tw`w-4 h-4 mr-1`}
  }
`;

const Title = tw.h5`mt-4 leading-snug font-bold text-lg`;
const Description = tw.div`mt-2 text-sm text-secondary-100`;
const Text = tw.div`ml-2 text-sm font-semibold text-gray-800`;

class ShowRoomCardClass extends Component{
    constructor(props){
        super(props)
        this.state = {
            nameRoom: "",
            nameFloor: "",
            numbersIssues: 0,
            materialsList: []
        }
    }

    componentDidMount(){
        this.updateInformationsRoom()
    }

    updateInformationsRoom = () => {
        AxiosHelper()

        axios.get(`/api/rooms/informations/${this.props.name_url}`)
        .then(roomsResponse => {            
            this.setState(
                {
                    nameRoom: roomsResponse.data.roomName, 
                    materialsList: roomsResponse.data.materials, 
                    nameFloor: roomsResponse.data.floorName, 
                    numbersIssues: roomsResponse.data.issueCounters
                }
            )
        })
    }

    render(){
        const {nameFloor, nameRoom, numbersIssues, materialsList} = this.state;
        return(
            <Card>
                <Details>
                    <MetaContainer>
                        <Meta>
                            <FeatherIcon icon="map"/>
                            <div>{nameFloor}</div>
                        </Meta>
                        <Meta>
                            <FeatherIcon icon="file-text"/>
                            <div>{numbersIssues < 2 ? (numbersIssues + " ticket"):(numbersIssues + " tickets")}</div>
                        </Meta>
                    </MetaContainer>
                    <Title>{nameRoom}</Title>
                    <Description>Matériels: 
                    {materialsList.map(material => 
                        <Text key={material.id}>- {material.quantity} {material.name}</Text>
                    )}</Description>
                    <a href={'/rooms/' + this.props.name_url}><PrimaryButton>Accéder</PrimaryButton></a>
                </Details>
            </Card>
        )
    }
}
export default ShowRoomCardClass