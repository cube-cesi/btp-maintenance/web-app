import React, { Component } from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { motion } from "framer-motion";
import { css } from "styled-components/macro";
import { SectionHeading, Subheading as SubheadingBase } from "../misc/Headings";
import { Container, ContentWithPaddingXl } from "../misc/Container";
import AxiosHelper from "../../utils/AxiosHelper";
import axios from "axios";
import Moment from 'react-moment';
import 'moment-timezone';
import FeatherIcon from 'feather-icons-react';
import Modal from 'react-modal';
import CreateIssue from "../issues/CreateIssueModalClass";
import { withAlert } from 'react-alert'
import ShowMaterialContactModal from "./ShowMaterialContactModal";
import { replace } from "feather-icons";
import ShowIssueModalClass from "../issues/ShowIssueModalClass";
import { Redirect } from "react-router";

const Row = tw.div`flex flex-col lg:flex-row -mb-10`;
const RowIssues = tw.div`flex flex-col lg:flex-row `;

const HeadingContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`
const IconContainer = styled.div`
${tw`flex items-center cursor-pointer sm:ml-4 mt-2 sm:mt-0`}
svg {
  ${tw`w-6 h-6 fill-current`}
}
`;

const Heading = tw(SectionHeading)`text-left lg:text-4xl xl:text-5xl`;
const HeadingNoContent = tw(SubheadingBase)`text-center text-red-500 mb-4`;
const Subheading = tw(SubheadingBase)`mb-4 cursor-pointer text-left`;

const InformationsContainer = tw.div`lg:w-2/3 mr-20`;
const IssueContainer = tw.div`mt-12 flex flex-wrap lg:flex-col flex flex-col sm:flex-row sm:justify-between lg:justify-start`;
const Issue = tw(motion.a)`flex justify-between mb-10 max-w-none w-full sm:w-1/2 lg:w-auto sm:odd:pr-12 lg:odd:pr-0 mr-0 block sm:max-w-sm cursor-pointer mb-16 last:mb-0 sm:mb-0 sm:odd:mr-8 lg:mr-8 xl:mr-16`;
const State = styled.button(tw`bg-green-400 text-white font-bold rounded h-12 w-20 flex-shrink-0 `);
const Title = tw.h5`mt-6 text-xl font-bold transition duration-300 group-hover:text-orange-400 text-base xl:text-lg mt-0 mr-4 lg:max-w-xs`;
const AuthorName = tw.h6`font-semibold text-lg mt-3 text-sm text-secondary-100 font-normal leading-none`;
const IssueTextContainer = tw.div`sm:w-2/3`
const IssuesStateContainer = tw.div`sm:w-1/3`

const Divider = tw.div`mt-4 mb-4 border-b-2 border-gray-300 w-full`;

const IssuesOpensContainer = tw.div`mt-24 lg:mt-0 lg:w-1/3`

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

//style for material

const Materials = tw.div`mt-6 sm:-mr-8 flex flex-wrap`;
const MaterialContainer = styled.div`
  ${tw`mt-10 w-full sm:w-1/2 lg:w-1/3 sm:pr-16`}
  ${props =>
        props.featured &&
        css`
      ${tw`w-full!`}
      ${Material} {
        ${tw`sm:flex-row! h-full sm:pr-4`}
      }
      ${Info} {
        ${tw`sm:-mr-4 sm:pl-8 sm:flex-1 sm:rounded-none sm:rounded-r-lg sm:border-t-2 sm:border-l-0`}
      }
      ${Description} {
        ${tw`text-sm mt-3 leading-loose text-gray-600 font-medium`}
      }
    `}
`;
const Material = tw.div`flex flex-col bg-gray-100 rounded-lg`;
const Info = tw.div`p-8 border-2 rounded-lg`;
const Quantity = tw.div`uppercase text-secondary-500 text-xs font-bold tracking-widest leading-loose after:content after:block after:border-b-2 after:border-orange-500 after:w-8`;
const CreationDate = tw.div`mt-4 uppercase text-gray-600 italic font-semibold text-xs`;
const MaterialName = tw.div`mt-1 font-black text-2xl text-gray-900 group-hover:text-primary-500 transition duration-300`;
const Description = tw.div``;

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'transparent',
        border: '0px'
    }
};

class ShowRoomInformationsClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            issues: [],
            room: [],
            materials: [],
            loaded: false,
            showModal: false,
            modalIssue: false,
            modalCreateIssue: false,
            modalIssueId: 0,
            activateNotification: false
        }

        this.handleClickNewIssue = this.handleClickNewIssue.bind(this);
        this.handleClickContactMaterial = this.handleClickContactMaterial.bind(this);
        this.handleClickShowIssue = this.handleClickShowIssue.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    componentDidMount() {
        Modal.setAppElement('body')

        this.getIssues()
    }

    handleClickNewIssue() {
        this.setState({ showModal: true, modalIssue: true, modalCreateIssue: true });
    }

    handleClickContactMaterial() {
        this.setState({ showModal: true, modalIssue: false });
    }

    handleClickShowIssue = (id_issue) => {
        this.setState({ showModal: true, modalIssue: true, modalCreateIssue: false, modalIssueId: id_issue });
    }

    handleCloseModal = () => {
        this.getIssues()
        this.setState({ showModal: false });
    }

    getIssues() {
        AxiosHelper();

        axios.get(`/api/issues/room/${this.props.name_url}`)
            .then(response => {
                this.setState({ issues: response.data })
            })

        axios.get(`/api/rooms/${this.props.name_url}`)
            .then(response => {
                this.setState({ room: response.data.data, materials: response.data.included, loaded: true })
                this.getUserActivateNotification(response.data.data.id)
            })
    }

    getUserActivateNotification(room_id) {
        AxiosHelper()

        axios.get(`/api/rooms/notified/${room_id}`)
            .then(response => {
                this.setState({ activateNotification: response.data.notified })
            })
    }

    handleActivateNotification = () => {
        let room_member = {
            room_id: this.state.room.id,
            member_id: this.props.member.id
        }

        if (this.state.activateNotification) {
            axios.post('/api/room_members/delete', { room_member })
                .then(response => {
                    if (!response.error) {
                        this.setState({ activateNotification: false })
                        this.props.alert.info("Notification désactivé.")
                    }
                })
        } else {
            axios.post('/api/room_members', { room_member })
                .then(response => {
                    if (!response.error) {
                        this.setState({ activateNotification: true })
                        this.props.alert.info("Notification activé.")
                    }
                })
        }
    }

    render() {
        const { issues, room, loaded, materials, showModal, modalIssue, modalCreateIssue, modalIssueId } = this.state;
        return (
            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <Modal
                            isOpen={showModal}
                            style={customStyles}
                        >
                            {modalIssue ? (
                                modalCreateIssue ? (
                                    <CreateIssue handleCloseModal={this.handleCloseModal} isAnonymous={this.props.isAnonymous} room_name={room.attributes.name} member_name={this.props.member.id ? (this.props.member.firstname + " " + this.props.member.lastname) : ("Anonyme")} member_id={this.props.member.id ? (this.props.member.id) : (0)} materials={materials} />
                                ) : (
                                        <ShowIssueModalClass handleCloseModal={this.handleCloseModal} member={this.props.member} issue_id={modalIssueId} />
                                    )
                            ) : (
                                    <ShowMaterialContactModal handleCloseModal={this.handleCloseModal} isAnonymous={this.props.isAnonymous} room_name={room.attributes.name} member_name={this.props.member.id ? (this.props.member.firstname + " " + this.props.member.lastname) : ("Anonyme")} member_id={this.props.member.id ? (this.props.member.id) : (0)} />
                                )}
                        </Modal>
                        <Row>
                            <InformationsContainer>
                                <HeadingContainer>
                                    <Heading>Informations <HighlightedText>{room.attributes.name.toLowerCase()}</HighlightedText></Heading>
                                    {this.props.member.id ? (
                                        this.state.activateNotification ? (
                                            <IconContainer tw="text-red-500">
                                                <FeatherIcon onClick={this.handleActivateNotification} icon="bell-off" />
                                            </IconContainer>
                                        ) : (
                                                <IconContainer tw="text-green-500">
                                                    <FeatherIcon onClick={this.handleActivateNotification} icon="bell" />
                                                </IconContainer>
                                            )
                                    ) : (null)}
                                    <IconContainer tw="text-green-500">
                                        <FeatherIcon onClick={(e) => {
                                            e.preventDefault();
                                            window.location.href = `https://api.qrserver.com/v1/create-qr-code/?data=${this.props.name_url}&size=500x500`;
                                        }} icon="tag" />
                                    </IconContainer>
                                </HeadingContainer>
                                <Subheading onClick={this.handleClickContactMaterial}>Matériel non référencé ?</Subheading>
                                <Materials>
                                    {materials.map(material => (
                                        <MaterialContainer key={material.id}>
                                            <Material>
                                                <Info>
                                                    <Quantity>Quantité: {material.attributes.quantity}</Quantity>
                                                    <CreationDate>Date de mise en ligne: <Moment format="DD/MM/YYYY" date={material.attributes.created_at} /></CreationDate>
                                                    <MaterialName>{material.attributes.name}</MaterialName>
                                                </Info>
                                            </Material>
                                        </MaterialContainer>
                                    ))}
                                </Materials>
                            </InformationsContainer>
                            <IssuesOpensContainer>
                                <Heading>Tickets en cours </Heading>
                                <Subheading onClick={this.handleClickNewIssue}>Nouveau ticket ?</Subheading>
                                <IssueContainer>
                                    {issues.length > 0 ? (
                                        issues.map(issue => (
                                            <Issue onClick={() => this.handleClickShowIssue(issue.id)} key={issue.id} className="group">
                                                <RowIssues>
                                                    <IssueTextContainer>
                                                        <Title>{issue.materialName} - {issue.title}</Title>
                                                        <AuthorName>de {issue.firstname !== null ? (issue.firstname + " " + issue.lastname) : ("Anonyme")} le <Moment format="DD/MM/YYYY" date={issue.created_at} /></AuthorName>
                                                    </IssueTextContainer>
                                                    <IssuesStateContainer>
                                                        <State>{issue.stateName}</State>
                                                    </IssuesStateContainer>
                                                </RowIssues>
                                                <Divider />
                                            </Issue>
                                        ))
                                    ) : (
                                            <HeadingNoContent>Aucun ticket actuellement.</HeadingNoContent>
                                        )
                                    }
                                </IssueContainer>
                            </IssuesOpensContainer>
                        </Row>
                    </ContentWithPaddingXl>
                }
            </Container>
        )
    }
}

export default withAlert()(ShowRoomInformationsClass)