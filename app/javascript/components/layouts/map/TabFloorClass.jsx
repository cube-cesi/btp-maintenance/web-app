import React, { Component, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../misc/Container";
import { SectionHeading } from "../misc/Headings";
import ShowMapClass from './ShowMapClass'
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import withUnmounted from '@ishawnwang/withunmounted'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)``;
const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-orange-400! text-gray-100!`}
  }
`;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

class TabBarClass extends Component {
    hasUnmounted = false;

    constructor(props) {
        super(props)
        this.state = {
            floors: [],
            activeTabName: "",
            activeTabId: 0,
            planSource: "",
            imagePlan: null,
            showContent: false
        }
    }

    componentDidMount() {
        AxiosHelper()
        axios.get('/api/floors')
            .then(response => {
                this.setState(
                    {
                        floors: response.data,
                        activeTabName: response.data[0].name,
                        activeTabId: response.data[0].id,
                        showContent: true,
                        planSource: response.data[0].plan_source,
                        imagePlan: response.data[0].image.url
                    });
            })
            .catch(error => console.log(error));
    }

    handleClickTab = (tabName, planSource, tabId, image) => {
        try {
            this.setState(
                {
                    activeTabName: tabName,
                    activeTabId: tabId,
                    planSource: planSource,
                    imagePlan: image.url
                })
            this.setState({ showContent: true })
        } catch (error) {
            this.setState({ showContent: false })
            this.props.alert.error(error)
        }
    }

    render() {
        const { floors, activeTabName, activeTabId, planSource, showContent, imagePlan } = this.state

        return (
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Choisir votre <HighlightedText>plan</HighlightedText></Header>
                        <TabsControl>
                            {floors.map(floor =>
                                <TabControl key={floor.id} active={activeTabName === floor.name} onClick={() => this.handleClickTab(floor.name, floor.plan_source, floor.id, floor.image)}>
                                    {floor.name}
                                </TabControl>
                            )}
                        </TabsControl>
                    </HeaderRow>
                    {showContent ?
                        (
                            <ShowMapClass activePlanId={activeTabId} imageSource={planSource} imgPlan={imagePlan} />
                        ) :
                        (
                            null
                        )
                    }
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withUnmounted(TabBarClass)