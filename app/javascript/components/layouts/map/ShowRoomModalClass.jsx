import React, { Component } from 'react'
import tw from "twin.macro";
import styled from "styled-components";
import FeatherIcon from 'feather-icons-react';
import AxiosHelper from '../../utils/AxiosHelper';
import axios from 'axios'

const Card = tw.div`h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const SecondaryInfoContainer = tw.div`flex flex-col sm:flex-row mt-2 sm:mt-4`;
const IconWithText = tw.div`flex items-center mr-6 my-2 sm:my-0`;
const IconContainer = styled.div`
  ${tw`inline-block rounded-full p-2 bg-gray-700 text-gray-100`}
  svg {
    ${tw`w-3 h-3`}
  }
`;
const Text = tw.div`ml-2 text-sm font-semibold text-gray-800`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;

class ShowRoomModalClass extends Component{
    constructor(props){
        super(props)
        this.state = {
            nameRoom: "",
            nameFloor: "",
            numbersIssues: 0,
            materialsList: []
        }
    }

    componentDidMount(){
        this.updateInformationsRoom()
    }

    updateInformationsRoom = () => {
        AxiosHelper()

        axios.get(`/api/rooms/informations/${this.props.name_url}`)
        .then(roomsResponse => {            
            this.setState(
                {
                    nameRoom: roomsResponse.data.roomName, 
                    materialsList: roomsResponse.data.materials, 
                    nameFloor: roomsResponse.data.floorName, 
                    numbersIssues: roomsResponse.data.issueCounters
                }
            )
        })
    }

    handleClickClose = () => {
        this.props.handleCloseModal();
    }

    render(){

        const {nameFloor, nameRoom, numbersIssues, materialsList} = this.state;

        return(
            <Card>
              <TextInfo>
                <TitleReviewContainer>
                  <Title>{nameRoom}</Title>
                  <CloseInfo onClick={this.handleClickClose}>
                      <FeatherIcon icon="x"/>
                  </CloseInfo>
                </TitleReviewContainer>
                <SecondaryInfoContainer>
                  <IconWithText>
                    <IconContainer>
                        <FeatherIcon icon="map"/>
                    </IconContainer>
                    <Text>{nameFloor}</Text>
                  </IconWithText>
                  <IconWithText>
                    <IconContainer>
                      <FeatherIcon icon="file-text"/>
                    </IconContainer>
                    {numbersIssues < 2 ? (<Text>{numbersIssues} ticket</Text>):(<Text>{numbersIssues} tickets</Text>)}
                  </IconWithText>
                </SecondaryInfoContainer>
                <Description>Matériels: 
                    {materialsList.map(material => 
                        <Text key={material.id}>- {material.quantity} {material.name}</Text>
                    )}
                </Description>
              </TextInfo>
              <a href={'/rooms/' + this.props.name_url}><PrimaryButton>Accéder</PrimaryButton></a>
            </Card>
        )
    }
}

export default ShowRoomModalClass