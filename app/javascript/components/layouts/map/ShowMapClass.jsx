import React, { Component, Fragment } from 'react'
import {Redirect}  from 'react-router-dom'
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import styled from 'styled-components';
import tw from 'twin.macro';
import Modal from 'react-modal';
import { css } from "styled-components/macro";
import ShowRoomModalClass from './ShowRoomModalClass';

const Container = styled.div`
    position: relative;
    padding-top: 70%;
    height: 0;
    ${props => `background: url('${props.imageSource}') 0 0 no-repeat;`}
    background-size: cover;
`;

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      backgroundColor       : 'transparent',
      border                : '0px'
    }
};

const PrimaryButtonBase = tw.button``;
const ButtonRoom = styled.button`
    ${tw`font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-8 inline-block tracking-wide text-center`}
    ${props => `position: absolute; left: ${props.coordinatesX}%; top: ${props.coordinatesY}%; margin: 0; font-size: 1vw;`}
`
class ShowMapClass extends Component{
    constructor(props){
        super(props)
        this.state = {
            rooms: [],
            redirectRoom: false,
            name_url: '',
            roomClicked: [],
            showModal: false
        }

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    redirectToRoom = (room) =>{
        this.setState({redirectRoom: true, name_url: room.attributes.name_url})
    }

    handleOpenModal (room) {
        this.setState({ showModal: true, roomClicked: room.attributes.name_url });
      }
      
    handleCloseModal () {
        this.setState({ showModal: false });
    }

    componentDidMount() {
        Modal.setAppElement('body')
        AxiosHelper()
       // Récup rooms de l'étage
       axios.get('/api/rooms')
       .then(res => {
             this.setState({rooms: res.data.data})
       })
       .catch(error => console.log('api errors:', error))
    }

    render(){
        const {rooms, redirectRoom, name_url, roomClicked, showModal} = this.state
        if(redirectRoom) {
            return (
                <Redirect to={`/api/rooms/${name_url}`} />
            )
        }
        else {
            return(
                <Container imageSource={this.props.imgPlan}>
                    {   
                        rooms.filter(room => room.attributes.floor_id == this.props.activePlanId).map(room =>
                                            <ButtonRoom key={room.id} coordinatesX={room.attributes.position_x} coordinatesY={room.attributes.position_y} onClick={() => this.handleOpenModal(room)}>{room.attributes.name}</ButtonRoom>
                                 )
                    }
                    <Modal
                        isOpen={showModal}
                        style={customStyles}
                    >
                     <ShowRoomModalClass handleCloseModal={this.handleCloseModal} name_url={roomClicked}/>
                    </Modal>
                </Container>
            )
        }
    }
}
export default ShowMapClass