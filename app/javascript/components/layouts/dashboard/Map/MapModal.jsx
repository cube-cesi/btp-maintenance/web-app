import React, { Component } from 'react';
import styled from 'styled-components'
import tw from "twin.macro";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import FeatherIcon from 'feather-icons-react';
import {withAlert} from "react-alert"

const Card = tw.div`h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const PrimaryButton = tw.button`font-bold px-8 mt-2 mr-2 lg:px-10 py-3 rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 focus:shadow-outline focus:outline-none transition duration-300`;

class MapModal extends Component{
    constructor(props){
        super(props)

        this.state = {
            rooms: []
        }

        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount(){
        AxiosHelper()
        axios.get(`/api/rooms/where/${this.props.activeTabId}`)
        .then( resp => {
            this.setState({rooms: resp.data.data});
        } )
    }

    onChooseRoom = (id, nameRoom) => {
        const {x, y} = this.props.coordinates;
        AxiosHelper()
        axios.patch(`/api/rooms/${id}`, {position_x: x, position_y: y})
        this.props.handleCloseModal()
        this.props.alert.success("Position mise à jour: " + nameRoom)
    }

    render(){
        const {rooms} = this.state;

        const roomsItem = rooms.map( item => {
            return (
                <PrimaryButton onClick={() => this.onChooseRoom(item.attributes.name_url, item.attributes.name)} key={item.attributes.name_url}>{item.attributes.name}</PrimaryButton>
            )
        });

        return(
            <Card>
                <TextInfo>
                    <TitleReviewContainer>
                        <Title>Choisir votre salle: </Title>
                        <CloseInfo onClick={this.props.handleCloseModal}>
                            <FeatherIcon icon="x"/>
                        </CloseInfo>
                    </TitleReviewContainer>
                    <Description>
                        {roomsItem}
                    </Description>
                </TextInfo>
            </Card>
        )
    }
}

export default withAlert()(MapModal)