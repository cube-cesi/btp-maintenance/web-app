import React, { Component, Fragment, useState } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageRoomsClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_room_id: 0,
            modify_room_id: 0,
            modify_room_name: "",
            modify_floor_id: 0,
            new_floor_id: 0,
            new_room_name: "",
            rooms: [],
            floors: [],
            showRoomInfo: false
        }

        this.handleChooseRoom = this.handleChooseRoom.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseRoom = (event) => {
        if(event.target.value === 0){
            this.setState({showRoomInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/rooms/${event.target.value}`)
            .then(response => {
                this.setState({showRoomInfo: true, modify_room_id: response.data.data.attributes.name_url, modify_room_name: response.data.data.attributes.name, modify_floor_id: response.data.data.attributes.floor_id});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_room_id} = this.state;
        if (delete_room_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/rooms/${delete_room_id}`)
            .then(response => {
                if(!response.error){
                    this.getData()
                    this.setState({delete_room_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_room_name, new_floor_id} = this.state;
        let room = {
            name: new_room_name,
            floor_id: new_floor_id
        }

        AxiosHelper()
        axios.post(`/api/rooms`, {room})
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({new_room_name: "", new_floor_id: 0})
                this.props.alert.success("Nouvelle salle créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_room_id, modify_room_name, modify_floor_id} = this.state;
        let room = {
            name: modify_room_name,
            floor_id: modify_floor_id
        }

        AxiosHelper()
        axios.patch(`/api/rooms/${modify_room_id}`, {room})
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({modify_room_id: 0, showRoomInfo: false})
                this.props.alert.success("Salle modifiée.")
            }
        })
    }

    getData(){
        this.getRooms()
        this.getFloors()
    }

    getRooms(){
        AxiosHelper()
        axios.get('/api/rooms')
        .then(response => {
            this.setState({rooms: response.data.data})
        })
    }

    getFloors(){
        AxiosHelper();
        axios.get('/api/floors')
        .then(response => {
            this.setState({floors: response.data})
        }).catch(err => console.log(err));
    }

    render(){
        const {delete_room_id, modify_room_id, modify_room_name, modify_floor_id, new_room_name, new_floor_id, showRoomInfo, rooms, floors} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>salles</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                    <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouvelle salle</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Nom de la salle"
                                                value={new_room_name}
                                                type="text"
                                                name="new_room_name"
                                                onChange={this.handleChange}
                                            />
                                            <Select value={new_floor_id} name="new_floor_id" id="new_floor_id" onChange={this.handleChange}>
                                                <option value={0}>Choisir un étage</option>
                                                {floors.map(floor => 
                                                    <option key={floor.id} value={floor.id}>{floor.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier salle</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_room_id} onChange={this.handleChooseRoom} name="modify_room_id" id="modify_room_id">
                                                <option value={0}>Choisir une salle</option>
                                                {rooms.map(room => 
                                                    <option key={room.id} value={room.attributes.name_url}>{room.attributes.name}</option>
                                                )}
                                            </Select>
                                            {showRoomInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom de la salle"
                                                        type="text"
                                                        name="modify_room_name"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_room_name}
                                                    />
                                                    <Select defaultValue={modify_floor_id} name="modify_floor_id" id="modify_floor_id" onChange={this.handleChange}>
                                                        {floors.map(floor => 
                                                            <option key={floor.id} value={floor.id}>{floor.name}</option>
                                                        )}
                                                    </Select>
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer salle</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_room_id} onChange={this.handleChange} name="delete_room_id" id="delete_room_id">
                                                <option value={0}>Choisir une salle</option>
                                                {rooms.map(room => 
                                                    <option key={room.id} value={room.attributes.name_url}>{room.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageRoomsClass)