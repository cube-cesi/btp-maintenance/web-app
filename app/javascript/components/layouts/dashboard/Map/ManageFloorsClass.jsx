import React, { Component, Fragment, useState } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageFloorsClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_floor_id: 0,
            modify_floor_id: 0,
            modify_floor_name: "",
            modify_floor_id: 0,
            new_floor_name: "",
            floors: [],
            imageNew: null,
            imageUpdate: null,
            showFloorInfo: false
        }

        this.handleChooseFloor = this.handleChooseFloor.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseFloor = (event) => {
        if(event.target.value === 0){
            this.setState({showFloorInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/floors/${event.target.value}`)
            .then(response => {
                this.setState({showFloorInfo: true, modify_floor_id: response.data.id, modify_floor_name: response.data.name});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_floor_id} = this.state;
        if (delete_floor_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/floors/${delete_floor_id}`)
            .then(response => {
                if(!response.error){
                    this.getData()
                    this.setState({delete_floor_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, veuillez réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_floor_name, imageNew} = this.state;
        var formData = new FormData();
        formData.append('image', imageNew);
        formData.append('name', new_floor_name);
        formData.append('plan_source', imageNew.name);

        AxiosHelper()
        axios.post(`/api/floors`, formData)
        .then(response => {
            if(!response.error){
                this.getData()
                document.getElementById("imageNew").value = null;
                this.setState({new_floor_name: "", imageNew: null})
                this.props.alert.success("Nouvel étage crée.")
            }
        })
    }

    onNewFileChange = event => { 
        this.setState({ imageNew: event.target.files[0] }); 
    }; 

    onUpdateFileChange = event => { 
        this.setState({ imageUpdate: event.target.files[0] }); 
    }; 

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_floor_id, modify_floor_name, imageUpdate} = this.state;
        var formData = new FormData();
        formData.append('image', imageUpdate);
        formData.append('name', modify_floor_name);
        formData.append('plan_source', imageUpdate.name);
        AxiosHelper()
        axios.patch(`/api/floors/${modify_floor_id}`, formData)
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({modify_floor_id: 0, showFloorInfo: false, imageUpdate: null})
                this.props.alert.success("étage modifié.")
            }
        })
    }

    getData(){
        this.getFloors()
    }

    getFloors(){
        AxiosHelper();
        axios.get('/api/floors')
        .then(response => {
            this.setState({floors: response.data})
        })
    }

    render(){
        const {delete_floor_id, modify_floor_id, modify_floor_name, new_floor_name, showFloorInfo, floors} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>étages</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                    <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouvel étage</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Nom de l'étage"
                                                value={new_floor_name}
                                                type="text"
                                                name="new_floor_name"
                                                onChange={this.handleChange}
                                            />
                                            <Input
                                                placeholder="Image du plan (format .png/.jpeg)"
                                                type="file"
                                                accept="image/png, image/jpeg"
                                                name="imageNew"
                                                id="imageNew"
                                                onChange={this.onNewFileChange}
                                            />
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier étage</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_floor_id} onChange={this.handleChooseFloor} name="modify_floor_id" id="modify_floor_id">
                                                <option value={0}>Choisir un étage</option>
                                                {floors.map(floor => 
                                                    <option key={floor.id} value={floor.id}>{floor.name}</option>
                                                )}
                                            </Select>
                                            {showFloorInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom de l'étage'"
                                                        type="text"
                                                        name="modify_floor_name"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_floor_name}
                                                    />
                                                    <Input
                                                        placeholder="Image du plan (format .png/.jpeg)"
                                                        type="file"
                                                        accept="image/png, image/jpeg"
                                                        name="imageUpdate"
                                                        onChange={this.onUpdateFileChange}
                                                    />
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer étage</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_floor_id} onChange={this.handleChange} name="delete_floor_id" id="delete_floor_id">
                                                <option value={0}>Choisir un étage</option>
                                                {floors.map(floor => 
                                                    <option key={floor.id} value={floor.id}>{floor.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageFloorsClass)
