import React, { Component } from 'react'
import MapModal from './MapModal'
import styled from 'styled-components';
import tw from "twin.macro";
import FeatherIcon from 'feather-icons-react';
import Modal from 'react-modal';
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";

import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'

const ContainerImage = styled.div`
    position: relative;
    padding-top: 70%;
    height: 0;
    ${props => `background: url('${props.imageSource}') 0 0 no-repeat;`}
    background-size: cover;
`;

const Card = tw.div`h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      backgroundColor       : 'transparent',
      border                : '0px'
    }
};
const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const Header = tw(SectionHeading)``;
const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const TabsControl = tw.div`flex flex-wrap bg-gray-200 px-2 py-2 rounded leading-none mt-12 xl:mt-0`;

const TabControl = styled.div`
  ${tw`cursor-pointer px-6 py-3 mt-2 sm:mt-0 sm:mr-2 last:mr-0 text-gray-600 font-medium rounded-sm transition duration-300 text-sm sm:text-base w-1/2 sm:w-auto text-center`}
  &:hover {
    ${tw`bg-gray-300 text-gray-700`}
  }
  ${props => props.active && tw`bg-orange-400! text-gray-100!`}
  }
`;

class Map extends Component {
    constructor(props) {
        super(props)

        this.state = {
            floors: [],
            activeTabName: "",
            activeTabId: 0,
            planSource: "",
            showContent: false,
            x: 0,
            y: 0,
            showModal: false,
            showSteps: false,
            imagePlan: null
        }

        this.onClickMap = this.onClickMap.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.refDiv = React.createRef()
    }

    componentDidMount(){
        Modal.setAppElement('body')

        this.getPropsUpdate();
        this.getFloors();
    }

    handleCloseModal () {
        this.setState({ showSteps: false, showModal: false });
    }

    getPropsUpdate = () => {
        this.setState({showSteps: this.props.openModal})
    }

    getFloors(){
        AxiosHelper()
        axios.get('/api/floors.json')
        .then(response =>{
            this.setState(
                {
                    floors: response.data, 
                    activeTabName: response.data[0].name,
                    activeTabId: response.data[0].id,
                    showContent: true, 
                    planSource: response.data[0].plan_source,
                    imagePlan: response.data[0].image
                })
        })
        .catch(error => this.props.alert.error("erreur : ", error))
    }

    onClickMap(event){
        event.preventDefault()

        const left = (event.nativeEvent.offsetX / this.refDiv.current.offsetWidth) * 100;
        const top = (event.nativeEvent.offsetY / this.refDiv.current.offsetHeight) * 100;

        this.setState({ x: left-2, y: top-1, showModal: true });
    }

    handleClickTab = (tabName, planSource, tabId, imgPlan) => {
        try {
            this.setState(
                {
                    activeTabName: tabName, 
                    activeTabId: tabId,
                    planSource: planSource,
                    imagePlan: imgPlan
                })        
            this.setState({showContent: true})
        } catch (error) {
            this.setState({showContent: false})
            this.props.alert.error(error)
        }
    }

    render(){
        const {showModal, showSteps,  floors, activeTabName, activeTabId, planSource, showContent, imagePlan} = this.state;

        return(
            <Container>
                <Modal
                        isOpen={showSteps}
                        style={customStyles}
                >
                    <Card>
                        <TextInfo>
                            <TitleReviewContainer>
                                <Title>Etapes à suivre: </Title>
                                <CloseInfo onClick={this.handleCloseModal}>
                                    <FeatherIcon icon="x"/>
                                </CloseInfo>
                            </TitleReviewContainer>
                            <Description>
                                1- Cliquez à la position de la salle
                                <br/>
                                2- Choisir la salle à mettre à jour
                            </Description>
                        </TextInfo>
                    </Card>
                </Modal>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer vos <HighlightedText>plans</HighlightedText></Header>
                        <TabsControl>
                        { floors.map(floor =>
                            <TabControl key={floor.id} active={activeTabName === floor.name} onClick={() => this.handleClickTab(floor.name, floor.plan_source, floor.id, floor.image)}>
                                {floor.name}
                            </TabControl>
                        )}
                        </TabsControl>
                    </HeaderRow>
                    <Modal
                        isOpen={showModal}
                        style={customStyles}
                    >
                        <MapModal handleCloseModal={this.handleCloseModal} coordinates={this.state} activeTabId={activeTabId}/>
                    </Modal>
                    {showContent ? 
                        (
                            <ContainerImage ref={this.refDiv} onClick={this.onClickMap} imageSource={imagePlan.url}/>                        
                        ):
                        (
                            null
                        )
                    }
                    
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default Map