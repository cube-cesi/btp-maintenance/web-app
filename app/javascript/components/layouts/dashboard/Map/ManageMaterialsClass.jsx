import React, { Component, Fragment, useState } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageRoomsClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_material_id: 0,
            modify_material_id: 0,
            modify_material_name: "",
            modify_material_quantity: 0,
            modify_material_room: 0,
            new_material_id: 0,
            new_material_name: "",
            new_material_quantity: 0,
            new_material_room: 0,
            materials: [],
            rooms: [],
            showMaterialInfo: false
        }

        this.handleChooseMaterial = this.handleChooseMaterial.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseMaterial = (event) => {
        if(event.target.value === 0){
            this.setState({showMaterialInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/materials/${event.target.value}`)
            .then(response => {
                this.setState({showMaterialInfo: true, modify_material_id: response.data.data.id, modify_material_name: response.data.data.attributes.name, modify_material_quantity: response.data.data.attributes.quantity, modify_material_room: response.data.data.attributes.room_id});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_material_id} = this.state;
        if (delete_material_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/materials/${delete_material_id}`)
            .then(response => {
                if(!response.error){
                    this.getData()
                    this.setState({delete_material_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_material_name, new_material_quantity, new_material_room} = this.state;
        let material = {
            name: new_material_name,
            quantity: new_material_quantity,
            room_id: new_material_room
        }

        AxiosHelper()
        axios.post(`/api/materials`, {material})
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({new_material_name: "", new_material_quantity: 0, new_material_room: 0})
                this.props.alert.success("Nouveeau matériel créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_material_id, modify_material_name, modify_material_quantity, modify_material_room} = this.state;
        let material = {
            name: modify_material_name,
            quantity: modify_material_quantity,
            room_id: modify_material_room
        }

        AxiosHelper()
        axios.patch(`/api/materials/${modify_material_id}`, {material})
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({modify_material_id: 0, showMaterialInfo: false})
                this.props.alert.success("Matériel modifié.")
            }
        })
    }

    getData(){
        this.getMaterials()
        this.getRooms()
    }

    getMaterials(){
        AxiosHelper()
        axios.get('/api/materials')
        .then(response => {
            this.setState({materials: response.data.data})
        })
    }

    getRooms(){
        AxiosHelper();
        axios.get('/api/rooms')
        .then(response => {
            console.log(response);
            this.setState({rooms: response.data.data})
        }).catch(err => console.log(err));
    }

    render(){
        const {rooms, materials, showMaterialInfo, new_material_name, new_material_quantity, new_material_room, modify_material_id, modify_material_name, modify_material_quantity, modify_material_room, delete_material_id} = this.state;
        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>matériels</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                    <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouveau matériel</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Nom du matériel"
                                                value={new_material_name}
                                                type="text"
                                                name="new_material_name"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Quantité"
                                                value={new_material_quantity}
                                                type="number"
                                                name="new_material_quantity"
                                                onChange={this.handleChange}
                                            />
                                            <Select value={new_material_room} name="new_material_room" id="new_material_room" onChange={this.handleChange}>
                                                <option value={0}>Choisir une salle</option>
                                                {rooms.map(room => 
                                                    <option key={room.id} value={room.id}>{room.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier matériel</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_material_id} onChange={this.handleChooseMaterial} name="modify_material_id" id="modify_material_id">
                                                <option value={0}>Choisir un matériel</option>
                                                {materials.map(material => 
                                                    <option key={material.id} value={material.id}>{material.attributes.name}</option>
                                                )}
                                            </Select>
                                            {showMaterialInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom du matériel"
                                                        type="text"
                                                        name="modify_material_name"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_material_name}
                                                    />
                                                    <Input
                                                        placeholder="Quantité"
                                                        type="number"
                                                        name="modify_material_quantity"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_material_quantity}
                                                    />
                                                    <Select defaultValue={modify_material_room} name="modify_material_room" id="modify_material_room" onChange={this.handleChange}>
                                                        {rooms.map(room => 
                                                            <option key={room.id} value={room.id}>{room.attributes.name}</option>
                                                        )}
                                                    </Select>
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer matériel</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_material_id} onChange={this.handleChange} name="delete_material_id" id="delete_material_id">
                                                <option value={0}>Choisir un matériel</option>
                                                {materials.map(material => 
                                                    <option key={material.id} value={material.id}>{material.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageRoomsClass)