import React, { Component, Fragment, useState } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'
import Autocomplete from "react-autocomplete-select";

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;

const DeleteContainer = tw.div`mx-auto max-w-xs`;

const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageUsersClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_user_id: 0,
            modify_user_id: 0,
            modify_user_firstname: "",
            modify_user_lastname: "",
            modify_user_email: "",
            modify_user_rank: 0,
            new_user_firstname: "",
            new_user_lastname: "",
            new_user_email: "",
            new_user_password: "",
            new_user_rank: 0,
            users: [],
            ranks: [],
            showUserInfo: false
        }

        this.handleChooseUser = this.handleChooseUser.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseUser = (id) => {
        AxiosHelper()
        axios.get(`/api/members/${id}`)
        .then(response => {
            this.setState({showUserInfo: true, modify_user_id: response.data.data.id, modify_user_firstname: response.data.data.attributes.firstname, modify_user_lastname: response.data.data.attributes.lastname, modify_user_email: response.data.data.attributes.email, modify_user_rank: response.data.data.attributes.rank_id});
        })
    }

    handleChooseUserDelete = (id) => {
        this.setState({delete_user_id: id})
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_user_id} = this.state;
        if (delete_user_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/members/${delete_user_id}`)
            .then(response => {
                if(!response.error){
                    this.getData()
                    this.setState({delete_user_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_user_firstname, new_user_lastname, new_user_email, new_user_password, new_user_rank} = this.state;
        let member = {
            firstname: new_user_firstname,
            lastname: new_user_lastname,
            email: new_user_email,
            password: new_user_password,
            rank_id: new_user_rank
        }

        AxiosHelper()
        axios.post(`/api/members`, {member})
        .then(response => {
            if(!response.error){
                this.getData()
                this.setState({new_user_firstname: "", new_user_lastname: "", new_user_email: "", new_user_password: "", new_user_rank: 0})
                this.props.alert.success("Nouveau membre créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_user_id,modify_user_firstname, modify_user_lastname, modify_user_email, modify_user_rank} = this.state;
        let member = {
            firstname: modify_user_firstname,
            lastname: modify_user_lastname,
            email: modify_user_email,
            rank_id: modify_user_rank
        }

        AxiosHelper()
        axios.patch(`/api/members/${modify_user_id}`, {member})
        .then(response => {
            if(!response.error){
                this.getMembers()
                this.setState({showUserInfo: false})
                this.props.alert.success("Membre modifié.")
            }
        })
    }

    getData(){
        this.getRanks()
        this.getMembers()
    }

    getRanks(){
        AxiosHelper()
        axios.get('/api/ranks')
        .then(response => {
            this.setState({ranks: response.data.data})
        })
    }

    getMembers(){
        AxiosHelper();
        axios.get('/api/members')
        .then(response => {
            this.setState({users: response.data.data})
        })
    }

    render(){
        const {showUserInfo,new_user_firstname, new_user_lastname, new_user_email, new_user_password, new_user_rank, modify_user_firstname, modify_user_lastname, modify_user_email, modify_user_rank, modify_user_id, delete_user_id, users, ranks} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>membres</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                    <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouveau membre</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Prénom"
                                                value={new_user_firstname}
                                                type="text"
                                                name="new_user_firstname"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Nom"
                                                value={new_user_lastname}
                                                type="text"
                                                name="new_user_lastname"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Adresse e-mail"
                                                value={new_user_email}
                                                type="text"
                                                name="new_user_email"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Mot de passe"
                                                value={new_user_password}
                                                type="password"
                                                name="new_user_password"
                                                onChange={this.handleChange}
                                            />
                                            <Select value={new_user_rank} name="new_user_rank" id="new_user_rank" onChange={this.handleChange}>
                                                <option value={0}>Choisir un rang</option>
                                                {ranks.map(rank => 
                                                    <option key={rank.id} value={rank.id}>{rank.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            {showUserInfo ? (
                                <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier membre</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Fragment>
                                                <Input 
                                                    placeholder="Prénom"
                                                    value={modify_user_firstname}
                                                    type="text"
                                                    name="modify_user_firstname"
                                                    onChange={this.handleChange}
                                                />
                                                <Input 
                                                    placeholder="Nom"
                                                    value={modify_user_lastname}
                                                    type="text"
                                                    name="modify_user_lastname"
                                                    onChange={this.handleChange}
                                                />
                                                <Input 
                                                    placeholder="Adresse e-mail"
                                                    value={modify_user_email}
                                                    type="text"
                                                    name="modify_user_email"
                                                    onChange={this.handleChange}
                                                />
                                                <Select value={modify_user_rank} name="modify_user_rank" id="modify_user_rank" onChange={this.handleChange}>
                                                    {ranks.map(rank => 
                                                        <option key={rank.id} value={rank.id}>{rank.attributes.name}</option>
                                                    )}
                                                </Select>
                                            </Fragment>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                            ):(
                                <TextInfo>
                                    <TitleReviewContainer>
                                        <Title>Modifier membre</Title>
                                    </TitleReviewContainer>
                                    <Description>
                                        <Autocomplete 
                                            searchPattern={'containsString'} 
                                            placeholder="Rechercher un membre" 
                                            getItemValue={(item)=>{ return  item.id + " - " + item.attributes.firstname + " " + item.attributes.lastname }}
                                            onSelect={
                                                (item)=>{
                                                    this.handleChooseUser(item.replace(/[^0-9]/g, ''))
                                                }
                                            }
                                            maxOptionsLimit={10} 
                                            itemsData={users}
                                        />
                                    </Description>
                                </TextInfo>
                            )
                            }
                            
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <DeleteContainer>
                                <TextInfo>
                                    <TitleReviewContainer>
                                        <Title>Supprimer membre</Title>
                                    </TitleReviewContainer>
                                    <Description>
                                        <Autocomplete
                                            value=""
                                            searchPattern={'containsString'} 
                                            placeholder="Rechercher un membre" 
                                            getItemValue={(item)=>{ return  item.id + " - " + item.attributes.firstname + " " + item.attributes.lastname }}
                                            onSelect={
                                                (item)=>{
                                                    this.handleChooseUserDelete(item.replace(/[^0-9]/g, ''))
                                                }
                                            }
                                            maxOptionsLimit={10} 
                                            itemsData={users}
                                        /> 

                                    </Description>
                                    </TextInfo>
                                    <PrimaryButton type="button" onClick={this.handleSubmitDelete}>Valider</PrimaryButton>
                                </DeleteContainer>
                            </FormContainer>
                            
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageUsersClass)