import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;

class ManageSmtpClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            modify_smtp_server: "",
            modify_smtp_port: "",
            modify_smtp_user: "",
            modify_smtp_password: "",
            loaded: false
        }

        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getSmtp();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_smtp_server, modify_smtp_user, modify_smtp_password, modify_smtp_port} = this.state;
        let setting = {
            mailSmtpAddress: modify_smtp_server,
            mailSmtpPort: modify_smtp_port,
            mailAddress: modify_smtp_user,
            pswd: modify_smtp_password
        }

        AxiosHelper()
        axios.patch(`/api/settings/update/mail`, {setting})
        .then(response => {
            if(!response.error){
                this.props.alert.success("SMTP modifié.")
            }
        })
    }

    getSmtp(){
        AxiosHelper()
        axios.get('/api/settings')
        .then(response => {
            this.setState({modify_smtp_server: response.data.data.attributes.mailSmtpAddress, modify_smtp_user: response.data.data.attributes.mailAddress, modify_smtp_password: response.data.data.attributes.pswd, modify_smtp_port: response.data.data.attributes.mailSmtpPort, loaded: true})
        })
    }

    render(){
        const {modify_smtp_server, modify_smtp_user, modify_smtp_password, modify_smtp_port, loaded} = this.state;
        return(
            <Container>
                {loaded &&
                    <ContentWithPaddingXl>
                        <HeaderRow>
                            <Header>Gérer le <HighlightedText>SMTP</HighlightedText></Header>
                        </HeaderRow>
                        <ManageContainer>
                            <SubManageContainer>
                                <FormContainer>
                                    <Form onSubmit={this.handleSubmitUpdate}>
                                        <TextInfo>
                                            <TitleReviewContainer>
                                                <Title>Modifier SMTP</Title>
                                            </TitleReviewContainer>
                                            <Description>
                                                <Input 
                                                    placeholder="Serveur smtp"
                                                    value={modify_smtp_server}
                                                    type="text"
                                                    name="modify_smtp_server"
                                                    onChange={this.handleChange}
                                                />
                                                <Input 
                                                    placeholder="Utilisateur smtp"
                                                    value={modify_smtp_user}
                                                    type="text"
                                                    name="modify_smtp_user"
                                                    onChange={this.handleChange}
                                                />
                                                <Input 
                                                    placeholder="Mot de passe smtp"
                                                    value={modify_smtp_password}
                                                    type="password"
                                                    name="modify_smtp_password"
                                                    onChange={this.handleChange}
                                                />
                                                <Input 
                                                    placeholder="Port smtp"
                                                    value={modify_smtp_port}
                                                    type="number"
                                                    name="modify_smtp_port"
                                                    onChange={this.handleChange}
                                                />
                                            </Description>
                                        </TextInfo>
                                        <PrimaryButton type="submit">Valider</PrimaryButton>
                                    </Form>
                                </FormContainer>
                            </SubManageContainer>
                        </ManageContainer>
                    </ContentWithPaddingXl>
                }
            </Container>
        )
    }
}

export default withAlert()(ManageSmtpClass)