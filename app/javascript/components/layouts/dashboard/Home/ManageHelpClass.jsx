import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextArea = tw.textarea`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageHelpClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_helper_id: 0,
            modify_helper_id: 0,
            modify_helper_head: "",
            modify_helper_desc: "",
            new_helper_head: "",
            new_helper_desc: "",
            helpers: [],
            showHelperInfo: false
        }

        this.handleChooseHelper = this.handleChooseHelper.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getHelpers();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseHelper = (event) => {
        if(event.target.value === 0){
            this.setState({showHelperInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/helpers/${event.target.value}`)
            .then(response => {
                this.setState({showHelperInfo: true, modify_helper_id: response.data.data.id, modify_helper_head: response.data.data.attributes.heading, modify_helper_desc: response.data.data.attributes.description});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_helper_id} = this.state;
        if (delete_helper_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/helpers/${delete_helper_id}`)
            .then(response => {
                if(!response.error){
                    this.getHelpers()
                    this.setState({delete_helper_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_helper_head, new_helper_desc} = this.state;
        let helper = {
            heading: new_helper_head,
            description: new_helper_desc
        }

        AxiosHelper()
        axios.post(`/api/helpers`, {helper})
        .then(response => {
            if(!response.error){
                this.getHelpers()
                this.setState({new_helper_head: "", new_helper_desc: ""})
                this.props.alert.success("Nouvelle aide créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_helper_id, modify_helper_head, modify_helper_desc} = this.state;
        let helper = {
            heading: modify_helper_head,
            description: modify_helper_desc
        }

        AxiosHelper()
        axios.patch(`/api/helpers/${modify_helper_id}`, {helper})
        .then(response => {
            if(!response.error){
                this.getHelpers()
                this.setState({modify_helper_id: 0, showHelperInfo: false})
                this.props.alert.success("Aide modifiée.")
            }
        })
    }

    getHelpers(){
        AxiosHelper()
        axios.get('/api/helpers')
        .then(response => {
            this.setState({helpers: response.data.data})
        })
    }

    render(){
        const {delete_helper_id, modify_helper_id, modify_helper_head, modify_helper_desc, new_helper_head, new_helper_desc, helpers, showHelperInfo} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>aides</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouvelle aide</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Titre"
                                                value={new_helper_head}
                                                type="text"
                                                name="new_helper_head"
                                                onChange={this.handleChange}
                                            />
                                            <TextArea
                                                name="new_helper_desc"
                                                onChange={this.handleChange}
                                                placeholder="Description"
                                                value={new_helper_desc}
                                            />
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier aide</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_helper_id} onChange={this.handleChooseHelper} name="modify_helper_id" id="modify_helper_id">
                                                <option value="0">Choisir une aide</option>
                                                {helpers.map(helper => 
                                                    <option key={helper.id} value={helper.id}>{helper.attributes.heading}</option>
                                                )}
                                            </Select>
                                            {showHelperInfo &&
                                                <Fragment>
                                                    <Input 
                                                        placeholder="Titre"
                                                        value={modify_helper_head}
                                                        type="text"
                                                        name="modify_helper_head"
                                                        onChange={this.handleChange}
                                                    />
                                                    <TextArea
                                                        name="modify_helper_desc"
                                                        onChange={this.handleChange}
                                                        placeholder="Description"
                                                        value={modify_helper_desc}
                                                    />
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer aide</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_helper_id} name="delete_helper_id" id="delete_helper_id" onChange={this.handleChange}>
                                                <option value="0">Choisir une aide</option>
                                                {helpers.map(helper => 
                                                    <option key={helper.id} value={helper.id}>{helper.attributes.heading}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageHelpClass)