import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import { withAlert } from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextArea = tw.textarea`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageDomainsClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            delete_domain_id: 0,
            modify_domain_id: 0,
            modify_domain_name: "",
            new_domain_name: "",
            domains: [],
            showDomainInfo: false
        }

        this.handleChooseDomain = this.handleChooseDomain.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount() {
        this.getDomains();
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseDomain = (event) => {
        if (event.target.value === 0) {
            this.setState({ showDomainInfo: false });
        } else {
            AxiosHelper()
            axios.get(`/api/email_domains/${event.target.value}`)
                .then(response => {
                    this.setState({ showDomainInfo: true, modify_domain_id: response.data.data.id, modify_domain_name: response.data.data.attributes.name });
                })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const { delete_domain_id } = this.state;
        if (delete_domain_id != 0) {
            AxiosHelper()
            axios.delete(`/api/email_domains/${delete_domain_id}`)
                .then(response => {
                    if (!response.error) {
                        this.getDomains()
                        this.setState({ delete_domain_id: 0 })
                        this.props.alert.success("Suppression réussie.")
                    }
                })
        } else {
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const { new_domain_name } = this.state;
        let email_domain = {
            name: new_domain_name
        }

        AxiosHelper()
        axios.post(`/api/email_domains`, { email_domain })
            .then(response => {
                if (!response.error) {
                    this.getDomains()
                    this.setState({ new_domain_name: "" })
                    this.props.alert.success("Nouveau domaine créer.")
                }
            })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const { modify_domain_id, modify_domain_name } = this.state;
        let email_domain = {
            id: modify_domain_id,
            name: modify_domain_name
        }

        AxiosHelper()
        axios.patch(`/api/email_domains/${modify_domain_id}`, { email_domain })
            .then(response => {
                if (!response.error) {
                    this.getDomains()
                    this.setState({ modify_domain_id: 0, showDomainInfo: false })
                    this.props.alert.success("Domaine modifié.")
                }
            })
    }

    getDomains() {
        AxiosHelper()
        axios.get('/api/email_domains')
            .then(response => {
                this.setState({ domains: response.data.data })
            })
    }

    render() {
        const { domains ,new_domain_name, modify_domain_id, modify_domain_name, delete_domain_id, showDomainInfo } = this.state;

        return (
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>domaines</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouveeau domaine</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input
                                                placeholder="Nom du domaine (sans @)"
                                                value={new_domain_name}
                                                type="text"
                                                name="new_domain_name"
                                                onChange={this.handleChange}
                                            />
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier domaine</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_domain_id} onChange={this.handleChooseDomain} name="modify_domain_id" id="modify_domain_id">
                                                <option value="0">Choisir un domaine</option>
                                                {domains.map(domain =>
                                                    <option key={domain.id} value={domain.id}>{domain.attributes.name}</option>
                                                )}
                                            </Select>
                                            {showDomainInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom du domaine (sans @)"
                                                        value={modify_domain_name}
                                                        type="text"
                                                        name="modify_domain_name"
                                                        onChange={this.handleChange}
                                                    />
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer domaine</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_domain_id} name="delete_domain_id" id="delete_domain_id" onChange={this.handleChange}>
                                                <option value="0">Choisir un domaine</option>
                                                {domains.map(domain =>
                                                    <option key={domain.id} value={domain.id}>{domain.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageDomainsClass)