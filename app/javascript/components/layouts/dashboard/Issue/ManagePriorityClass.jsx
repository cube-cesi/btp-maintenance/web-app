import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManagePriorityClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_priority_id: 0,
            modify_priority_id: 0,
            modify_priority_name: "",
            modify_priority_order: 0,
            new_priority_name: "",
            new_priority_order: "",
            priorities: [],
            showPriorityInfo: false
        }

        this.handleChoosePriority = this.handleChoosePriority.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getPriorities();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChoosePriority = (event) => {
        if(event.target.value === 0){
            this.setState({showPriorityInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/priorities/${event.target.value}`)
            .then(response => {
                this.setState({showPriorityInfo: true, modify_priority_id: response.data.data.id, modify_priority_name: response.data.data.attributes.name, modify_priority_order: response.data.data.attributes.order});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_priority_id} = this.state;
        if (delete_priority_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/priorities/${delete_priority_id}`)
            .then(response => {
                if(!response.error){
                    this.getPriorities()
                    this.setState({delete_priority_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_priority_name, new_priority_order} = this.state;
        let priority = {
            name: new_priority_name,
            order: new_priority_order
        }

        AxiosHelper()
        axios.post(`/api/priorities`, {priority})
        .then(response => {
            if(!response.error){
                this.getPriorities()
                this.setState({new_priority_name: "", new_priority_order: ""})
                this.props.alert.success("Nouvelle priorité créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_priority_id, modify_priority_name, modify_priority_order} = this.state;
        let priority = {
            name: modify_priority_name,
            order: modify_priority_order
        }

        AxiosHelper()
        axios.patch(`/api/priorities/${modify_priority_id}`, {priority})
        .then(response => {
            if(!response.error){
                this.getPriorities()
                this.setState({modify_priority_id: 0, showPriorityInfo: false})
                this.props.alert.success("Priorité modifié.")
            }
        })
    }

    getPriorities(){
        AxiosHelper()
        axios.get('/api/priorities')
        .then(response => {
            this.setState({priorities: response.data.data})
        })
    }

    render(){
        const {delete_priority_id, modify_priority_id, modify_priority_name, modify_priority_order, priorities, showPriorityInfo, new_priority_name, new_priority_order} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>priorités</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouvelle priorité</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Nom priorité"
                                                value={new_priority_name}
                                                type="text"
                                                name="new_priority_name"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Ordre de puissance"
                                                value={new_priority_order}
                                                type="number"
                                                name="new_priority_order"
                                                onChange={this.handleChange}
                                            />
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier priorité</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_priority_id} onChange={this.handleChoosePriority} name="modify_priority_id" id="modify_priority_id">
                                                <option value="0">Choisir une priorité</option>
                                                {priorities.map(priority => 
                                                    <option key={priority.id} value={priority.id}>{priority.attributes.name}</option>
                                                )}
                                            </Select>
                                            {showPriorityInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom priorité"
                                                        type="text"
                                                        name="modify_priority_name"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_priority_name}
                                                    />
                                                    <Input
                                                        placeholder="Ordre de puissance"
                                                        type="number"
                                                        name="modify_priority_order"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_priority_order}
                                                    />
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer priorité</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_priority_id} name="delete_priority_id" id="delete_priority_id" onChange={this.handleChange}>
                                                <option value="0">Choisir une priorité</option>
                                                {priorities.map(priority => 
                                                    <option key={priority.id} value={priority.id}>{priority.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManagePriorityClass)