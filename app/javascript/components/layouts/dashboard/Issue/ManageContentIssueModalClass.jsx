import React, { Component, Fragment } from 'react'
import FeatherIcon from 'feather-icons-react';
import tw from "twin.macro";
import styled from "styled-components";
import AxiosHelper from '../../../utils/AxiosHelper';
import Axios from 'axios';
import { withAlert } from 'react-alert'

const Card = tw.div`h-full flex! flex-col sm:border max-w-sm sm:rounded-tl-4xl sm:rounded-br-5xl relative focus:outline-none bg-white`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const CloseInfo = styled.div`
  ${tw`flex items-center sm:ml-4 mt-2 sm:mt-0`}
  svg {
    ${tw`w-6 h-6 text-red-700 fill-current`}
  }
`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageContentIssueModal extends Component{
    constructor(props){
        super(props)
        this.state = {
            state_id: this.props.state_id,
            technician_id: this.props.technician_id,
            priority_id: this.props.priority_id,
            technicians: [],
            states: [],
            priorities: []
        }
    }

    componentDidMount(){
        this.loadData()
    }

    loadData = () => {
        AxiosHelper()
        Axios.get('/api/states/')
        .then(response => {
            this.setState({states: response.data.data})
        })

        Axios.get('/api/members/get/tech')
        .then(response => {
            this.setState({technicians: response.data.data})
        })

        Axios.get('/api/priorities')
        .then(response => {
            this.setState({priorities: response.data.data})
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const {state_id, technician_id, priority_id} = this.state
        let issue = {
          state_id: state_id,
          technician_id: technician_id === "0"? (null):(technician_id),
          priority_id: priority_id
        }

        AxiosHelper();
        Axios.patch(`/api/issues/${this.props.issue_id}`, {issue})
        .then(response => {
            if(!response.error){
                this.handleClose()
            }
        })
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleClose = () => {
        this.props.handleCloseModal()
    }

    render(){
        const {technicians, states, priorities, state_id, priority_id, technician_id} = this.state
        const {rank} = this.props;
        return(
            <Card>
                <FormContainer>
                            <Form onSubmit={this.handleSubmit}>
                                <TextInfo>
                                    <TitleReviewContainer>
                                        <Title>Gestion ticket</Title>
                                        <CloseInfo onClick={this.handleClose}>
                                            <FeatherIcon icon="x"/>
                                        </CloseInfo>
                                    </TitleReviewContainer>
                                    <Description>
                                        {rank !== "Technicien" &&
                                            <Select value={technician_id} name="technician_id" id="technician" onChange={this.handleChange}>
                                                <option value="0">Pas de technicien</option>
                                                {technicians.map(technician => 
                                                    <option key={technician.id} value={technician.id}>{technician.attributes.firstname + " " + technician.attributes.lastname}</option>
                                                )}
                                            </Select> 
                                        }
                                             
                                        <Select value={state_id} name="state_id" id="state" onChange={this.handleChange}>
                                            {states.map(state => 
                                                <option key={state.id} value={state.id}>{state.attributes.name}</option>
                                            )}
                                        </Select>
                                        <Select value={priority_id} name="priority_id" id="priority" onChange={this.handleChange}>
                                            {priorities.map(priority => 
                                                <option key={priority.id} value={priority.id}>{priority.attributes.name}</option>
                                            )}
                                        </Select>
                                    </Description>
                                </TextInfo>
                                <PrimaryButton type="submit">Valider</PrimaryButton>
                            </Form>
                        </FormContainer>
            </Card>
        )
    }
}

export default withAlert()(ManageContentIssueModal)