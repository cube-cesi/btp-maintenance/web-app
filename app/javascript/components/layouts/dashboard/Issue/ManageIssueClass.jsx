import React, { Component, Fragment } from 'react'
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import styled from 'styled-components';
import tw from 'twin.macro';
import { css } from "styled-components/macro";
import FeatherIcon from 'feather-icons-react'
import Modal from 'react-modal';
import ManageIssueModalClass from './ManageIssueModalClass';

const Container = tw.div`relative`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;
const ThreeColumn = tw.div`flex flex-col items-center lg:items-stretch lg:flex-row flex-wrap`;
const Column = tw.div` lg:w-1/3 mb-8`;

const Card = tw.div`lg:mx-4 xl:mx-8 max-w-sm flex flex-col h-full `;
const PrimaryButton = tw.button`px-8 mt-4 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300`;
const Details = tw.div`p-6 rounded border-2 border-gray-500 flex-1 flex flex-col items-center text-center lg:block lg:text-left`;
const MetaContainer = tw.div`flex items-center`;
const Meta = styled.div`
  ${tw`text-secondary-100 font-medium text-sm flex items-center leading-none mr-6 last:mr-0`}
  svg {
    ${tw`w-4 h-4 mr-1`}
  }
`;

const Title = tw.h5`mt-4 leading-snug font-bold text-lg`;
const Description = tw.p`mt-2 text-sm text-secondary-100`;
const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      backgroundColor       : 'transparent',
      border                : '0px'
    }
};

class ManageIssueClass extends Component{
    constructor(props){
        super(props)
        this.state = {
            issues: [],
            redirectIssue: false,
            issueClicked: [],
            id_issue: 0,
            loaded: false,
            showModal: false
        }

        this.handleClickOpenModal = this.handleClickOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    componentDidMount() {
        Modal.setAppElement('body')

        this.getIssues()
    }

    handleClickOpenModal = (issue_id) => {
        this.setState({showModal: true, issue_id: issue_id})
    }

    handleCloseModal = () => {
        this.getIssues();
        this.setState({ showModal: false });
    }

    getIssues(){
        AxiosHelper();
        if(this.props.rank === "Technicien"){
            axios.get(`/api/issues/get/all/${this.props.member.id}`)
            .then(res => {
                this.setState({issues: res.data.data, loaded: true})
            })
        }else{
            axios.get('/api/issues/get/all')
            .then(res => {
                this.setState({issues: res.data.data, loaded: true})
            })
        }
    }

    render(){
        const {issues, showModal, issue_id, loaded} = this.state
        console.log(this.props.activePriorityId)
        return(
            <Container>
                {loaded && 
                    <Content>
                        <Modal
                            isOpen={showModal}
                            style={customStyles}
                        >
                            <ManageIssueModalClass rank={this.props.rank} member={this.props.member} handleCloseModal={this.handleCloseModal} issue_id={issue_id}/>
                        </Modal>
                        <ThreeColumn>
                            {
                                issues.filter(issue => (issue.priority_id == this.props.activePriorityId && issue.state.name !== "Fermé") || (this.props.activePriorityId == 0 && issue.state.name === "Fermé")).map(issue => 
                                    <Column key={issue.id}>
                                        <Card>
                                            <Details>
                                                <MetaContainer>
                                                    <Meta>
                                                        <FeatherIcon icon="user"/>
                                                        <div>{issue.memberFirstname === null ? ("Anonyme"):(issue.memberFirstname + " " + issue.memberLastname)}</div>
                                                    </Meta>
                                                    <Meta>
                                                        <FeatherIcon icon="tag"/>
                                                        <div>{issue.state.name}</div>
                                                    </Meta>
                                                    <Meta>
                                                        <FeatherIcon icon="map"/>
                                                        <div>{issue.roomName}</div>
                                                    </Meta>
                                                </MetaContainer>
                                                <Title>{issue.title}</Title>
                                                <Description>{issue.description.substring(0, 50)}...</Description>
                                                <PrimaryButton type="button" onClick={() => this.handleClickOpenModal(issue.id)}>Gérer le ticket</PrimaryButton>
                                            </Details>
                                        </Card>
                                    </Column>  
                                )
                                
                            }
                        </ThreeColumn>
                    </Content>
                }
            </Container>
        )
    }
}
export default ManageIssueClass