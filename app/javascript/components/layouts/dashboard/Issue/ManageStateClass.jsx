import React, { Component, Fragment, useState } from "react";
import { motion } from "framer-motion";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { Container, ContentWithPaddingXl } from "../../misc/Container";
import { SectionHeading } from "../../misc/Headings";
import axios from 'axios'
import AxiosHelper from '../../../utils/AxiosHelper'
import {withAlert} from 'react-alert'

const HeaderRow = tw.div`flex justify-between items-center flex-col xl:flex-row`;
const ManageContainer = tw.div`mt-8 flex flex-col lg:flex-row`;
const SubManageContainer = tw.div`lg:w-1/3`
const Header = tw(SectionHeading)``;

const HighlightedText = tw.span`bg-orange-400 text-gray-100 px-4 transform -skew-x-12 inline-block`;

const FormContainer = tw.div`w-full flex-1`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const TextInfo = tw.div`py-6 sm:px-10 sm:py-6`;
const TitleReviewContainer = tw.div`flex flex-col sm:flex-row sm:justify-between sm:items-center`;
const Title = tw.h5`text-2xl font-bold`;

const PrimaryButton = tw.button`px-8 py-3 font-bold rounded bg-orange-400 text-gray-100 hocus:bg-orange-500 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition duration-300 mt-auto sm:text-lg rounded-none w-full rounded sm:rounded-none sm:rounded-br-4xl py-3 sm:py-6`;
const Description = tw.div`leading-loose mt-2 sm:mt-4`;
const Select = tw.select`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-800 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;

class ManageStateClass extends Component {
    constructor(props){
        super(props)
        this.state = {
            delete_state_id: 0,
            modify_state_id: 0,
            modify_state_name: "",
            modify_state_order: 0,
            new_state_name: "",
            new_state_order: "",
            states: [],
            showStateInfo: false
        }

        this.handleChooseState = this.handleChooseState.bind(this);
        this.handleSubmitDelete = this.handleSubmitDelete.bind(this);
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this);
    }

    componentDidMount(){
        this.getStates();
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    };

    handleChooseState = (event) => {
        if(event.target.value === 0){
            this.setState({showStateInfo: false});
        }else{
            AxiosHelper()
            axios.get(`/api/states/${event.target.value}`)
            .then(response => {
                this.setState({showStateInfo: true, modify_state_id: response.data.data.id, modify_state_name: response.data.data.attributes.name, modify_state_order: response.data.data.attributes.order});
            })
        }
    }

    handleSubmitDelete = (event) => {
        event.preventDefault()
        const {delete_state_id} = this.state;
        if (delete_state_id != 0)
        {
            AxiosHelper()
            axios.delete(`/api/states/${delete_state_id}`)
            .then(response => {
                if(!response.error){
                    this.getStates()
                    this.setState({delete_state_id: 0})
                    this.props.alert.success("Suppression réussie.")
                }
            })
        }else{
            this.props.alert.error("Erreur, réessayer.")
        }
    }

    handleSubmitNew = (event) => {
        event.preventDefault();
        const {new_state_name, new_state_order} = this.state;
        let state = {
            name: new_state_name,
            order: new_state_order
        }

        AxiosHelper()
        axios.post(`/api/states`, {state})
        .then(response => {
            if(!response.error){
                this.getStates()
                this.setState({new_state_name: "", new_state_order: ""})
                this.props.alert.success("Nouveau statut créer.")
            }
        })
    }

    handleSubmitUpdate = (event) => {
        event.preventDefault();
        const {modify_state_id, modify_state_name, modify_state_order} = this.state;
        let state = {
            name: modify_state_name,
            order: modify_state_order
        }

        AxiosHelper()
        axios.patch(`/api/states/${modify_state_id}`, {state})
        .then(response => {
            if(!response.error){
                this.getStates()
                this.setState({modify_state_id: 0, showStateInfo: false})
                this.props.alert.success("Statut modifié.")
            }
        })
    }

    getStates(){
        AxiosHelper()
        axios.get('/api/states/manage/get')
        .then(response => {
            this.setState({states: response.data.data})
        })
    }

    render(){
        const {delete_state_id, modify_state_id, modify_state_name, modify_state_order, states, showStateInfo, new_state_name, new_state_order} = this.state;

        return(
            <Container>
                <ContentWithPaddingXl>
                    <HeaderRow>
                        <Header>Gérer les <HighlightedText>statuts</HighlightedText></Header>
                    </HeaderRow>
                    <ManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitNew}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Nouveau statut</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Input 
                                                placeholder="Nom du statut"
                                                value={new_state_name}
                                                type="text"
                                                name="new_state_name"
                                                onChange={this.handleChange}
                                            />
                                            <Input 
                                                placeholder="Ordre de puissance"
                                                value={new_state_order}
                                                type="number"
                                                name="new_state_order"
                                                onChange={this.handleChange}
                                            />
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitUpdate}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Modifier statut</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={modify_state_id} onChange={this.handleChooseState} name="modify_state_id" id="modify_state_id">
                                                <option value="0">Choisir un statut</option>
                                                {states.map(state => 
                                                    <option key={state.id} value={state.id}>{state.attributes.name}</option>
                                                )}
                                            </Select>
                                            {showStateInfo &&
                                                <Fragment>
                                                    <Input
                                                        placeholder="Nom du statut"
                                                        type="text"
                                                        name="modify_state_name"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_state_name}
                                                    />
                                                    <Input
                                                        placeholder="Ordre de puissance"
                                                        type="number"
                                                        name="modify_state_order"
                                                        onChange={this.handleChange}
                                                        defaultValue={modify_state_order}
                                                    />
                                                </Fragment>
                                            }
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                        <SubManageContainer>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmitDelete}>
                                    <TextInfo>
                                        <TitleReviewContainer>
                                            <Title>Supprimer statut</Title>
                                        </TitleReviewContainer>
                                        <Description>
                                            <Select value={delete_state_id} name="delete_state_id" id="delete_state_id" onChange={this.handleChange}>
                                                <option value="0">Choisir un statut</option>
                                                {states.map(state => 
                                                    <option key={state.id} value={state.id}>{state.attributes.name}</option>
                                                )}
                                            </Select>
                                        </Description>
                                    </TextInfo>
                                    <PrimaryButton type="submit">Valider</PrimaryButton>
                                </Form>
                            </FormContainer>
                        </SubManageContainer>
                    </ManageContainer>
                </ContentWithPaddingXl>
            </Container>
        )
    }
}

export default withAlert()(ManageStateClass)