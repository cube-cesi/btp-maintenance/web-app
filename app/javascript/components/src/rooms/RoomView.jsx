import React, { Component, Fragment } from 'react'
import NavBarClass from '../../layouts/include/NavBarClass'
import FooterClass from '../../layouts/include/FooterClass';
import tw from "twin.macro";
import { css } from "styled-components/macro";
import AxiosHelper from '../../utils/AxiosHelper';
import axios from 'axios'
import { Redirect } from 'react-router';
import ShowRoomInformationsClass from '../../layouts/rooms/ShowRoomInformationsClass';

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class RoomView extends Component{
    constructor(props){
        super(props)
        this.state = {
            roomError: false,
            loaded: false
        }
    }

    componentDidMount(){
        this.checkRoomExist()
    }

    checkRoomExist = () => {
        AxiosHelper();
        axios.get(`/api/rooms/informations/${this.props.match.params.name_url}`)
        .then(response => {
            this.setState({roomError: response.data.error, loaded: true})
        })
    }

    render(){
        const {loaded, roomError} = this.state;

        if(loaded && roomError){
            return(<Redirect to="/"/>)
        }

        return(
            <StyledDiv>
                { loaded ? (
                    <Fragment>
                        <NavBarClass member={this.props.member} rank={this.props.rank} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect}/>
                        <ShowRoomInformationsClass isAnonymous={this.props.isAnonymous} member={this.props.member} name_url={this.props.match.params.name_url}/>
                        <FooterClass/>
                    </Fragment>
                ):(null)}
            </StyledDiv>
        )
    }
}

export default RoomView