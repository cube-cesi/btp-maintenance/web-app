import React, { Component } from 'react'
import NavBarClass from '../../layouts/include/NavBarClass'
import tw from "twin.macro";
import TabRoomsClass from '../../layouts/rooms/TabRoomsClass';

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class RoomsView extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <StyledDiv>
                <NavBarClass {...this.props} member={this.props.member} rank={this.props.rank} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect}/>
                <TabRoomsClass/>
            </StyledDiv>
        )
    }
}

export default RoomsView