import React, { Component, Fragment } from 'react'
import NavBarClass from '../layouts/include/NavBarClass'
import QuestionsClass from '../layouts/home/QuestionsClass'
import StartedBlockClass from '../layouts/home/StartedBlockClass'
import tw from "twin.macro";
import { css } from "styled-components/macro";
import {Redirect}  from 'react-router-dom'
import StartedStepClass from '../layouts/home/StartedStepClass'
import MobileBlockClass from '../layouts/home/MobileBlockClass';
import FooterClass from '../layouts/include/FooterClass';

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class Home extends Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <StyledDiv>
                <NavBarClass rank={this.props.rank} member={this.props.member} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect}/>
                <StartedBlockClass/>
                <StartedStepClass/>
                <MobileBlockClass/>
                <QuestionsClass/>
                <FooterClass/>
            </StyledDiv>
        );  
    }
};

export default Home;