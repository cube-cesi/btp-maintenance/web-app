import React, { Component } from 'react';
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import illustrationImageSrc from '../../../../assets/images/loginIllustration.svg'
import { Link, Redirect } from 'react-router-dom'
import tw from 'twin.macro';
import styled from 'styled-components';
import { Container as ContainerBase } from "../../layouts/misc/Container";
import { withAlert } from 'react-alert'
import App from '../../App'

const Container = tw(ContainerBase)`min-h-screen bg-gray-900 text-white font-medium flex justify-center`;
const Content = tw.div`max-w-screen-xl m-0 sm:mx-20 sm:my-16 bg-white text-gray-900 shadow sm:rounded-lg flex justify-center flex-1`;
const MainContainer = tw.div`lg:w-1/2 xl:w-5/12 p-6 sm:p-12`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold`;
const FormContainer = tw.div`w-full flex-1 mt-8`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-orange-400 text-gray-100 w-full py-4 rounded-lg hover:bg-orange-500 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
  `;

const SubmitAnonymousButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-gray-800 text-gray-100 w-full py-4 rounded-lg hover:bg-gray-900 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
`;

const Divider = tw.div`my-16 border-b-2 border-gray-300 w-full`;

const IllustrationContainer = tw.div`sm:rounded-r-lg flex-1 bg-orange-100 text-center hidden lg:flex justify-center`;
const IllustrationImage = styled.div`
  ${props => `background-image: url("${props.imageSrc}");`}
  ${tw`m-12 xl:m-16 w-full max-w-sm bg-contain bg-center bg-no-repeat`}
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      code2FA: '',
      generatedCode: '',
      dataConnect: '',
      show2FA: false,
      errors: '',
      redirect: false
    };
  }

  componentDidMount() {
    this.destroyAnonymous();

    if (this.props.alertDisconnect) {
      this.props.alert.error("Déconnecté")
    }
  }

  destroyAnonymous = () => {
    axios.get('/api/auth/is-anonymous')
      .then(response => {
        if (response.data.anonymous) {
          axios.get('/api/auth/remove-anonymous')
          this.props.handleAnonymous(false);
          this.props.alert.error("Anonymat désactivé")
        }
      })
  }

  handleChange = (event) => {
    const { name, value } = event.target
    this.setState({
      [name]: value
    })
  };

  generateCode2Fa() {
    let codeReturn = ""

    for (var i = 0; i < 6; i++) {
      codeReturn += Math.floor(Math.random() * Math.floor(9)).toString();
    }

    return codeReturn
  }

  verify2FACode = (event) => {
    event.preventDefault()
    const { code2FA, generatedCode, dataConnect } = this.state

    if (generatedCode === code2FA) {
      AxiosHelper()
      // Ajout du code2FA dans un cookie
      axios.post('/api/2fa/handle', { generatedCode2FA: generatedCode })
      this.createSession(dataConnect)
    } else {
      // afficher erreur code
    }

    this.render()
  }

  createSession = (member) => {
    AxiosHelper()
    axios.post('/api/auth/login', { member }, { withCredentials: true })
      .then(response => {
        axios.patch(`/api/members/last-connection/${response.data.member.id}`, { lastConnection: new Date().toLocaleString() })
        this.props.handleLogin(response.data)
        this.props.alert.success("Connecté")
        this.render()
      })
  }

  handleConnectAnonymous = (event) => {
    event.preventDefault()
    this.props.handleAnonymous();

    axios.get('/api/auth/anonymous')
      .then(response => {
        this.props.handleAnonymous(response.data.anonymous)
        this.props.alert.success("Anonymat activé")
        this.render()
      })
  }

  encrypt(text) {
    let crypto = require('crypto'),
      algorithm = 'aes-256-cbc',
      key = 'jMitwfdrSXLQzKvij33vsN2BweAJiGgT', // 32 Characters
      iv = "YfhaFH4BfUmUth87"; // 16 Characters

    var cipher = crypto.createCipheriv(algorithm, key, iv)
    var crypted = cipher.update(text, 'utf-8', "base64")
    crypted += cipher.final("base64");
    return crypted;
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { email, password } = this.state
    let member = {
      email: email,
      password: password
    }
    AxiosHelper()
    axios.post('/api/auth/checkLogin', { member }, { withCredentials: true })
      .then(response => {
        if (response.data.logged_in) {
          //2FA actived
          if (response.data.member.activate2FA) {
            const generatedCode2FA = this.generateCode2Fa()
            // Envoi du code par mail

            let cryptedCode = this.encrypt(generatedCode2FA);

            AxiosHelper()
            axios.post('/api/2fa/send', { email: member.email, generatedCode2FA: cryptedCode })
            this.setState({
              show2FA: true,
              generatedCode: generatedCode2FA,
              dataConnect: member
            })

            this.render(generatedCode2FA, response.data)
          } else {
            this.createSession(member)
          }
        }
      })
      .catch(error => this.props.alert.error("Erreur : " + error.response.data.error))
  };

  handleErrors = () => {
    return (
      <div>
        <ul>
          {this.state.errors.map(error => {
            return <li key={error}>{error}</li>
          })}
        </ul>
      </div>
    )
  };

  redirectToRegister() {
    this.setState({ redirect: true });
  }

  render() {
    const { email, password, show2FA, code2FA, redirect } = this.state
    return (
      <Container>
        {
          redirect ? (
            <Redirect to="/register" />
          ) : (
              <Content>
                <MainContainer>
                  <LogoLink href="#">
                    <LogoImage src="https://yt3.ggpht.com/a/AATXAJwHM242C6zlkiNyKhjc0jRq9WOcz0LdBMU-UHhF9Q=s900-c-k-c0x00ffffff-no-rj" />
                  </LogoLink>
                  <MainContent>
                    <Heading>Se connecter</Heading>
                    <FormContainer>
                      {show2FA ? (
                        <Form onSubmit={this.verify2FACode}>
                          <Input
                            placeholder="Code 2FA"
                            type="text"
                            name="code2FA"
                            value={code2FA}
                            onChange={this.handleChange}
                          />
                          <SubmitButton placeholder="submit" type="submit">
                            <span className="text">Valider</span>
                          </SubmitButton>
                        </Form>
                      ) : (
                          <Form onSubmit={this.handleSubmit}>
                            <Input
                              placeholder="Adresse e-mail"
                              type="email"
                              name="email"
                              value={email}
                              onChange={this.handleChange}
                            />
                            <Input
                              placeholder="Mot de passe"
                              type="password"
                              name="password"
                              value={password}
                              onChange={this.handleChange}
                            />
                            <SubmitButton type="submit">
                              <span className="text">Valider</span>
                            </SubmitButton>
                            <Divider />
                            <SubmitAnonymousButton onClick={() => this.redirectToRegister()} type="button">
                              <span className="text">S'incrire</span>
                            </SubmitAnonymousButton>
                            <SubmitAnonymousButton onClick={this.handleConnectAnonymous} type="button">
                              <span className="text">Se connecter en anonyme</span>
                            </SubmitAnonymousButton>
                          </Form>
                        )}
                    </FormContainer>
                  </MainContent>
                </MainContainer>
                <IllustrationContainer>
                  <IllustrationImage imageSrc={illustrationImageSrc} />
                </IllustrationContainer>
              </Content>
            )
        }
      </Container>
    );
  }
}

export default withAlert()(Login);