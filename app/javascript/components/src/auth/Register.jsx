import React, { Component } from 'react';
import axios from 'axios'
import AxiosHelper from '../../utils/AxiosHelper'
import illustrationImageSrc from '../../../../assets/images/loginIllustration.svg'
import tw from 'twin.macro';
import styled from 'styled-components';
import { Container as ContainerBase } from "../../layouts/misc/Container";
import { withAlert } from 'react-alert'
import { Link, Redirect } from 'react-router-dom'

const Container = tw(ContainerBase)`min-h-screen bg-gray-900 text-white font-medium flex justify-center`;
const Content = tw.div`max-w-screen-xl m-0 sm:mx-20 sm:my-16 bg-white text-gray-900 shadow sm:rounded-lg flex justify-center flex-1`;
const MainContainer = tw.div`lg:w-1/2 xl:w-5/12 p-6 sm:p-12`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold`;
const FormContainer = tw.div`w-full flex-1 mt-8`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-orange-400 text-gray-100 w-full py-4 rounded-lg hover:bg-orange-500 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
  `;

const SubmitAnonymousButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-gray-800 text-gray-100 w-full py-4 rounded-lg hover:bg-gray-900 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
      .icon {
        ${tw`w-6 h-6 -ml-2`}
      }
      .text {
        ${tw`ml-3`}
      }
`;

const Divider = tw.div`my-16 border-b-2 border-gray-300 w-full`;

const IllustrationContainer = tw.div`sm:rounded-r-lg flex-1 bg-orange-100 text-center hidden lg:flex justify-center`;
const IllustrationImage = styled.div`
  ${props => `background-image: url("${props.imageSrc}");`}
  ${tw`m-12 xl:m-16 w-full max-w-sm bg-contain bg-center bg-no-repeat`}
`;

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            dataConnect: '',
            errors: '',
            redirectLogin: false
        };
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    };

    handleSubmit = (event) => {
        event.preventDefault()
        const { firstname, lastname, email, password } = this.state
        let member = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            rank_id: 4
        }

        AxiosHelper()
        axios.post('/api/members', { member }, { withCredentials: true })
            .then(response => {
                this.props.alert.success("Mail de vérification envoyé.")
                this.setState({ firstname: "", lastname: "", email: "", password: "" });
            })
            .catch(error => this.props.alert.error("Erreur : " + error.response.data.error))
    };

    redirectToLogin() {
        this.setState({ redirectLogin: true });
    }

    render() {
        const { email, password, firstname, lastname, redirectLogin } = this.state

        return (
            <Container>
                {
                    redirectLogin &&
                    <Redirect to="/login" />
                }
                <Content>
                    <MainContainer>
                        <LogoLink href="#">
                            <LogoImage src="https://yt3.ggpht.com/a/AATXAJwHM242C6zlkiNyKhjc0jRq9WOcz0LdBMU-UHhF9Q=s900-c-k-c0x00ffffff-no-rj" />
                        </LogoLink>
                        <MainContent>
                            <Heading>S'inscrire</Heading>
                            <FormContainer>
                                <Form onSubmit={this.handleSubmit}>
                                    <Input
                                        placeholder="Prénom"
                                        type="text"
                                        name="firstname"
                                        value={firstname}
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Nom de famille"
                                        type="text"
                                        name="lastname"
                                        value={lastname}
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Adresse e-mail"
                                        type="email"
                                        name="email"
                                        value={email}
                                        onChange={this.handleChange}
                                    />
                                    <Input
                                        placeholder="Mot de passe"
                                        type="password"
                                        name="password"
                                        value={password}
                                        onChange={this.handleChange}
                                    />
                                    <SubmitButton type="submit">
                                        <span className="text">Valider</span>
                                    </SubmitButton>
                                    <Divider />
                                    <SubmitAnonymousButton onClick={() => this.redirectToLogin()} type="button">
                                        <span className="text">Se connecter</span>
                                    </SubmitAnonymousButton>
                                </Form>
                            </FormContainer>
                        </MainContent>
                    </MainContainer>
                    <IllustrationContainer>
                        <IllustrationImage imageSrc={illustrationImageSrc} />
                    </IllustrationContainer>
                </Content>
            </Container>
        );
    }
}

export default withAlert()(Register);