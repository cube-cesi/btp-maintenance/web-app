import React, { Component } from 'react'
import TabFloorClass from '../../layouts/map/TabFloorClass'
import NavBarClass from '../../layouts/include/NavBarClass'
import tw from "twin.macro";

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class MapView extends Component{
    constructor(props){
        super(props)
    }

    render(){

        return(
            <StyledDiv>
                <NavBarClass {...this.props} member={this.props.member} rank={this.props.rank} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect}/>
                <TabFloorClass/>
            </StyledDiv>
        )
    }
}

export default MapView
