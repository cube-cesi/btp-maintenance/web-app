import React, { Component } from 'react'
import NavBarClass from '../../layouts/include/NavBarClass'
import FooterClass from '../../layouts/include/FooterClass';
import tw from "twin.macro";
import {BrowserRouter, Switch, Route, Redirect}  from 'react-router-dom'
import { css } from "styled-components/macro";
import AxiosHelper from '../../utils/AxiosHelper';
import axios from 'axios'
import TabIssueClass from '../../layouts/issues/TabIssueClass';

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class IssueView extends Component{
    constructor(props){
        super(props)
        this.state = {
            redirect: false
        }
    }

    componentDidMount(){
        this.redirectAnonymous()
    }

    redirectAnonymous = () => {
        this.setState({redirect: this.props.isAnonymous})
    }

    render(){
        const { redirect } = this.state
        if (redirect){
            return(
                <Redirect to="/"/>
            )
        }
        return(
            <StyledDiv>
                <NavBarClass member={this.props.member} handleDesactivateAnonymous={this.props.handleDesactivateAnonymous} handleDisconnect={this.props.handleDisconnect} rank={this.props.rank}/>
                <TabIssueClass member={this.props.member}/>
                <FooterClass/>
            </StyledDiv>
        )
    }
}

export default IssueView