import React, { Component, Fragment } from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro";
import SideBarClass from "../../layouts/include/SideBarClass";
import Map from "../../layouts/dashboard/Map/Map";
import ManageTabIssueClass from "../../layouts/dashboard/Issue/ManageTabIssueClass";
import ManageStateClass from "../../layouts/dashboard/Issue/ManageStateClass";
import ManagePriorityClass from "../../layouts/dashboard/Issue/ManagePriorityClass";
import ManageRoomsClass from "../../layouts/dashboard/Map/ManageRoomsClass";
import ManageFloorsClass from "../../layouts/dashboard/Map/ManageFloorsClass";
import ManageMaterialsClass from "../../layouts/dashboard/Map/ManageMaterialsClass";
import ManageUsersClass from "../../layouts/dashboard/User/ManageUsersClass";
import ManageHelpClass from "../../layouts/dashboard/Home/ManageHelpClass";
import ManageSmtpClass from "../../layouts/dashboard/Home/ManageSmtpClass";
import ManageDomainsClass from "../../layouts/dashboard/Home/ManageDomainsClass";

const Container = tw.div`bg-white font-sans`
const SubContainer = tw.div`flex flex-col lg:flex-row`;
const NavContainer = tw.div`lg:w-2/12`
const ElementContainer = tw.div`lg:w-10/12`

const StyledDiv = tw.div`font-display min-h-screen text-secondary-500 p-8 overflow-hidden`;

class Dashboard extends Component{
    constructor(props){
        super(props)

        this.state = {
            element: "manageMembers"
        }
    }

    modifyElementRender = (attribute) => {
        this.setState({element: attribute})
    }

    render(){
        const {element} = this.state;

        return(
            <StyledDiv>
                <Container>
                    <SubContainer>
                        <NavContainer>
                            <SideBarClass rank={this.props.rank} modifyElementRender={this.modifyElementRender}/>
                        </NavContainer>
                        <ElementContainer>
                            {element === "home" && 
                                <div>accueil</div>
                            }
                            {element === "mapRooms" && 
                                <Map openModal={true}/>
                            }
                            {element === "manageRooms" && 
                                <ManageRoomsClass/>
                            }
                            {element === "manageFloors" &&
                                <ManageFloorsClass/>
                            }
                            {element === "showIssues" && 
                                <ManageTabIssueClass rank={this.props.rank} member={this.props.member}/>
                            }
                            {element === "manageStates" && 
                                <ManageStateClass/>
                            }
                            {element === "managePriorities" &&
                                <ManagePriorityClass/>
                            }
                            {element === "manageUsers" &&
                                <ManageUsersClass/>
                            }
                            {element === "manageHelpers" &&
                                <ManageHelpClass/>
                            }
                            {element === "manageSmtp" &&
                                <ManageSmtpClass/>
                            }
                            {
                                element === "manageDomains" &&
                                <ManageDomainsClass/>
                            }
                            {
                                element === "manageMaterials" &&
                                <ManageMaterialsClass/>
                            }
                        </ElementContainer>
                    </SubContainer>
                </Container>
            </StyledDiv> 
        )
    }
}

export default Dashboard