class MemberSerializer
  include FastJsonapi::ObjectSerializer
  attributes :firstname, :lastname, :email, :phone, :rank_id, :user_verified
end
