class IssueSerializer
    include FastJsonapi::ObjectSerializer
    attributes :description, :endDate, :material_id, :member_id, :technician, :state_id, :title, :priority_id
end
  