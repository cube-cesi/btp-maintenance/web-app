class PrioritySerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :order

  has_many :issues
end
