class CommentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :member_id, :issue_id, :description
end
