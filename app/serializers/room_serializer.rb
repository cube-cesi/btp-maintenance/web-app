class RoomSerializer
    include FastJsonapi::ObjectSerializer
    attributes :name, :position_x, :position_y, :localisation_gps, :name_url, :floor_id
  
    has_many :materials
  end  