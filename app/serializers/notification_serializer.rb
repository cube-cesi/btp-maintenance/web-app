class NotificationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :description, :read, :member_id
end
