class RoomMemberSerializer
  include FastJsonapi::ObjectSerializer
  attributes :room_id, :member_id
  
  has_one :member
  has_one :room
end
