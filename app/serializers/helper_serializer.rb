class HelperSerializer
  include FastJsonapi::ObjectSerializer
  attributes :heading, :description
end
