class MaterialSerializer
    include FastJsonapi::ObjectSerializer
    attributes :name, :quantity, :room_id, :created_at
  end
  