class SettingSerializer
  include FastJsonapi::ObjectSerializer
  attributes :mailSmtpAddress, :mailSmtpPort, :mailAddress, :encrypted_pswd, :pswd
end
