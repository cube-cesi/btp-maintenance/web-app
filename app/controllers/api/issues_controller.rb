require './lib/src/smtp.rb'

class Api::IssuesController < ApplicationController
    #protect_from_forgery with: :null_session
    def create
        issue = Issue.new(issue_params.except(:anonyme))
        if (Issue.joins(:state).all.where(material_id: issue_params["material_id"]).where.not(states: {name: "Fermé"}).count > 0)
            render json: { error: "Un ticket est déjà en cours pour ce matériel." }
        else
            if (issue_params[:anonyme] == false && !helpers.is_anonymous?)
                issue.member_id = helpers.current_member["id"]
            else
                issue.member_id = nil
            end
            if issue.save
                notify_room_users(issue)
                render json: IssueSerializer.new(issue).serialized_json
            else
                render json: { error: "Une erreur est survenue, merci de ré-essayer." }
            end
        end
    end

    def create_mobile
        issue = Issue.new(issue_params_mobile)
        if (Issue.joins(:state).all.where(material_id: issue_params["material_id"]).where.not(states: {name: "Fermé"}).count > 0)
            render json: { error: "Un ticket est déjà en cours pour ce matériel." }
        else
            if (params[:anonyme] == false && !helpers.is_anonymous?)
                issue.member_id = helpers.current_member["id"]
            else
                issue.member_id = nil
            end
            if issue.save
                notify_room_users(issue)
                render json: IssueSerializer.new(issue).serialized_json
            else
                render json: { error: "Une erreur est survenue, merci de ré-essayer." }
            end
        end
    end
    
    def index 
        if !helpers.current_member.nil?
            issues = Issue.joins(:priority, :state, :member, {material: :room})
            .select("issues.*", "priorities.name as priority", "states.name as state", "materials.name as materialName", "members.firstname as memberFirstname", "members.lastname as memberLastname", "rooms.name as roomName")
            .where(member_id: helpers.current_member["id"])

            render json: {data: issues}
        else
            render json: {error: "Veuillez vous connecter pour voir vos tickets"}
        end
    end

    def all
        issues = Issue.joins(:priority, :state, {material: :room}).left_joins(:member)
        .select("issues.*", "priorities.name as priority", "states.name as state", "materials.name as materialName", "members.firstname as memberFirstname", "members.lastname as memberLastname", "rooms.name as roomName")

        render json: {data: issues}
    end

    def allByMember
        issues = Issue.joins(:priority, :state, {material: :room}).left_joins(:member)
        .select("issues.*", "priorities.name as priority", "states.name as state", "materials.name as materialName", "members.firstname as memberFirstname", "members.lastname as memberLastname", "rooms.name as roomName")
        .where.not(states: {name: "Fermé"}).where(member_id: params[:id])

        render json: {data: issues}
    end

    def show 
        issue = Issue.joins(:priority, :state, {material: :room}).left_joins(:member)
        .select("issues.*", "priorities.name as priority", "states.name as state", "rooms.name as roomName", "materials.name as materialName",
                "members.firstname as memberFirstname", "members.lastname as memberLastname")
        .find(params[:id])

        comments = Comment.joins(:member)
        .select("comments.*", "members.firstname as memberFirstname", "members.lastname as memberLastname")
        .where(issue_id: issue.id).order(created_at: :desc)

        render json: {data: issue, comments: comments}
    end
    
    def destroy
        issue = Issue.find(params[:id])

        if issue.destroy
            head :no_content
        else
            render json: { error: issue.errors.messages }, status: 422
        end
    end
    
    def update
        issue = Issue.find(params[:id])
        old_id = issue.state_id

        if issue.update(manage_issue_params)
            if old_id != issue.state_id
                notify_issue_creator(issue)
            end
        end
    end

    def index_by_room
        issues = Issue.joins(:state, {material: :room}).left_joins(:member).select("issues.*", "materials.name as materialName", "states.name as stateName", "members.lastname as lastname", "members.firstname as firstname" ).all.where.not(states: {name: "Fermé"}).where(rooms: {name_url: params[:name_url]})
        render json: issues
    end

    def update_technicien
        issue = Issue.find(params[:id])
        issue.update(issue_update_technicien)
    end

    def update_enddate
        issue = Issue.find(params[:id])
        issue.update(issue_update_enddate)
    end
    
    private
    
    def issue_params
        if params.require(:issue).has_key?(:anonyme)
            params.require(:issue).permit(:description, :priority_id, :material_id, :member_id, :state_id, :title, :anonyme)
        else
            params.require(:issue).permit(:description, :priority_id, :material_id, :member_id, :state_id, :title)
        end
    end

    def issue_params_mobile
        params.require(:issue).permit(:description, :priority_id, :material_id, :member_id, :state_id, :title)
    end

    def manage_issue_params
        params.require(:issue).permit(:priority_id, :state_id, :technician)
    end

    def issue_update_technicien
        params.require(:issue).permit(:technician)
    end

    def issue_update_enddate
        params.require(:issue).permit(:endDate)
    end

    def notify_room_users(issue)
        material = Issue.find(issue.id).material
        room = material.room
        room.members.each do |member|
            sendEmail(member.email, "CESI : Un nouveau ticket à été crée concernant la salle #{room.name}", 
                "Un <i>ticket</i> de priorité <ins>#{issue.priority.name}</ins> concernant le matériel <b>#{material.name}</b> à été crée.
                <br>La description du <i>ticket</i> est :</br>
                #{issue.description}")
        end
    end

    def notify_issue_creator(issue)
        member = issue.member
        # On vérifie que le ticket na pas été crée par un anonyme
        if !member.nil?
            state = issue.state.name
            material = issue.material.name
            room = issue.material.room.name
            sendEmail(member.email, "CESI : L'état d'un de vos ticket à changé",
                "Le ticket nommé <u>#{issue.title}</u> concernant le matériel <i>#{material}</i> de la <i>#{room}</i> est passé à l'état <b>#{state}</b>."
            )
        end
    end
end