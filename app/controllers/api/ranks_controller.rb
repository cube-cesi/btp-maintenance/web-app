class Api::RanksController < ApplicationController

    def index
        ranks = Rank.all

        render json: RankSerializer.new(ranks).serialized_json
    end

    def show
        rank = Rank.find_by(id: params[:id])

        render json: RankSerializer.new(rank).serialized_json
    end

    def create
        rank = Rank.new(rank_params)

        if rank.save
            render json: RankSerializer.new(rank).serialized_json
        else
            render json: { error: rank.errors.messages }, status: 422
        end
    end

    def update
        rank = Rank.find(id: params[:id])
        rank.update(rank_params)
    end

    private

    def rank_params
        params.require(:rank).permit(:name)
    end
end