class Api::RoomMembersController < ApplicationController

    def index
        room_members = RoomMember.all

        render json: room_members
    end

    def notified
        member_id = !current_member.nil? ? helpers.current_member["id"] : nil
        room_member = RoomMember.find_by(room_id: params[:room_id], member_id: member_id)
        render json: {notified: (!room_member.nil?)}
    end

    def show_by_room
        room_members = RoomMember.all.where(room_id: params[:room_id])

        render json: room_members
    end

    def show_by_member
        room_members = RoomMember.all.where(member_id: params[:member_id])

        render json: room_members
    end

    def create
        room_member = RoomMember.new(room_member_params)

        if room_member.save
            render json: room_member
        else
            render json: { error: room_member.errors.messages }, status: 422
        end
    end

    def update
        room_member = RoomMember.find_by(room_id: room_member_params[:room_id], member_id: room_member_params[member_id])

        if room_member.update(room_member_params)
            render json: room_member
        else
            render json: { error: room_member.errors.messages }, status: 422
        end
    end

    def delete
        room_member = RoomMember.find_by(room_id: room_member_params[:room_id], member_id: room_member_params[:member_id])
        # ActiveRecord ne supporte pas la supression des tables à clef primaire composite
        if !room_member.nil?
            ActiveRecord::Base.connection.execute("DELETE FROM room_members WHERE member_id = #{room_member.member_id} AND room_id = #{room_member.room_id};")
        end
    end

    def destroy_by_room
        room_members = RoomMember.all.where(room_id: params[:room_id])
        # ActiveRecord ne supporte pas la supression des tables à clef primaire composite
        if room_members.length > 0
            ActiveRecord::Base.connection.execute("DELETE FROM room_members WHERE room_id = #{room_members.first.room_id};")
        end
    end

    def destroy_by_member
        room_members = RoomMember.all.where(member_id: params[:member_id])
        # ActiveRecord ne supporte pas la supression des tables à clef primaire composite
        if room_members.length > 0
            ActiveRecord::Base.connection.execute("DELETE FROM room_members WHERE member_id = #{room_members.first.member_id};")
        end
    end

    private

    def room_member_params
        params.require(:room_member).permit(:room_id, :member_id)
    end
end