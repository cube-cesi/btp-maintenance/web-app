class Api::SettingsController < ApplicationController

    def index
        setting = Setting.first
        render json: SettingSerializer.new(setting).serialized_json
    end

    def updateMailSettings
        setting = Setting.first

        if setting.update(mail_settings_params)
            render json: SettingSerializer.new(setting).serialized_json
        else
            render json: { error: setting.errors.messages }, status: 422
        end
    end

    private

    def mail_settings_params
        params.require(:setting).permit(:mailSmtpAddress, :mailSmtpPort, :mailAddress,:pswd)
    end
end