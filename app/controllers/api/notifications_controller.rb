class Api::NotificationsController < ApplicationController

    def index
        notifications = Notification.all

        render json: NotificationSerializer.new(notifications).serialized_json
    end

    def show
        notification = Notification.find(params[:id])

        render json: NotificationSerializer.new(notification).serialized_json
    end

    def create
        notification = Notification.new(notification_params)

        if notification.save
            render json: NotificationSerializer.new(notification).serialized_json
        else
            render json: { error: notification.errors.messages }, status: 422
        end
    end

    def update
        notification = Notification.find(params[:id])

        if notification.update(notification_params)
            render json: NotificationSerializer.new(notification).serialized_json
        else
            render json: { error: notification.errors.messages }, status: 422
        end
    end

    def destroy
        notification = Notification.find(params[:id])

        if notification.destroy
            head :no_content
        else
            render json: { error: notification.errors.messages }, status: 422
        end
    end

    private

    def notification_params
        params.require(:notification).permit(:description, :read, :member_id)
    end
end