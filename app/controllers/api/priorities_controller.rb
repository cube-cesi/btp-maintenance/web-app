class Api::PrioritiesController < ApplicationController

    def index
        priorities = Priority.all.order(:order)

        render json: PrioritySerializer.new(priorities).serialized_json
    end

    def show
        priority = Priority.find(params[:id])

        render json: PrioritySerializer.new(priority).serialized_json
    end

    def create
        priority = Priority.new(priority_params)

        if priority.save
            render json: PrioritySerializer.new(priority).serialized_json
        else
            render json: { error: priority.errors.messages }, status: 422
        end
    end

    def update
        priority = Priority.find(params[:id])

        if priority.update(priority_params)
            render json: PrioritySerializer.new(priority).serialized_json
        else
            render json: { error: priority.errors.messages }, status: 422
        end
    end

    def destroy
        priority = Priority.find(params[:id])

        if priority.destroy
            head :no_content
        else
            render json: { error: priority.errors.messages }, status: 422
        end
    end

    private

    def priority_params
        params.require(:priority).permit(:name, :order)
    end
end