class Api::RoomsController < ApplicationController
    def index
        rooms = Room.all

        render json: RoomSerializer.new(rooms, options).serialized_json
    end

    def index_coords
        rooms = Room.where.not(localisation_gps: nil)
        room_list = Array.new
        user_coords = params[:localisation]
        rooms.each do |room|
            room_x = Float(room.localisation_gps.split(',')[0]).ceil
            room_y = Float(room.localisation_gps.split(',')[1]).ceil
            room_z = Float(room.localisation_gps.split(',')[2]).ceil
            if ("#{room_x},#{room_y},#{room_z}" == user_coords)
                room_list.push(room)
            end
        end
        render json: RoomSerializer.new(room_list, options).serialized_json
    end

    def show
        if(params.has_key?(:id))
            room = Room.find(params[:id])
        else
            room = Room.find_by(name_url: params[:name_url])
        end
        render json: RoomSerializer.new(room, options).serialized_json
    end
    
    def create
        room = Room.new(room_params)

        if room.save
            render json: RoomSerializer.new(room, options).serialized_json
        else
            render json: { error: room.errors.messages }, status: 422
        end
    end

    def update
        room = Room.find_by(name_url: params[:name_url])
        room.update(room_params)
    end

    def destroy
        room = Room.find_by(name_url: params[:name_url])
    
        if room.destroy
            head :no_content
        else
            render json: { error: room.errors.messages }, status: 422
        end
    end

    def delete
        room = Room.find(params[:id])
        room.destroy
        redirect_to :back
    end

    def index_floor
        rooms = Room.all.where(floor_id: params[:floor_id])

        render json: RoomSerializer.new(rooms, options).serialized_json
    end

    def index_floor_mobile
        rooms = Room.select("rooms.*", "(SELECT COUNT(*) FROM issues i WHERE i.material_id IN
            (SELECT m.id FROM materials m WHERE m.room_id = rooms.id)
        ) as issues")
        .where(floor_id: params[:floor_id])

        render json: rooms
    end

    def informations
        room = Room.find_by(name_url: params[:name_url])
        if (room.nil?)
            render json:{
                error: true
            }
        else
            issuesNumbers = Room.joins(materials: :issues).where(id: room.id).count
            floor = Floor.find_by(id: room.floor_id)
            materials = Material.all.where(room_id: room.id)

            render json: {
                roomName: room.name,
                issueCounters: issuesNumbers,
                floorName: floor.name,
                materials: materials,
                error: false
            }
        end
    end

    def notified
        member_id = !current_member.nil? ? helpers.current_member["id"] : nil
        room_member = RoomMember.find_by(room_id: params[:room_id], member_id: member_id)
        render json: {notified: (!room_member.nil?)}
    end

    private

    def room_params
        params.require(:room).permit(:name, :position_x, :position_y, :localisation_gps, :floor_id)
    end

    def options
        @options ||= {include: %i[materials] }
    end
end
