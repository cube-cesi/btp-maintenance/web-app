class Api::StatesController < ApplicationController
    def index
        states = State.all.order(:order)

        render json: StateSerializer.new(states).serialized_json
    end

    def create
        state = State.new(state_params)        
        state_verif = State.find_by(order: state.order)
        
        if state_verif.nil?
            if state.save
                render json: StateSerializer.new(state).serialized_json
            else
                render json: { error: state.errors.messages }, status: 422
            end
        else
            render json: { error: "Cet ordre est déjà utilisé" }, status: 422
        end
    end

    def show
        state = State.find(params[:id])
        render json: StateSerializer.new(state).serialized_json
    end

    def update
        state = State.find_by(id: params[:id])
        state_verif = nil
        if state.order != state_params["order"]
            state_verif = State.find_by(order: state_params["order"])
        end
        
        if state_verif.nil?
            if state.update(state_params)
                render json: StateSerializer.new(state).serialized_json
            else
                render json: { error: state.errors.messages }, status: 422
            end
        else
            render json: { error: "Cet ordre est déjà utilisé" }, status: 422
        end
    end

    def destroy
        state = State.find_by(id: params[:id])
    
            if state.destroy
                head :no_content
            else
                render json: { error: state.errors.messages }, status: 422
            end
    end

    def manageState
        states = State.all.where.not(name: ["Fermé", "Ouvert"]).order(:order)
        render json: StateSerializer.new(states).serialized_json
    end

    def getIdCloseState
        states = State.where(states: {name: "Fermé"})
        render json: {data: states}
    end

    private

    def state_params
        params.require(:state).permit(:name, :order)
    end
end
