class Api::EmailDomainsController < ApplicationController
  def index
    domains = EmailDomain.all

    render json: EmailDomainSerializer.new(domains).serialized_json
  end

  def create
    domain = EmailDomain.new(email_domains_params)

    if domain.save
      render json: EmailDomainSerializer.new(domain).serialized_json
    else
      render json: { error: domain.errors.messages }, status: 422
    end
  end

  def update
    domain = EmailDomain.find_by(id: email_domains_params[:id])

    if domain.update(email_domains_params.except(:id))
      render json: domain
    else
      render json: { error: domain.errors.messages }, status: 422
    end
  end

  def show
    domain = EmailDomain.find(params["id"])

    render json: EmailDomainSerializer.new(domain).serialized_json
  end

  def destroy
    domain = EmailDomain.find(params["id"])

    if domain.destroy
      head :no_content
    else
      render json: { error: domain.errors.messages }, status: 422
    end
  end

  def email_domains_params
    params.require(:email_domain).permit(:id, :name)
  end
end
