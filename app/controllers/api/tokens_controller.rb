require 'securerandom'
require './lib/src/smtp.rb'

class Api::TokensController < ApplicationController
    def create
        memberToken = Member.find(params["id"])
        token = genToken
        url_validation = "#{request.base_url}/api/members/validate_token/#{token}"
        memberToken.update(token: token)

        sendEmail(memberToken.email, 'CESI : validation de votre compte', 
        "Bonjour #{memberToken.firstname} #{memberToken.lastname},<br>
        Pour valider votre compte, veuillez cliquer sur le lien ci dessous :<br>        
        <a href=\"#{url_validation}\">Valider votre compte</a>")

        render json: MemberSerializer.new(memberToken).serialized_json
    end

    def validate_token
        memberValidate = Member.find_by(token: params[:token])
        if !memberValidate.nil?
            if !memberValidate.user_verified
                memberValidate.update(user_verified: true)
            end
        end
        
        redirect_to "/"
    end

    private

    def genToken(size = 64)
        return SecureRandom.alphanumeric(size)
    end
end