require './lib/src/smtp.rb'
require 'openssl'
require 'base64'

class Api::TwofaController < ApplicationController
    def sender
        sendEmail(params[:email], 'Votre code complémentaire à votre connexion', 
            "Le code pour vous <ins><i>connecter</i></ins> est : <b>#{decrypt(params[:generatedCode2FA]).strip}</b>")
    end

    def handle2FA
        if cookies.encrypted[:code2FA].blank?
            cookies.encrypted[:code2FA] = { value: params[:generatedCode2FA], expires: 1.hour }
        end
    end

    def decrypt(des_text)
        $alg = 'aes-256-cbc'
        $key = 'jMitwfdrSXLQzKvij33vsN2BweAJiGgT'  ## 32 Characters
        $iv = 'YfhaFH4BfUmUth87' ## 16 characters

        des = OpenSSL::Cipher::Cipher.new($alg)
        des.decrypt
        des.key = $key
        des.iv = $iv
        result = des.update(Base64.decode64(des_text))
        result << des.final
        return result
    end
end