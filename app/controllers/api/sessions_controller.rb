class Api::SessionsController < ApplicationController
    def create
        @member = Member.find_by_email(session_params[:email])

        if @member && @member.authenticate(session_params[:password])
            login!
            render json: {
                logged_in: true,
                member: @member,
                rank: @member.rank.name,
                verified: @member.user_verified
            }
        else
            render json: { error: 'Aucun membre trouvé' }, status: 422
        end
    end

    def checkLogin
      email_domain = session_params[:email].split('@')[1]
      validate = EmailDomain.find_by(name: email_domain)
      if !validate.nil?
        @member = Member.find_by_email(session_params[:email])
        if !@member.nil?
          if !@member.user_verified
            render json: { error: 'Utilisateur non vérifié' }, status: 422
          else
            if @member && @member.authenticate(session_params[:password])
              render json: {
                  logged_in: true,
                  member: @member,
                  rank: @member.rank.name
              }
            else
              render json: { error: 'Mot de passe erroné' }, status: 422
            end
          end
        else
          render json: { error: 'Adresse mail incorrect' }, status: 422
        end
      else
        render json: { error: 'Adresse mail non autorisé' }, status: 422
      end
    end

    def create_anonymous
      login_anon!
      if is_anonymous?
        render json: {
          anonymous: true
        }
      else
        render json: { error: 'Mode anonyme non disponible' }, status: 422
      end
    end

    def is_logged_in?
        if logged_in? && current_member
          render json: {
            logged_in: true,
            member: current_member,
            rank: current_member.rank.name
          }
        else
          render json: {
            logged_in: false,
            message: 'Aucun membre'
          }
        end
    end

    def anonymous?
        render json: {anonymous: (is_anonymous?)}
    end

    def remove_anonymous
      session[:anon] = false
      render json: {result: (!is_anonymous?)}
    end

    def destroy
        logout!
        render json: {
          status: 200,
          logged_out: true
        }
    end

    private

    def session_params
        params.require(:member).permit(:email, :password)
    end
end