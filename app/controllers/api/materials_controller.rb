class Api::MaterialsController < ApplicationController
    protect_from_forgery with: :null_session

    def create
        material = Material.new(material_params)

        if material.save
            render json: MaterialSerializer.new(material).serialized_json
        else
            render json: { error: material.errors.messages }, status: 422
        end
    end

    def show 
        material = Material.find(params[:id])
        render json: MaterialSerializer.new(material).serialized_json
    end

    def index 
        materials = Material.all
        render json: MaterialSerializer.new(materials).serialized_json
    end

    def index_by_room
        materials = Material.joins(:room).where(rooms: {name_url: params[:name_url]})
        render json: materials
    end

    def destroy
        material = Material.find(params[:id])

        if material.destroy
            head :no_content
        else
            render json: { error: material.errors.messages }, status: 422
        end
    end

    def update
        material = Material.find_by(id: params[:id])
        material.update(material_params)
    end

    private

    def material_params
        params.require(:material).permit(:id, :name, :quantity, :room_id)
    end
end