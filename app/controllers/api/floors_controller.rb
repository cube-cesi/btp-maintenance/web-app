class Api::FloorsController < ApplicationController
    def index
        floors = Floor.all

        render json: floors
    end

    def show
        floor = Floor.find_by(id: params[:id])

        render json: floor
    end

    def create
        floor = Floor.new(floor_params.except(:id))

        if floor.save
            render json: floor
        else
            render json: { error: floor.errors.messages }, status: 422
        end
    end

    def update
        floor = Floor.find_by(id: floor_params[:id])

        if floor.update(floor_params.except(:id))
            render json: floor
        else
            render json: { error: floor.errors.messages }, status: 422
        end
    end

    def update_img
        floor = Floor.find_by(id: params[:id])

        if floor.update(plan_source: params["image"].original_filename, image: params["image"])
            render json: floor
        else
            render json: { error: floor.errors.messages }, status: 422
        end
    end

    def destroy
        floor = Floor.find_by(id: params[:id])
    
            if floor.destroy
                head :no_content
            else
                render json: { error: floor.errors.messages }, status: 422
            end
        end

    private

    def floor_params
        params.permit(:id, :name, :plan_source, :image)
    end
end
