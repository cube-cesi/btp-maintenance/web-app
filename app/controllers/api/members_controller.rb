class Api::MembersController < ApplicationController

    def index
        members = Member.all

        render json: MemberSerializer.new(members).serialized_json
    end

    def all_technicians
        members = Member.joins(:rank).all.where(ranks: {name: "Technicien"})

        render json: MemberSerializer.new(members).serialized_json
    end

    def show
        member = Member.find_by(id: params[:id])

        render json: MemberSerializer.new(member).serialized_json
    end

    def create
        member = Member.new(member_params)

        if member.save
            redirect_to "/api/members/token/#{member.id}"
        else
            render json: { error: member.errors.messages }, status: 422
        end
    end

    def update
        member = Member.find_by(id: params[:id])
        member.update(member_params)
    end

    def destroy
        member = Member.find_by(id: params[:id])
    
        if member.destroy
            head :no_content
        else
            render json: { error: member.errors.messages }, status: 422
        end
    end

    def updateLastConnection
        member = Member.find_by(id: params[:id])
        
        if member.update(update_last_connection_param)
            render json: MemberSerializer.new(member).serialized_json
        else
            render json: { error: member.errors.messages }, status: 422
        end
    end

    private

    def member_params
        params.require(:member).permit(:firstname, :lastname, :email, :password, :rank_id, :user_verified)
    end

    def update_last_connection_param
        params.require(:member).permit(:lastConnection)
    end
end