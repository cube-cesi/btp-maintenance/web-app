class Api::CommentsController < ApplicationController

    def index
        comments = Comment.all

        render json: CommentSerializer.new(comments).serialized_json
    end

    def show
        comment = Comment.find(params[:id])

        render json: CommentSerializer.new(comment).serialized_json
    end

    def create
        comment = Comment.new(comment_params)

        if comment.save
            render json: CommentSerializer.new(comment).serialized_json
        else
            render json: { error: comment.errors.messages }, status: 422
        end
    end

    def create_mobile
        comment = Comment.new(comment_params_mobile)
        comment.member_id = helpers.current_member.id

        if comment.save
            render json: CommentSerializer.new(comment).serialized_json
        else
            render json: { error: comment.errors.messages }, status: 422
        end
    end

    def update
        comment = Comment.find(params[:id])

        if comment.update(comment_params)
            render json: CommentSerializer.new(comment).serialized_json
        else
            render json: { error: comment.errors.messages }, status: 422
        end
    end

    def destroy
        comment = Comment.find(params[:id])

        if comment.destroy
            head :no_content
        else
            render json: { error: comment.errors.messages }, status: 422
        end
    end

    private

    def comment_params
        params.require(:comment).permit(:member_id, :issue_id, :description)
    end

    def comment_params_mobile
        params.require(:comment).permit(:issue_id, :description)
    end
end