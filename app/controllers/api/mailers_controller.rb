require './lib/src/smtp.rb'

class Api::MailersController < ApplicationController
    def sendEmailMaterial
        Member.joins(:rank).where(ranks: {name: "Administrateur"}).each do | administrator |
            params_email = params["params"]
            sendEmail(administrator.email, 'Demande d\'ajout de matériel', "Une demande a été effectué pour ajouter: #{params_email[:quantity]} #{params_email[:material]} pour #{params_email[:room]}")
        end
    end
end