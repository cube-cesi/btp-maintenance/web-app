class Api::HelpersController < ApplicationController
    def index
        helpers = Helper.all
        render json: HelperSerializer.new(helpers).serialized_json
    end

    def create
        helper = Helper.new(helper_params)
        if helper.save
            render json: HelperSerializer.new(helper).serialized_json
        else
            render json: { error: helper.errors.messages }, status: 422
        end
    end

    def update
        helper = Helper.find(params[:id])

        if helper.update(helper_params)
            render json: HelperSerializer.new(helper).serialized_json
        else
            render json: { error: helper.errors.messages }, status: 422
        end
    end

    def show
        helper = Helper.find(params[:id])
        render json: HelperSerializer.new(helper).serialized_json
    end

    def destroy
        helper = Helper.find(params[:id])
    
        if helper.destroy
            head :no_content
        else
            render json: { error: helper.errors.messages }, status: 422
        end
    end

    private

    def helper_params
        params.require(:helper).permit(:heading, :description)
    end
end