class ApplicationController < ActionController::Base
    skip_before_action :verify_authenticity_token

    helper_method :login!, :login_anon!, :logged_in?, :current_member, :authorized_member?, :logout!, :set_member, :is_admin?, :is_user?, :is_technician?, :is_referent?, :is_anonymous?, :is_session_expired?, :reset_session_timer

    # Éxecuté avant chaque action du controller
    before_action :verify_login_and_timer

    # Éxecuté après chaque action
    after_action :reset_session_timer

    def login!
        session[:member_id] = @member.id
        session[:anon] = false
        session[:expires_at] = Time.now + 1.hours
    end

    def login_anon!
        session[:anon] = true
        session[:expires_at] = Time.now + 1.hours
    end

    def logged_in?
        !!session[:member_id]
    end

    def current_member
        @current_member ||= Member.find(session[:member_id]) if session[:member_id]
    end

    def authorized_member?
        @member == current_member
    end

    def logout!
        cookies.clear
        reset_session
    end

    def set_member
        @member = Member.find_by(id: session[:member_id])
    end

    def is_admin?
        current_member.rank.name == "Administrateur"
    end

    def is_user?
        current_member.rank.name == "Utilisateur"
    end

    def is_technician?
        current_member.rank.name == "Technicien"
    end

    def is_referent?
        current_member.rank.name == "Référant bâtiment"
    end

    def is_anonymous?
        session[:anon] == true
    end

    def is_session_expired?
        session[:expires_at] < Time.now
    end

    def reset_session_timer
        session[:expires_at] = Time.now + 1.hours
    end

    def verify_login_and_timer
        autorised_path = ['', '/', '/login', '/api/auth/checkLogin', '/api/auth/logged_in', '/api/auth/login', '/api/auth/is-anonymous', '/api/2fa/send', '/api/2fa/handle', '/api/auth/anonymous']

        # Évite les accès à la base de données sans être connecté
        if (!logged_in? && !is_anonymous?) && (!request.fullpath.in?(autorised_path))
            redirect_to "/"
        else
            if !session[:expires_at].blank?
                # Déconnecte l'utilisateur après 1 heure d'inactivité
                if is_session_expired?
                    logout!
                end
            end
        end
    end
end
