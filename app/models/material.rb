class Material < ApplicationRecord
  belongs_to :room
  has_many :issues, dependent: :destroy
end
