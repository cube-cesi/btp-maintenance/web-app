class Room < ApplicationRecord
    has_many :materials, dependent: :destroy

    has_many :room_members
    has_many :members, through: :room_members

    before_create :nameify
    before_update :nameify
    before_destroy :remove_user_notification

    def nameify
        self.name_url = name.parameterize
    end

    def remove_user_notification
        ActiveRecord::Base.connection.execute("DELETE FROM room_members WHERE room_id = #{self.id};")
    end
end
