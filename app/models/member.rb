class Member < ApplicationRecord
    #session
    has_secure_password
    validates :email, presence: true
    validates :email, uniqueness: true
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

    before_destroy :remove_user_notification

    belongs_to :rank
    has_many :issues, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :notifications, dependent: :destroy
    has_many :member_issues, class_name: 'Issue', foreign_key: 'member_id', dependent: :destroy
    has_many :room_members
    has_many :rooms, through: :room_members

    trigger.before(:update).of(:rank_id) do
        <<-SQL
            IF (SELECT r.name FROM ranks r WHERE r.id = NEW.rank_id) = 'Administrateur' THEN
                SET NEW.activate2FA = TRUE;
            ELSE
                SET NEW.activate2FA = FALSE;
            END IF;
        SQL
    end

    def remove_user_notification
        ActiveRecord::Base.connection.execute("DELETE FROM room_members WHERE member_id = #{self.id};")
    end
end
