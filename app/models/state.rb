class State < ApplicationRecord
    has_many :issues, dependent: :destroy
end
