class Issue < ApplicationRecord
    belongs_to :member, class_name: 'Member', optional: true
    belongs_to :material
    belongs_to :state
    belongs_to :priority
    has_many :comments, dependent: :destroy
end
