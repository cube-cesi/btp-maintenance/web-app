class Priority < ApplicationRecord
    has_many :issues, dependent: :destroy
end
