class Rank < ApplicationRecord
    has_many :members, dependent: :destroy
end
