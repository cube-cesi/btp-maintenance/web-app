class AddRdcToRooms < ActiveRecord::Migration[6.0]
  def change
    # Valeur fausse = 1er étage, valeur vrai = 2ème étage
    add_column :rooms, :rdc, :boolean, default: false, null: false
  end
end
