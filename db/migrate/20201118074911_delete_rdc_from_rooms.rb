class DeleteRdcFromRooms < ActiveRecord::Migration[6.0]
  def change
    remove_column :rooms, :rdc, :boolean 
  end
end
