class DropZonesTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :zones
  end
end
