class RenameMaterialsIdToMaterialId < ActiveRecord::Migration[6.0]
  def change
    rename_column :issues, :materials_id, :material_id
  end
end