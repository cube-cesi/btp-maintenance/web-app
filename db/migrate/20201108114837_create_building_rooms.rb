class CreateBuildingRooms < ActiveRecord::Migration[6.0]
  def change
    create_table(:building_rooms, primary_key: [:buildings_id, :rooms_id]) do |t|
      t.belongs_to :buildings, null: false, foreign_key: true
      t.belongs_to :rooms, null: false, foreign_key: true

      t.timestamps
    end
  end
end
