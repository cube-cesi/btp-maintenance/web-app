class CreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :members do |t|
      t.string :firstname, limit: 30, null: false
      t.string :lastname, limit: 30, null: false
      t.string :email, limit: 30, null: false
      t.column(:phone, 'char(10)', null: true)  # Pas de type char avec rails
      t.datetime :lastConnection
      t.boolean :activate2FA, default: false, null: false
      t.string :password_digest, limit: 64, null: false
      t.belongs_to :rank, null: false, foreign_key: true

      t.timestamps
    end
  end
end
