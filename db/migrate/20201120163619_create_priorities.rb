class CreatePriorities < ActiveRecord::Migration[6.0]
  def change
    create_table :priorities do |t|
      t.string :name, null: false

      t.timestamps
    end
    # Supression du champ priority dans issue et ajout clef étrangère
    remove_column :issues, :priority, :string, limit: 25, null: false 
    add_reference :issues, :priorities, foreign_key: true
    # On renomme le champ des raisons de cohérences et éviter de futur bugs
    rename_column :issues, :priorities_id, :priority_id
  end
end
