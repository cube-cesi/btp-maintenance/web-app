class CreateRooms < ActiveRecord::Migration[6.0]
  def change
    create_table :rooms do |t|
      t.string :name, limit: 30, null: false
      t.string :position_x, null: true
      t.string :position_y, null: true
      t.string :localisation_gps, null: true
      t.string :name_url, null: false

      t.belongs_to :floor, null: false, foreign_key: true

      t.index :name , unique: true

      t.timestamps
    end
  end
end