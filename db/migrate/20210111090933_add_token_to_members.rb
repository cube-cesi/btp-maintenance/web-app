class AddTokenToMembers < ActiveRecord::Migration[6.0]
  def change
    add_column :members, :token, :string, limit: 64
  end
end
