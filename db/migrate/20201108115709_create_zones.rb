class CreateZones < ActiveRecord::Migration[6.0]
  def change
    create_table :zones do |t|
      t.string :name, limit: 30, null: false

      t.timestamps
    end
  end
end
