class DropBuildingZonesTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :building_zones
  end
end
