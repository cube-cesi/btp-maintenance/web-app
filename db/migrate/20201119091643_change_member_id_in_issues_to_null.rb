class ChangeMemberIdInIssuesToNull < ActiveRecord::Migration[6.0]
  def change
    change_column_null :issues, :member_id, true
  end
end
