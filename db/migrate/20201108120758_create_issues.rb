class CreateIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issues do |t|
      t.text :description, null: false
      t.string :priority, limit: 25, null: false
      t.datetime :openDate, null: false
      t.datetime :endDate
      t.belongs_to :materials, null: false, foreign_key: true

      t.references :member, null: false,  foreign_key: { to_table: :members }
      t.references :technician, null: true,  foreign_key: { to_table: :members }

      t.timestamps
    end
  end
end
