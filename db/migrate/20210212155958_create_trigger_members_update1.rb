# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggerMembersUpdate1 < ActiveRecord::Migration[6.0]
  def up
    drop_trigger("members_before_update_of_rank_id_row_tr", "members", :generated => true)

    create_trigger("members_before_update_of_rank_id_row_tr", :generated => true, :compatibility => 1).
        on("members").
        before(:update).
        of(:rank_id) do
      <<-SQL_ACTIONS
            IF (SELECT r.name FROM ranks r WHERE r.id = NEW.rank_id) = 'Administrateur' THEN
                SET NEW.activate2FA = TRUE;
            ELSE
                SET NEW.activate2FA = FALSE;
            END IF;
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger("members_before_update_of_rank_id_row_tr", "members", :generated => true)

    create_trigger("members_before_update_of_rank_id_row_tr", :generated => true, :compatibility => 1).
        on("members").
        before(:update).
        of(:rank_id) do
      <<-SQL_ACTIONS
            IF (SELECT r.name FROM ranks r WHERE r.id = NEW.rank_id) = 'Administrateur' THEN
                SET NEW.activate2FA = TRUE;
            END IF;
      SQL_ACTIONS
    end
  end
end
