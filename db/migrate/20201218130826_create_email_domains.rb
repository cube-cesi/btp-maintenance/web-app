class CreateEmailDomains < ActiveRecord::Migration[6.0]
  def change
    create_table :email_domains do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
