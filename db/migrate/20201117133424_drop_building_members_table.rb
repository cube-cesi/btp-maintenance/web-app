class DropBuildingMembersTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :building_members
  end
end
