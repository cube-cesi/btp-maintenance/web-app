class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.belongs_to :member, null: true, foreign_key: true
      t.belongs_to :issue, null: false, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
