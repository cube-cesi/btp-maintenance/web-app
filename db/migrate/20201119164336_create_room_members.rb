class CreateRoomMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :room_members, primary_key: [:room_id, :member_id] do |t|
      t.belongs_to :room, null: false, foreign_key: true
      t.belongs_to :member, null: false, foreign_key: true
      #t.integer :room_id
      #t.integer :member_id

      t.timestamps
    end
  end
end
