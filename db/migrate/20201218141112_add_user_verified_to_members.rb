class AddUserVerifiedToMembers < ActiveRecord::Migration[6.0]
  def change
    add_column :members, :user_verified, :boolean, default: false, null: false
  end
end
