class ReplaceTechnicianInIssues < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :issues, column: :technician_id
    remove_column :issues, :technician_id, :bigint
    add_column :issues, :technician, :string
  end
end
