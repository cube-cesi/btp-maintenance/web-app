class DropBuildingsTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :buildings
  end
end
