class CreateStates < ActiveRecord::Migration[6.0]
  def change
    create_table :states do |t|
      t.string :name, null: false
    end
    add_reference :issues, :state, foreign_key: true
  end
end
