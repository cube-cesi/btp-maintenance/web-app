class AddTitreAndAddDefaultPriorityAndRemoveOpenDateInIssues < ActiveRecord::Migration[6.0]
  def change
    remove_column :issues, :openDate, :datetime 
    change_column_default :issues, :state_id, 1
    change_column_null :issues, :state_id, false
    add_column :issues, :title, :string, limit: 50, null: false
  end
end
