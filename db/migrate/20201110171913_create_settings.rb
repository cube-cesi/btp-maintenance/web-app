class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.string :mailSmtpAddress, limit: 255, null: true
      t.integer :mailSmtpPort, limit: 5, null: true
      t.string :mailAddress, limit: 255, null: true
      t.string :encrypted_pswd, limit: 255, null: true
      t.string :encrypted_pswd_iv, limit: 18, null: true

      t.timestamps
    end
  end
end
