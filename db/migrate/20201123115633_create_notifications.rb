class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.string :description, limit: 255, null: false
      t.boolean :read, null: false, default: false
      t.belongs_to :members, null: false, foreign_key: true

      t.timestamps
    end
    # On renomme le champ pour des raisons de cohérences et éviter de futur bugs
    rename_column :notifications, :members_id, :member_id
  end
end
