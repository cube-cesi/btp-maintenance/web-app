class AddOrderToPriorities < ActiveRecord::Migration[6.0]
  def change
    add_column :priorities, :order, :integer
  end
end
