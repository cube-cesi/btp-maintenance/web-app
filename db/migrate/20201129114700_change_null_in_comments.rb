class ChangeNullInComments < ActiveRecord::Migration[6.0]
  def change
    change_column_null :comments, :description, false
    change_column_null :comments, :member_id, false
  end
end
