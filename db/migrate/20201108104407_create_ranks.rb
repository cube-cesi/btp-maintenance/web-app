class CreateRanks < ActiveRecord::Migration[6.0]
  def change
    create_table :ranks do |t|
      t.string :name, limit: 30, null: false

      t.timestamps
    end
  end
end
