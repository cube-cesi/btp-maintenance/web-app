class CreateBuildingZones < ActiveRecord::Migration[6.0]
  def change
    create_table(:building_zones, primary_key: [:buildings_id, :zones_id]) do |t|
      t.belongs_to :buildings, null: false, foreign_key: true
      t.belongs_to :zones, null: false, foreign_key: true

      t.timestamps
    end
  end
end
