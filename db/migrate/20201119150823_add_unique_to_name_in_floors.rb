class AddUniqueToNameInFloors < ActiveRecord::Migration[6.0]
  def change
    add_index :floors, :name, unique: true
  end
end
