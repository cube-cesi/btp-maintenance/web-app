class CreateBuildingMembers < ActiveRecord::Migration[6.0]
  def change
    create_table(:building_members, primary_key: [:buildings_id, :members_id]) do |t|
      t.belongs_to :buildings, null: false, foreign_key: true
      t.belongs_to :members, null: false, foreign_key: true
      
      t.timestamps
    end
  end
end