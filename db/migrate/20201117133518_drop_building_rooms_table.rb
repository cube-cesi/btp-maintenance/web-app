class DropBuildingRoomsTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :building_rooms
  end
end
