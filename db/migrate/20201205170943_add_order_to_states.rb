class AddOrderToStates < ActiveRecord::Migration[6.0]
  def change
    add_column :states, :order, :integer
  end
end
