# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

floor = Floor.create([
    {
        name: "Rez-de-chaussé",
        plan_source: "floor-1.png"
    },
    {
        name: "Etage 1",
        plan_source: "floor-2.png"
    }
])

rooms = Room.create([
    {
        name: "Salle 1",
        floor_id: 1
    },
    {
        name: "Salle 2",
        floor_id: 1
    },
    {
        name: "Salle 3",
        floor_id: 1
    },
    {
        name: "Salle 4",
        floor_id: 1
    }
])

materials = Material.create([
    {
        name: "Projecteur",
        quantity: 1,
        room: rooms.first
    },
    {
        name: "Chaise",
        quantity: 30,
        room: rooms.first
    }
])

ranks = Rank.create([
    {
        name: "Administrateur"
    },
    {
        name: "Référant bâtiment"
    },
    {
        name: "Technicien"
    },
    {
        name: "Utilisateur"
    }
])

setting = Setting.create([
    {
        mailSmtpAddress: "smtp.google.com"
    }
])

states = State.create([
    {
        name: "Ouvert"
    },

    {
        name: "Envoyé"
    },

    {
        name: "Assigné"
    },

    {
        name: "Fermé"
    }
])
        
helper = Helper.create([
    {
        heading: "Comment poster un ticket ?",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        heading: "Comment voir un ticket ?",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    }
])

priorities = Priority.create([
    {
        name: "Bas"
    },

    {
        name: "Normal"
    },

    {
        name: "Haut"
    },

    {
        name: "Urgent"
    }
])

email_domains = EmailDomain.create([
    {
        name: "cesi.fr"
    },

    {
        name: "viacesi.fr"
    },
=begin
    # Test
    {
        name: 'gmail.com'
    }
=end
])